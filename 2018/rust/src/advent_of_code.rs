#![allow(dead_code)]
// This file requires the "num" crate

use num::Signed;
use std::fmt::Display;
use std::fs::File;
use std::io;
use std::io::{BufRead, BufReader};
use std::ops::Sub;

pub struct Lines {
    io_lines: io::Lines<BufReader<File>>,
}

impl Lines {
    pub fn new(io_lines: io::Lines<BufReader<File>>) -> Self {
        Lines { io_lines }
    }
}

impl Iterator for Lines {
    type Item = String;
    fn next(&mut self) -> Option<Self::Item> {
        self.io_lines.next().map(|n| n.unwrap())
    }
}

pub fn read_file_lines(path: &str) -> Lines {
    let file = File::open(path).unwrap();
    let reader = BufReader::new(file);
    Lines::new(reader.lines())
}

pub fn print_equality_test<T>(label: &str, expected: T, actual: T)
where
    T: PartialEq + Display,
{
    println!(
        "{label}Expected: {expected}, Actual: {actual}, Equal: {}",
        expected == actual
    )
}

pub fn manhattan_distance<T>(x: &(T, T), y: &(T, T)) -> T
where
    T: Sub + Signed + Copy,
{
    (x.0 - y.0).abs() + (x.1 - y.1).abs()
}
