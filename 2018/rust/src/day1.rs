mod advent_of_code;

use advent_of_code::*;
use std::collections::HashSet;

fn part1(changes: &Vec<i64>) {
    let sum = changes.iter().fold(0, |s, x| x + s);
    print_equality_test("Part 1: ", 582, sum);
}

fn part2(changes: &Vec<i64>) {
    let mut sum = 0;
    let mut counts = HashSet::with_capacity(200000);
    'outer:
    loop {
        for change in changes {
            sum += change;
            if counts.contains(&sum) {
                break 'outer;
            } else {
                counts.insert(sum);
            }
        }
    }
    print_equality_test("Part 2: ", 488, sum);
}

fn main() {
    let path = std::env::args().nth(1).unwrap_or("../input/day1.txt".into());
    let changes = std::fs::read_to_string(path).unwrap()
        .split("\n")
        .filter(|&x| x != "")
        .map(|x| x.parse::<i64>().unwrap())
        .collect::<Vec<_>>();
    part1(&changes);
    part2(&changes);
}
