mod advent_of_code;

use advent_of_code::*;

fn calc_counts(s: &str) -> (i64, i64) {
    let mut counts: [u8; 256] = [0; 256];
    for &b in s.as_bytes() {
        let index: usize = b as usize - 97;
        counts[index] += 1;
    }
    counts.iter().fold((0, 0), |(twos, threes), &c| {
        if c == 2 {
            (1, threes)
        } else if c == 3 {
            (twos, 1)
        } else {
            (twos, threes)
        }
    })
}

fn part1(lines: &Vec<&str>) {
    let mut num_twos = 0;
    let mut num_threes = 0;
    for line in lines {
        let (twos, threes) = calc_counts(line);
        num_twos += twos;
        num_threes += threes;
    }
    print_equality_test("Part 1: ", 8715, num_twos * num_threes);
}

fn part2(lines: &Vec<&str>) {
    let mut answer = None;
    'outer:
    for &line1 in lines {
        'inner:
        for &line2 in lines {
            if line1 == line2 {
                continue;
            }
            let mut diffs = 0;
            for i in 0..line1.len() {
                if line1.as_bytes()[i] != line2.as_bytes()[i] {
                    diffs += 1;
                    if diffs > 1 {
                        continue 'inner
                    }
                }
            }
            if diffs == 1 {
                answer = Some(
                    line1.as_bytes().iter()
                    .zip(line2.as_bytes().iter())
                    .filter(|(&a, &b)| a == b)
                    .map(|(&a, _)| a)
                    .collect::<Vec<u8>>()
                    );
                break 'outer
            }
        }
    }
    let answer: String = std::str::from_utf8(answer.unwrap().as_slice()).unwrap().into();
    print_equality_test("Part 1: ", "fvstwblgqkhpuixdrnevmaycd".to_string(), answer);
}

fn main() {
    let path = std::env::args().nth(1).unwrap_or("../input/day2.txt".into());
    let contents = std::fs::read_to_string(path).unwrap();
    let lines = contents.split("\n").filter(|&x| x != "").collect::<Vec<_>>();
    part1(&lines);
    part2(&lines);
}
