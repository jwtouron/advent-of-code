mod advent_of_code;

use advent_of_code::*;
use regex::Regex;
use std::collections::HashMap;
use std::str::FromStr;

#[derive(Debug)]
enum Event {
    BeginsShift(i64),
    FallsAsleep,
    WakesUp,
}

thread_local!(static EVENT_REGEX: Regex = Regex::new(r"Guard #(\d+) (begins shift)|(falls asleep)|(wakes up)").unwrap());

impl FromStr for Event {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        EVENT_REGEX.with(|re| {
            let caps = re.captures(s).unwrap();
            if let Some(_) = caps.get(2) {
                Ok(Event::BeginsShift(caps[1].parse().unwrap()))
            } else if let Some(_) = caps.get(3) {
                Ok(Event::FallsAsleep)
            } else if let Some(_) = caps.get(4) {
                Ok(Event::WakesUp)
            } else {
                unreachable!()
            }
        })
    }
}

#[derive(Debug)]
#[allow(dead_code)]
struct Timestamp {
    year: i64,
    month: i64,
    day: i64,
    hour: i64,
    minute: i64,
}

thread_local!(static TIMESTAMP_REGEX: Regex = Regex::new(r"\[(\d{4})-(\d{2})-(\d{2}) (\d{2}):(\d{2})\]").unwrap());

impl FromStr for Timestamp {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        TIMESTAMP_REGEX.with(|re| {
            let caps = re.captures(s).unwrap();
            let year = caps[1].parse().unwrap();
            let month = caps[2].parse().unwrap();
            let day = caps[3].parse().unwrap();
            let hour = caps[4].parse().unwrap();
            let minute = caps[5].parse().unwrap();
            Ok(Timestamp {
                year,
                month,
                day,
                hour,
                minute,
            })
        })
    }
}

#[derive(Debug)]
struct Record {
    timestamp: Timestamp,
    event: Event,
}

impl FromStr for Record {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let index = s.find(']').unwrap();
        let timestamp: Timestamp = s[0..index + 1].parse().unwrap();
        let event: Event = s[index + 1..].parse().unwrap();
        Ok(Record { timestamp, event })
    }
}

// Map of guard id => map of minute => number of times asleep at minute
type Analysis = HashMap<i64, HashMap<i64, i64>>;

fn analyze(records: &Vec<Record>) -> Analysis {
    let mut hm: HashMap<i64, Vec<i64>> = HashMap::with_capacity(1000);
    let mut guard_id: i64 = -1;
    let mut fell_asleep: i64 = -1;
    for record in records.iter() {
        match record.event {
            Event::BeginsShift(gid) => {
                if fell_asleep > -1 {
                    for min in fell_asleep .. record.timestamp.minute {
                        hm.entry(guard_id).or_insert(Vec::new()).push(min);
                    }
                }
                guard_id = gid;
                fell_asleep = -1;
            }
            Event::FallsAsleep => fell_asleep = record.timestamp.minute,
            Event::WakesUp => {
                for min in fell_asleep .. record.timestamp.minute {
                    hm.entry(guard_id).or_insert(Vec::new()).push(min);
                }
                fell_asleep = -1;
            }
        }
    }
    hm.into_iter()
        .fold(Analysis::new(), |mut analysis, (k, v)| {
            let hm = v.into_iter().fold(HashMap::<i64, i64>::new(), |mut hm, v| {
                *hm.entry(v).or_insert(0) += 1;
                hm
            });
            analysis.insert(k, hm);
            analysis
        })
}

fn part1(analysis: &Analysis) {
    let guard = analysis.iter()
        .max_by_key(|(_, hm)| hm.values().sum::<i64>())
        .unwrap();
    let minute = guard.1.iter().max_by_key(|x| x.1).unwrap();
    print_equality_test("Part 1: ", 8421, guard.0 * minute.0);
}

fn part2(analysis: &Analysis) {
    let guard: (i64, i64, i64) = analysis.iter()
        .fold((0, 0, 0), |(gid, min, count), (gid2, hm)| {
            let (min2, count2) = hm.iter().max_by_key(|x| x.1).unwrap();
            if *count2 > count {
                (*gid2, *min2, *count2)
            } else {
                (gid, min, count)
            }
        });
    print_equality_test("Part 2: ", 83359, guard.0 * guard.1)
}

fn main() {
    let path = std::env::args().nth(1).unwrap_or("../input/day4.txt".into());
    let mut records = read_file_lines(path.as_ref()).map(|x| x.parse().unwrap()).collect::<Vec<Record>>();
    records.sort_by_key(|r| {
        let ts = &r.timestamp;
        (ts.year, ts.month, ts.day, ts.hour, ts.minute)
    });
    let analysis = analyze(&records);
    part1(&analysis);
    part2(&analysis);
}
