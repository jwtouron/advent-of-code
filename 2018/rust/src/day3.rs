mod advent_of_code;

use advent_of_code::print_equality_test;
use std::collections::{HashSet, HashMap};
use std::str::FromStr;

#[derive(Debug)]
struct Claim {
    id: usize,
    left: usize,
    top: usize,
    width: usize,
    height: usize,
}

impl FromStr for Claim {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let splits = s.split(" ").collect::<Vec<_>>();
        let id = splits[0][1..].parse().unwrap();
        let pos = splits[2].split_once(",").unwrap();
        let left = pos.0.parse().unwrap();
        let top = pos.1[0..pos.1.len()-1].parse().unwrap();
        let size = splits[3].split_once("x").unwrap();
        let width = size.0.parse().unwrap();
        let height = size.1.parse().unwrap();
        Ok( Claim { id, left, top, width, height } )
    }
}


type Analysis = HashMap<(usize, usize), HashSet<usize>>;

fn analyze(claims: &Vec<Claim>) -> Analysis {
    let mut result = HashMap::new();
    for claim in claims.iter() {
        for x in claim.left .. claim.left + claim.width {
            for y in claim.top .. claim.top + claim.height {
                 result.entry((x, y))
                     .or_insert(HashSet::new())
                     .insert(claim.id);
            }
        }
    }
    result
}

fn part1(analysis: &Analysis) {
    let answer: usize = analysis.values()
        .map(|s| s.len())
        .filter(|&s| s > 1)
        .count();
    print_equality_test("Part 1: ", 114946, answer);
}

fn part2(claims: &Vec<Claim>, analyses: &Analysis) {
    let mut claim_ids: HashSet<usize> = HashSet::from_iter(claims.iter().map(|x| x.id));
    for set in analyses.values() {
        if set.len() > 1 {
            for x in set {
                claim_ids.remove(x);
            }
        }
    }
    print_equality_test("Part 2: ", 877, *claim_ids.iter().nth(0).unwrap());
}

fn main() {
    let path = std::env::args().nth(1).unwrap_or("../input/day3.txt".into());
    let claims: Vec<Claim> = std::fs::read_to_string(path).unwrap()
        .split("\n")
        .filter(|&s| s != "")
        .map(|x| x.parse().unwrap())
        .collect();
    let analyses = analyze(&claims);
    part1(&analyses);
    part2(&claims, &analyses);
}
