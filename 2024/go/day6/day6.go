package main

import (
	"aoc/util"
)

type dummy struct{}

var nextDir = map[vec2]vec2{
	{-1, 0}: {0, 1},
	{0, 1}: {1, 0},
	{1, 0}: {0, -1},
	{0, -1}: {-1, 0},
}

type vec2 struct {
	r, c int
}

type guard struct {
	pos, dir vec2
	seen map[vec2]map[vec2]dummy
	looping bool
}

func newGuard(pos vec2) guard {
	return guard{
		pos: pos,
		dir: vec2{-1, 0},
		seen: map[vec2]map[vec2]dummy{pos: {{-1, 0}: dummy{}}},
	}
}

func isWithinLab(row, col int, lab [][]rune) bool {
	return row >= 0 && row < len(lab) && col >= 0 && col < len(lab[0])
}

func (g *guard) isWithinLab(lab [][]rune) bool {
	return isWithinLab(g.pos.r, g.pos.c, lab)
}

func (g *guard) isLooping() bool {
	return g.looping
}

func (g *guard) step(lab [][]rune) {
	nextRow := g.pos.r + g.dir.r
	nextCol := g.pos.c + g.dir.c
	if isWithinLab(nextRow, nextCol, lab) && lab[nextRow][nextCol] == '#' {
		g.dir = nextDir[g.dir]
	} else {
		g.pos.r = nextRow
		g.pos.c = nextCol
		if g.isWithinLab(lab) {
			if dirs, ok := g.seen[g.pos]; ok {
				if _, ok = dirs[g.dir]; ok {
					g.looping = true
				} else {
					dirs[g.dir] = dummy{}
				}
			} else {
				g.seen[g.pos] = map[vec2]dummy{g.dir: {}}
			}
		}
	}
}

func part1(g *guard, lab [][]rune) {
	for g.isWithinLab(lab) {
		g.step(lab)
	}
	util.AssertPuzzlePart(1, 4711, len(g.seen))
}

func part2(path map[vec2]map[vec2]dummy, lab [][]rune, startPos vec2) {
	count := 0
	for pos := range path {
		if pos == startPos {
			continue
		}
		lab[pos.r][pos.c] = '#'
		g := newGuard(startPos)
		for g.isWithinLab(lab) && !g.isLooping() {
			g.step(lab)
		}
		if g.isLooping() {
			count++
		}
		lab[pos.r][pos.c] = '.'
	}
	util.AssertPuzzlePart(2, 1562, count)
}

func main() {
	path := util.GetInputPath(6)
	guard := guard{}
	startPos := vec2{}
	lab, _ := util.CollectInput(path, func(row int, line string) []rune {
		res := make([]rune, 0, len(line))
		for col, r := range line {
			res = append(res, r)
			if r == '^' {
				startPos = vec2{row, col}
				guard = newGuard(startPos)
			}
		}
		return res
	})

	part1(&guard, lab)
	part2(guard.seen, lab, startPos)
}
