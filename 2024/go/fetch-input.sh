#!/bin/sh

# Default year to current year
YEAR=$(date +%Y)

# Default day to yesterday's day
DAY=$(date +%-d)

SESSION="$AOC_SESSION"

# User agent string
# USERAGENT="https://github.com/jonathanpaulson/AdventOfCode/blob/master/get_input.py by jonathanpaulson@gmail.com"
USERAGENT="Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/130.0.0.0 Safari/537.36"

# Function to show usage
usage() {
    echo "Usage: $0 [--session SESSION] [--year YEAR] [--day DAY]"
    exit 1
}

# Parse command-line arguments
while [ "$#" -gt 0 ]; do
    case "$1" in
        --session)
            SESSION="$2"
            shift 2
            ;;
        --year)
            YEAR="$2"
            shift 2
            ;;
        --day)
            DAY="$2"
            shift 2
            ;;
        *)
            usage
            ;;
    esac
done

# Check if session is set either by environment variable or argument
if [ -z "$SESSION" ]; then
    echo "Error: SESSION is required. Set it via --session or AOC_SESSION environment variable."
    echo ""
    echo 'You can find SESSION by using Chrome tools:'
    echo '1) Go to https://adventofcode.com/2022/day/1/input'
    echo '2) right-click -> inspect -> click the "Application" tab.'
    echo '3) Refresh'
    echo '5) Click https://adventofcode.com under "Cookies"'
    echo '6) Grab the value for session. Fill it in.'

    exit 1
fi

# Make the request using curl
curl -s "https://adventofcode.com/${YEAR}/day/${DAY}/input" --cookie "session=${SESSION}" -A "${USERAGENT}"
