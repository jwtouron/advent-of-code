package main

import (
	"aoc/util"
	v "aoc/util/vec"
	"iter"
)

type coord = v.Vec2
type dummy struct{}

func isWithinGrid(coord coord, grid [][]int) bool {
	r, c := coord.X, coord.Y
	return r >= 0 && r < len(grid) && c >= 0 && c < len(grid[0])
}

func part1(grid [][]int, trailheads []coord) {
	newNeighborsFunc := func() func (coord) iter.Seq[coord] {
		seen := make(map[coord]dummy)
		return func (pos coord) iter.Seq[coord] {
			return func(yield func(coord) bool) {
				for _, nbor := range []coord{pos.Add(v.Vec2{X: 1, Y: 0}), pos.Add(v.Vec2{X: 0, Y: 1}), pos.Add(v.Vec2{X: -1, Y: 0}), pos.Add(v.Vec2{X: 0, Y: -1})} {
					if _, ok := seen[nbor]; !ok && isWithinGrid(nbor, grid) && grid[nbor.X][nbor.Y] == grid[pos.X][pos.Y] + 1 {
						seen[nbor] = dummy{}
						if !yield(nbor) {
							return
						}
					}
				}
			}
		}
	}

	count := 0
	for _, trailhead := range trailheads {
		for range util.DFS(trailhead, newNeighborsFunc(), func(c coord) bool { return grid[c.X][c.Y] == 9 }) {
			count++
		}
	}

	util.AssertPuzzlePart(1, 688, count)
}

func part2(grid [][]int, trailheads []coord) {
	newNeighborsFunc := func (pos coord) iter.Seq[coord] {
		return func(yield func(coord) bool) {
			for _, nbor := range []coord{pos.Add(v.Vec2{X: 1, Y: 0}), pos.Add(v.Vec2{X: 0, Y: 1}), pos.Add(v.Vec2{X: -1, Y: 0}), pos.Add(v.Vec2{X: 0, Y: -1})} {
				if isWithinGrid(nbor, grid) && grid[nbor.X][nbor.Y] == grid[pos.X][pos.Y] + 1 {
					if !yield(nbor) {
						return
					}
				}
			}
		}
	}

	count := 0
	for _, trailhead := range trailheads {
		for range util.DFS(trailhead, newNeighborsFunc, func(c coord) bool { return grid[c.X][c.Y] == 9 }) {
			count++
		}
	}

	util.AssertPuzzlePart(2, 1459, count)
}

func main() {
	path := util.GetInputPath(10)

	trailheads := make([]coord, 0)

	grid, _ := util.CollectInput(path, func(row int, line string) []int {
		res := make([]int, 0, len(line))
		for col, ch := range line {
			coord := coord{X: row, Y: col}
			res = append(res, int(ch) - 48)
			if ch == '0' {
				trailheads = append(trailheads, coord)
			}
		}
		return res
	})

	part1(grid, trailheads)
	part2(grid, trailheads)
}
