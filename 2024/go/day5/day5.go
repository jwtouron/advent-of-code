package main

import (
	"aoc/util"
	"slices"
	"strings"
)

type pair struct {
	a, b int
}

func part1(rules map[pair]struct{}, pages [][]int) {
	sum := 0

	for _, page := range pages {
		correct := true
		outer:
		for i := 0; i < len(page) - 1; i++ {
			for j := i + 1; j < len(page); j++ {
				if _, ok := rules[pair{page[j], page[i]}]; ok {
					correct = false
					break outer
				}
			}
		}
		if correct {
			sum += page[len(page) / 2]
		}
	}

	util.AssertPuzzlePart(1, 5732, sum)
}

func part2(rules map[pair]struct{}, pages [][]int) {
	sum := 0

	for _, page := range pages {
		page2 := slices.Clone(page)
		slices.SortFunc(page2, func(a, b int) int {
			if _, ok := rules[pair{a, b}]; ok {
				return -1
			} else if _, ok := rules[pair{b, a}]; ok {
				return 1
			} else {
				return 0
			}
		})
		if slices.Compare(page, page2) != 0 {
			sum += page2[len(page2) / 2]
		}
	}

	util.AssertPuzzlePart(2, 4716, sum)
}

func main() {
	path := util.GetInputPath(5)

	rules := make(map[pair]struct{})
	pages := make([][]int, 0)

	parsingRules := true
	util.CollectInput(path, func(_ int, line string) int {
		if line != "" {
			if parsingRules {
				parts := strings.Split(line, "|")
				p1 := util.MustAtoi(parts[0])
				p2 := util.MustAtoi(parts[1])

				rules[pair{p1, p2}] = struct{}{}
			} else {
				parts := strings.Split(line, ",")
				page := make([]int, 0)
				for _, part := range parts {
					page = append(page, util.MustAtoi(part))
				}
				pages = append(pages, page)
			}
		} else {
			parsingRules = false
		}

		return 0
	})

	part1(rules, pages)
	part2(rules, pages)
}
