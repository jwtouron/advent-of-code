package main

import (
	"aoc/util"
	"math"
	"slices"
	"strings"
)

func isValid(nums []int64) bool {
	ascending := nums[0] < nums[1]
	for i := 0; i < len(nums) - 1; i++ {
		if ascending && nums[i] >= nums[i+1] {
			return false
		} else if !ascending && nums[i] <= nums[i+1] {
			return false
		}
		diff := math.Abs(float64(nums[i] - nums[i+1]))
		if diff < 1 || diff > 3 {
			return false
		}
	}
	return true
}

func solve(nums [][]int64, isValid func([]int64) bool) int {
	count := 0
	for _, ns := range nums {
		if isValid(ns) {
			count++
		}
	}
	return count
}

func part1(nums [][]int64) {
	count := solve(nums, isValid)
	util.AssertPuzzlePart(1, 369, count)
}

func part2(nums [][]int64) {
	isValid2 := func(nums []int64) bool {
		if isValid(nums) {
			return true
		} else {
			for i := 0; i < len(nums); i++ {
				nums := slices.Clone(nums)
				nums = slices.Delete(nums, i, i+1)
				if isValid(nums) {
					return true
				}
			}
			return false
		}
	}
	count := solve(nums, isValid2)
	util.AssertPuzzlePart(2, 428, count)
}

func main() {
	path := util.GetInputPath(2)
	nums, _ := util.CollectInput(path, func(_ int, line string) []int64 {
		parts := strings.Split(line, " ")
		nums := make([]int64, len(parts))
		for i := range parts {
			nums[i] = util.MustParseInt(parts[i], 10, 64)
		}
		return nums
	})

	part1(nums)
	part2(nums)
}
