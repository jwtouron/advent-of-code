package main

import (
	"aoc/util"
	"iter"
	"math"
	"slices"
	"strings"
)

type Zipped2[A, B any] struct {
	a A
	b B
}

func Zip2[A, B any](as []A, bs []B) iter.Seq[Zipped2[A, B]] {
	return func(yield func(Zipped2[A, B]) bool) {
		for i := 0; i < len(as) && i < len(bs); i++ {
			if !yield(Zipped2[A, B]{as[i], bs[i]}) {
				return
			}
		}
	}
}

func part1(lines []Zipped2[int64, int64]) int {
	lefts := []int64{}
	rights := []int64{}

	for _, line := range lines {
		lefts = append(lefts, line.a)
		rights = append(rights, line.b)
	}

	slices.Sort(lefts)
	slices.Sort(rights)

	sum := 0

	for pair := range Zip2(lefts, rights) {
		sum += int(math.Abs(float64(pair.a - pair.b)))
	}

	return sum
}

func part2(lines []Zipped2[int64, int64]) int64 {
	counts := make(map[int64]int64, len(lines))

	for _, line := range lines {
		counts[line.b]++
	}

	sum := int64(0)

	for _, line := range lines {
		sum += line.a * counts[line.a]
	}

	return sum
}

func main() {
	path := util.GetInputPath(1)

	lines, _ := util.CollectInput(path, func(_ int, line string) Zipped2[int64, int64] {
		parts := strings.Split(line, " ")
		left := util.MustParseInt(parts[0], 10, 64)
		right := util.MustParseInt(parts[len(parts)-1], 10, 64)
		return Zipped2[int64, int64]{left, right}
	})

	util.AssertPuzzlePart(1, 1646452, part1(lines))
	util.AssertPuzzlePart(2, 23609874, part2(lines))
}
