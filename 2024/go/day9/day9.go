package main

import (
	"aoc/util"
	"iter"
	"slices"
)

func part1(input []int) {
	compacted := make([]int, 0, 20000)
	i, j := 0, len(input) - 1

	for {
		if i > j {
			break
		}

		if input[i] > 0 {
			if i % 2 == 0 {
				compacted = append(compacted, i / 2)
			} else {
				compacted = append(compacted, j / 2)
				input[j]--
				if input[j] == 0 {
					j -= 2
				}
			}
			input[i]--
		}

		if input[i] == 0 {
			i++
		}
	}

	checksum := 0
	for i, x := range compacted {
		checksum += i * x
	}

	util.AssertPuzzlePart(1, 6607511583593, checksum)
}

type file struct {
	index, size int
}

func iterFiles(mem []int) iter.Seq[file] {
	return func(yield func(file) bool) {
		var i int
		j := len(mem) - 1
		for i >= 0 && j >= 0 {
			for ; mem[j] == -1; j-- {
			}
			for i = j; i >= 0 && mem[i] == mem[j]; i-- {
			}
			i++
			if !yield(file{i, j - i + 1}) {
				return
			}
			j = i - 1
		}
	}
}

func part2(input []int) {
	compacted := make([]int, 0, 200000)

	empties := make([][]int, 10)

	index := 0

	for i := range input {
		if i % 2 == 0 {
			for range input[i] {
				compacted = append(compacted, i / 2)
			}
		} else {
			for range input[i] {
				compacted = append(compacted, -1)
			}
			empties[input[i]] = append(empties[input[i]], index)
		}
		index += input[i]
	}

	for file := range iterFiles(compacted) {
		minIndex := len(compacted)
		minSize := -1
		for i := file.size; i < 10; i++ {
			if len(empties[i]) > 0 && empties[i][0] < minIndex && empties[i][0] < file.index {
				minIndex = empties[i][0]
				minSize = i
			}
		}
		if minIndex < len(compacted) {
			for i := range file.size {
				compacted[minIndex + i] = compacted[file.index + i]
				compacted[file.index + i] = -1

			}
			empties[minSize] = slices.Delete(empties[minSize], 0, 1)
			if minSize > file.size {
				i := minSize - file.size
				v := minIndex + file.size
				j, _ := slices.BinarySearch(empties[i], v)
				empties[i] = slices.Insert(empties[i], j, v)
			}
		}
	}

	checksum := 0
	for i, x := range compacted {
		if x == -1 {
			continue
		}
		checksum += i * x
	}

	util.AssertPuzzlePart(2, 6636608781232, checksum)
}

func main() {
	path := util.GetInputPath(9)

	lines, _ := util.CollectInput(path, func(_ int, line string) []int {
		res := make([]int, 0, 20000)
		for _, c := range line {
			res = append(res, int(c) - 48)
		}
		return res
	})

	inputPart1 := lines[0]
	inputPart2 := slices.Clone(inputPart1)

	part1(inputPart1)
	part2(inputPart2)
}
