package main

import (
	"aoc/util"
	"strings"
)

type equation struct {
	testValue int
	nums []int
}

func part1(equations []equation) {
	var helper func (nums []int, total, testValue int) bool
	helper = func(nums []int, total, testValue int) bool {
		if total > testValue {
			return false
		}
		if len(nums) == 0 {
			return total == testValue
		}
		return helper(nums[1:], total + nums[0], testValue) ||
			helper(nums[1:], total * nums[0], testValue)
	}

	sum := 0

	for _, equation := range equations {
		if helper(equation.nums[1:], equation.nums[0], equation.testValue) {
			sum += equation.testValue
		}
	}

	util.AssertPuzzlePart(1, 6083020304036, sum)
}

func concatenateNumbers(a, b int) int {
	temp := b
	for temp > 0 {
		temp /= 10
		a *= 10
	}
	return a + b
}

func part2(equations []equation) {
	var helper func (nums []int, total, testValue int) bool
	helper = func(nums []int, total, testValue int) bool {
		if total > testValue {
			return false
		}
		if len(nums) == 0 {
			return total == testValue
		}
		return helper(nums[1:], total + nums[0], testValue) ||
			helper(nums[1:], total * nums[0], testValue) ||
			helper(nums[1:], concatenateNumbers(total, nums[0]), testValue)
	}

	sum := 0

	for _, equation := range equations {
		if helper(equation.nums[1:], equation.nums[0], equation.testValue) {
			sum += equation.testValue
		}
	}

	util.AssertPuzzlePart(2, 59002246504791, sum)
}

func main() {
	path := util.GetInputPath(7)

	equations, _ := util.CollectInput(path, func(_ int, line string) equation {
		parts := strings.Split(line, ": ")
		nums := make([]int, 0)
		for _, part := range strings.Split(parts[1], " ") {
			nums = append(nums, util.MustAtoi(part))
		}
		return equation {
			testValue: util.MustAtoi(parts[0]),
			nums: nums,
		}
	})

	part1(equations)
	part2(equations)
}
