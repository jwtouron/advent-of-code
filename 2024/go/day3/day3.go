package main

import (
	"aoc/util"
	"aoc/util/errors"
	"regexp"
	"slices"
)

var regex = regexp.MustCompile(`(mul)\((\d+),(\d+)\)|(do)\(\)|(don't)\(\)`)

type instruction interface{}

type do struct {}

type dont struct {}

type mul struct {
	a, b int
}

func part1(instrs []instruction) {
	sum := 0
	for _, instr := range instrs {
		switch instr := instr.(type) {
		case mul:
			sum += instr.a * instr.b
		}
	}
	util.AssertPuzzlePart(1, 161289189, sum)
}

func part2(instrs []instruction) {
	sum := 0
	enabled := true
	for _, instr := range instrs {
		switch instr := instr.(type) {
		case mul:
			if enabled {
				sum += instr.a * instr.b
			}
		case do:
			enabled = true
		case dont:
			enabled = false
		}
	}
	util.AssertPuzzlePart(2, 83595109, sum)
}

func parseInstructions(_ int, s string) []instruction {
	result := []instruction{}
	for _, match := range regex.FindAllStringSubmatch(s, -1) {
		if match[1] == "mul" {
			result = append(result, mul{util.MustAtoi(match[2]), util.MustAtoi(match[3])})
		} else if match[4] == "do" {
			result = append(result, do{})
		} else if match[5] == "don't" {
			result = append(result, dont{})
		} else {
			errors.Assert(false, "Can't parse: '%s'", match)
		}
	}
	return result
}

func main() {
	path := util.GetInputPath(3)
	lines, _ := util.CollectInput(path, parseInstructions)
	input := slices.Concat(lines...)
	part1(input)
	part2(input)
}
