package main

import (
	"aoc/util"
)

type coord struct {
	r, c int
}

func (c1 coord) add(c2 coord) coord {
	return coord{c1.r + c2.r, c1.c + c2.c}
}

func (c1 coord) sub(c2 coord) coord {
	return coord{c1.r - c2.r, c1.c - c2.c}
}

func (c coord) isWithinGrid(grid [][]rune) bool {
	return c.r >= 0 && c.r < len(grid) && c.c >= 0 && c.c < len(grid[0])
}

func (c1 coord) compare(c2 coord) int {
	if c1.r < c2.r {
		return -1
	} else if c1.r > c2.r {
		return 1
	} else {
		if c1.c < c2.c {
			return -1
		} else if c1.c > c2.c {
			return 1
		} else {
			return 0
		}
	}
}

type dummy = struct{}

func part1(grid [][]rune, antennas map[rune]map[coord]dummy) {
	antinodes := map[coord]dummy{}

	for _, coords := range antennas {
		for c1 := range coords {
			for c2 := range coords {
				if c1 == c2 || c1.compare(c2) > 0 {
					continue
				}

				diff := coord{c2.r - c1.r, c2.c - c1.c}

				loc1 := c1.sub(diff)
				loc2 := c2.add(diff)

				if loc1.isWithinGrid(grid) {
					antinodes[loc1] = dummy{}
				}

				if loc2.isWithinGrid(grid) {
					antinodes[loc2] = dummy{}
				}
			}
		}
	}

	util.AssertPuzzlePart(1, 280, len(antinodes))
}

func part2(grid [][]rune, antennas map[rune]map[coord]dummy) {
	antinodes := map[coord]dummy{}

	for _, coords := range antennas {
		for c1 := range coords {
			for c2 := range coords {
				if c1 == c2 || c1.compare(c2) > 0 {
					continue
				}

				antinodes[c1] = dummy{}
				antinodes[c2] = dummy{}

				diff := coord{c2.r - c1.r, c2.c - c1.c}

				for loc := c1.sub(diff); loc.isWithinGrid(grid); loc = loc.sub(diff) {
					antinodes[loc] = dummy{}
				}

				for loc := c2.add(diff); loc.isWithinGrid(grid); loc = loc.add(diff) {
					antinodes[loc] = dummy{}
				}
			}
		}
	}

	util.AssertPuzzlePart(2, 958, len(antinodes))
}

func main() {
	path := util.GetInputPath(8)

	antennas := make(map[rune]map[coord]dummy)

	grid, _ := util.CollectInput(path, func (row int, line string) []rune {
		res := make([]rune, 0)
		for col, r := range line {
			res = append(res, r)
			if (r >= 'A' && r <= 'Z') ||
			   (r >= 'a' && r <= 'z') ||
			   (r >= '0' && r <= '9') {
				if coords, ok := antennas[r]; ok {
					coords[coord{row, col}] = dummy{}
				} else {
					antennas[r] = map[coord]dummy{{row, col}: {}}
				}
			}
		}
		return res
	})

	part1(grid, antennas)
	part2(grid, antennas)
}
