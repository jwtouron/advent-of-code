package main

import (
	"aoc/util"
	"slices"
)

func checkXmas(puzzle [][]rune, row, col int, dir [2]int) bool {
	chars := []rune{'M', 'A', 'S'}
	for i := 0; i < 3; i++ {
		if puzzle[row + dir[0]*(i+1)][col + dir[1]*(i+1)] != chars[i] {
			return false
		}
	}
	return true
}

func part1(puzzle [][]rune) {
	sum := 0
	for r := range len(puzzle) {
		for c := range len(puzzle[0]) {
			if puzzle[r][c] == 'X' {
				for _, dir := range [8][2]int{{1, 1}, {1, -1}, {1, 0}, {-1, 1}, {-1, -1}, {-1, 0}, {0, 1}, {0, -1}} {
					if checkXmas(puzzle, r, c, dir) {
						sum++
					}
				}
			}
		}
	}
	util.AssertPuzzlePart(1, 2569, sum)
}

func checkMas(puzzle [][]rune, r, c int) bool {
	valid := (puzzle[r-1][c-1] == 'M' && puzzle[r+1][c+1] == 'S') || (puzzle[r-1][c-1] == 'S' && puzzle[r+1][c+1] == 'M')
	valid = valid && ((puzzle[r-1][c+1] == 'M' && puzzle[r+1][c-1] == 'S') || (puzzle[r-1][c+1] == 'S' && puzzle[r+1][c-1] == 'M'))
	return valid
}

func part2(puzzle [][]rune) {
	sum := 0
	for r := range len(puzzle) {
		for c := range len(puzzle[0]) {
			if puzzle[r][c] == 'A' {
				if checkMas(puzzle, r, c) {
					sum++
				}
			}
		}
	}
	util.AssertPuzzlePart(2, 1998, sum)
}

func main() {
	path := util.GetInputPath(4)
	puzzle, _ := util.CollectInput(path, func(_ int, line string) []rune {
		chars := make([]rune, 0, len(line))
		for _, c := range line {
			chars = append(chars, c)
		}
		return slices.Concat([]rune{'#'}, chars, []rune{'#'})
	})
	padding := [][]rune{slices.Repeat([]rune{'#'}, len(puzzle[0]))}
	puzzle = slices.Concat(padding, puzzle, padding)

	part1(puzzle)
	part2(puzzle)
}
