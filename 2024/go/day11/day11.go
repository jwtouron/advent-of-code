package main

import (
	"aoc/util"
	"math"
	"strings"
)

type pair struct {
	a, b int
}

func extractLeftPart(n int, numDigits int) int {
	divisor := int(math.Pow(10, float64(numDigits/2)))
	return n / divisor
}

func extractRightPart(n int, numDigits int) int {
	divisor := int(math.Pow(10, float64(numDigits/2)))
	return n % divisor
}

func splitNumber(n int) (int, int, bool) {
	if n == 0 {
		return 0, 0, false
	}

	numDigits := int(math.Log10(float64(n))) + 1

	if numDigits%2 == 0 {
		left := extractLeftPart(n, numDigits)
		right := extractRightPart(n, numDigits)
		return left, right, true
	} else {
		return n, 0, false
	}
}

func solveHelper(num int, blinks int, memo map[pair]int) int {
	if blinks == 1 {
		if _, _, ok := splitNumber(num); ok {
			return 2
		} else {
			return 1
		}
	}

	if _, ok := memo[pair{num, blinks}]; !ok {
		var ret int
		if n1, n2, ok := splitNumber(num); ok {
			ret = solveHelper(n1, blinks - 1, memo) + solveHelper(n2, blinks - 1, memo)
		} else {
			if n1 == 0 {
				n1 = 1
			} else {
				n1 *= 2024
			}
			ret = solveHelper(n1, blinks - 1, memo)
		}
		memo[pair{num, blinks}] = ret
	}

	return memo[pair{num, blinks}]
}

func solve(nums []int, blinks int, memo map[pair]int) int {
	count := 0
	for _, num := range nums {
		count += solveHelper(num, blinks, memo)
	}
	return count
}

func part1(nums []int, memo map[pair]int) {
	util.AssertPuzzlePart(1, 209412, solve(nums, 25, memo))
}

func part2(nums []int, memo map[pair]int) {
	util.AssertPuzzlePart(2, 248967696501656, solve(nums, 75, memo))
}

func main() {
	path := util.GetInputPath(11)

	nums, _ := util.CollectInput(path, func(_ int, line string) []int {
		nums := make([]int, 0)
		for _, x := range strings.Split(line, " ") {
			nums = append(nums, util.MustAtoi(x))
		}
		return nums
	})

	memo := make(map[pair]int)

	part1(nums[0], memo)
	part2(nums[0], memo)
}
