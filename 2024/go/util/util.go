package util

import (
	"aoc/util/errors"
	"aoc/util/iter"
	"fmt"
	goiter "iter"
	"os"
	"strconv"
)

func AssertPuzzlePart[T comparable](part int, expected T, actual T) {
	fmt.Printf("Part %d: Expected: %v, Actual: %v, Success: %v\n", part, expected, actual, expected == actual)
}

func CollectInput[T any](path string, transform func(int, string) T) ([]T, error) {
	var err error
	result := make([]T, 0)
	row := 0
	for line := range iter.FileLines(path, &err) {
		result = append(result, transform(row, line))
		row++
	}
	return result, err
}

func dfsHelper[T comparable](path []T, neighbors func(T) goiter.Seq[T], atGoal func(T) bool, yield func([]T) bool) bool {
	if atGoal(path[len(path) - 1]) {
		if !yield(path) {
			return false
		}
	}

	for neighbor := range neighbors(path[len(path) - 1]) {
		path = append(path, neighbor)
		shouldContinue := dfsHelper(path, neighbors, atGoal, yield)
		path = path[:len(path) - 1]
		if !shouldContinue {
			return false
		}
	}

	return true
}

func DFS[T comparable](start T, neighbors func (T) goiter.Seq[T], atGoal func(T) bool) goiter.Seq[[]T] {
	return func(yield func([]T) bool) {
		path := []T{start}
		dfsHelper(path, neighbors, atGoal, yield)
	}
}

func GetInputPath(day int) string {
	if len(os.Args) > 1 {
		return os.Args[1]
	}
	return fmt.Sprintf("../input/day%d.txt", day)
}

func MustAtoi(s string) int {
	i, err := strconv.Atoi(s)
	errors.Assert(err == nil, "Bad parse: %s", s)
	return i
}

func MustParseInt(s string, base int, bitSize int) int64 {
	i, err := strconv.ParseInt(s, base, bitSize)
	errors.Assert(err == nil, "Bad parse: %s", s)
	return i
}
