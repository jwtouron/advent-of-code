package vec

type Vec2 struct {
	X, Y int
}

func (v Vec2) Add(u Vec2) Vec2 {
	return Vec2{v.X + u.X, v.Y + u.Y}
}
