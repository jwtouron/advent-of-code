package errors

import (
	"fmt"
)

func Assert(cond bool, format string, a ...any) {
	if !cond {
		panic(fmt.Errorf(format, a...))
	}
}

