package iter

import (
	"bufio"
	"iter"
	"os"
)

func FileLines(path string, err *error) iter.Seq[string] {
	return func(yield func(string) bool) {
		var errLocal error
		defer func() {
			if err != nil && errLocal != nil { 
				*err = errLocal
			}
		}()

		var file *os.File
		file, errLocal = os.Open(path)
		if errLocal != nil {
			return
		}

		defer file.Close()

		scanner := bufio.NewScanner(file)
		for scanner.Scan() {
			if !yield(scanner.Text()) {
				return
			}
		}

		errLocal = scanner.Err()
	}
}
