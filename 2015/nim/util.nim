import strformat
import system

proc test*[T](expected: T, actual: T, desc: string) =
  doAssert expected == actual, &"{desc}: expected: {expected}, actual: {actual}"

template tests*(body: untyped) =
  if paramStr(1) == "-t":
    body
    echo "All tests passed."
    quit(0)
