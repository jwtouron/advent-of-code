import os
import streams
import util

proc solvePart1(s: string): int =
  for c in s:
    if c == '(':
      result += 1
    elif c == ')':
      result -= 1
    else:
      doAssert false

proc solvePart2(s: string): int =
  var floor: int = 0
  for i, c in s:
    if c == '(':
      floor += 1
    elif c == ')':
      floor -= 1
      if floor < 0:
        return i + 1
    else:
      doAssert false

when isMainModule:
  tests:
    var input = newFileStream(paramStr(2)).readLine
    test 0, solvePart1("(())"), "Part 1, small 1"
    test 0, solvePart1("()()"), "Part 1, small 2"
    test 3, solvePart1("(()(()("), "Part 1, small 3"
    test 3, solvePart1("))((((("), "Part 1, small 4"
    test -3, solvePart1(")())())"), "Part 1, small 5"
    test 232, solvePart1(input), "Part 1, input"

    test 1, solvePart2(")"), "Part 2, small 1"
    test 5, solvePart2("()())"), "Part 2, small 2"
    test 1783, solvePart2(input), "Part 2, input"

  var input = newFileStream(paramStr(1)).readLine
  echo "Part1: ", solvePart1(input)
  echo "Part2: ", solvePart2(input)
