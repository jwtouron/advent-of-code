#include "util.h"
#include <string.h>

typedef struct {
    int l, w, h;
} Box;

static Box parse_box(const char *line)
{
    Box result;
    char *c = strstr(line, "x");
    *c = 0;
    result.l = atoi(line);
    line = c + 1;
    *c = 'x';
    c = strstr(line, "x");
    *c = 0;
    result.w = atoi(line);
    line = c + 1;
    *c = 'x';
    c = strstr(line, "\n");
    *c = 0;
    result.h = atoi(line);
    *c = '\n';
    return result;
}

static int calc_needed_paper(const Box *box)
{
    int a = box->l * box->h;
    int b = box->l * box->w;
    int c = box->h * box->w;
    int bonus = min(a, min(b, c));
    return a * 2 + b * 2 + c * 2 + bonus;
}

static int solve_part1(char **lines)
{
    int total = 0;
    for (int i = 0; lines[i]; i++) {
        Box b = parse_box(lines[i]);
        total += calc_needed_paper(&b);
    }
    return total;
}

static int calc_needed_ribbon(const Box *box)
{
    int needed = 0;
    if (box->h >= box->w && box->h >= box->l) {
        needed = box->w * 2 + box->l * 2;
    } else if (box->w >= box->h && box->w >= box->l) {
        needed = box->h * 2 + box->l * 2;
    } else if (box->l >= box->w && box->l >= box->h) {
        needed = box->h * 2 + box->w * 2;
    } else {
        CHECK(0, "Should never get here: box->h: %d, box->w: %d, box->l: %d", box->h, box->w, box->l);
    }
    needed += box->w * box->h * box->l;
    return needed;
}

static int solve_part2(char **lines)
{
    int total = 0;
    for (int i = 0; lines[i]; i++) {
        Box b = parse_box(lines[i]);
        total += calc_needed_ribbon(&b);
    }
    return total;
}

static void test(char **lines)
{
    const Test tests[] = {
        {.expected.i = 58, .actual.i = calc_needed_paper(&(Box){2, 3, 4}), Int, "Part1, ex. 1"},
        {.expected.i = 43, .actual.i = calc_needed_paper(&(Box){1, 1, 10}), Int, "Part1, ex. 2"},

        {.expected.i = 1598415, .actual.i = solve_part1(lines), Int, "Part1, input"},

        {.expected.i = 34, .actual.i = calc_needed_ribbon(&(Box){2, 3, 4}), Int, "Part2, ex. 1"},
        {.expected.i = 14, .actual.i = calc_needed_ribbon(&(Box){1, 1, 10}), Int, "Part2, ex. 2"},

        {.expected.i = 3812909, .actual.i = solve_part2(lines), Int, "Part2, input"},

        {{0}}
    };
    run_tests(tests);
}

int main(int argc, char **argv)
{
    CHECK(argc >= 2, "Usage: %s [-t] <input-file>", argv[0]);
    if (strcmp(argv[1], "-t") == 0) {
        CHECK(argc >= 3, "Usage: %s -t <input-file>", argv[0]);
        char **lines = slurp_lines(argv[2]);
        test(lines);
        return 0;
    }
    char **input = slurp_lines(argv[1]);
    printf("part1: %d\n", solve_part1(input));
    printf("part2: %d\n", solve_part2(input));
    return 0;
}

