#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include "util.h"

static int solve_part1(const char *parens)
{
    int floor = 0;
    for (const char *c = parens; *c != 0; c++) {
        if (*c == '(') {
            floor++;
        } else if (*c == ')') {
            floor--;
        }
    }
    return floor;
}

static int solve_part2(const char *parens)
{
    int i = 0, floor = 0;
    for (; i < strlen(parens); i++) {
        if (parens[i] == '(') {
            floor++;
        } else if (parens[i] == ')') {
            floor--;
        }
        if (floor < 0) {
            return i + 1;
        }
    }
    return 0;
}

static void test(const char *input)
{
    const Test tests[] = {
        {.expected.i = 0, .actual.i = solve_part1("(())"), Int, "Part1, ex. 1" },
        {.expected.i = 0, .actual.i = solve_part1("()()"), Int, "Part1, ex. 2" },
        {.expected.i = 3, .actual.i = solve_part1("((("), Int, "Part1, ex. 3" },
        {.expected.i = 3, .actual.i = solve_part1("(()(()("), Int, "Part1, ex. 4" },
        {.expected.i = 3, .actual.i = solve_part1("))((((("), Int, "Part1, ex. 5" },
        {.expected.i = -1, .actual.i = solve_part1("())"), Int, "Part1, ex. 6" },
        {.expected.i = -1, .actual.i = solve_part1("))("), Int, "Part1, ex. 7" },
        {.expected.i = -3, .actual.i = solve_part1(")))"), Int, "Part1, ex. 8" },
        {.expected.i = -3, .actual.i = solve_part1(")())())"), Int, "Part1, ex. 9" },
        {.expected.i = 232, .actual.i = solve_part1(input), Int, "Part1, input" },

        {.expected.i = 1, .actual.i = solve_part2(")"), Int, "Part2, ex. 1" },
        {.expected.i = 5, .actual.i = solve_part2("()())"), Int, "Part2, ex. 2" },
        {.expected.i = 1783, .actual.i = solve_part2(input), Int, "Part2, input" },
        {{0}}
    };
    run_tests(tests);
}

int main(int argc, char **argv)
{
    CHECK(argc >= 2, "Usage: day1 [-t] <input-file>");
    if (strcmp(argv[1], "-t") == 0) {
        CHECK(argc >= 3, "Usage: day1 -t <input-file>");
        char *input = slurp_file(argv[2]);
        test(input);
        return 0;
    }
    char *input = slurp_file(argv[1]);
    printf("part1: %d\n", solve_part1(input));
    printf("part2: %d\n", solve_part2(input));
    return 0;
}
