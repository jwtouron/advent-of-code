#define _GNU_SOURCE
#include "util.h"
#include <stdbool.h>
#include <string.h>
#include <errno.h>

char *slurp_file(const char *path)
{
    FILE *stream = fopen(path, "r");
    CHECK(stream != NULL, "fopen failed: %d", errno);
    fseek(stream, 0, SEEK_END);
    long size = ftell(stream);
    fseek(stream, 0, SEEK_SET);
    char *result = malloc(size + 1);
    size_t read = fread(result, 1, size, stream);
    fclose(stream);
    CHECK(read == size, "fread: Didn't read all data");
    result[size] = 0;
    return result;
}


char **slurp_lines(const char *path)
{
    FILE *stream = fopen(path, "r");
    CHECK(stream != NULL, "fopen failed: %d", errno);
    char **result = 0;
    size_t resultlen = 0;
    ssize_t linelen = 0;
    while (linelen >= 0) {
        char *line = 0;
        size_t n;
        linelen = getline(&line, &n, stream);
        if (linelen >= 0) {
            result = realloc(result, sizeof(char *) * ++resultlen);
            result[resultlen - 1] = line;
        }
    }
    result = realloc(result, sizeof(char *) * ++resultlen);
    result[resultlen - 1] = 0;
    return result;
}

void run_tests(const Test *tests)
{
    bool all_passed = true;
    const Test *test = 0;
    int i = 0;
    for (i = 0, test = tests; test->type != Null; test = &tests[++i]) {
        switch (test->type) {
            case String:
                if (strcmp(test->expected.s, test->actual.s) != 0) {
                    printf("Test failed: %s, expected: %s, actual: %s\n", test->message, test->expected.s, test->actual.s);
                    all_passed = false;
                }
                break;
            case Int:
                if (test->expected.i != test->actual.i) {
                    printf("Test failed: %s, expected: %d, actual: %d\n", test->message, test->expected.i, test->actual.i);
                    all_passed = false;
                }
                break;
            default:
                break;
        }
    }
    if (all_passed) {
        printf("All tests passed.\n");
    }
}
