#ifndef AOC_UTIL_H
#define AOC_UTIL_H

#include <stdio.h>
#include <stdlib.h>

#define min(a, b) ((a) < (b) ? (a) : (b))

#define CHECK(cond, msg, ...) \
    if (!(cond)) { fprintf(stderr, "ERROR: " msg "\n", ##__VA_ARGS__); exit(1); }

typedef enum { Null, String, Int } Type;

typedef union {
    const char *s;
    int i;
} Result;

typedef struct {
    Result expected;
    Result actual;
    Type type;
    const char *message;
} Test;

char *slurp_file(const char *path);
char **slurp_lines(const char *path);

void run_tests(const Test *tests);

#endif
