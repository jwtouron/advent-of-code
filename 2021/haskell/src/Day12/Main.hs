{-# LANGUAGE ImportQualifiedPost, TupleSections #-}

module Main where

import Control.Monad
import Data.Char
import Data.List
import Data.List.Split (splitOn)
import Data.Map.Strict (Map)
import Data.Map.Strict qualified as Map
import Data.Set (Set)
import Data.Set qualified as Set
import Test.HUnit
import Util

import Debug.Trace
traceShow' a = traceShow a a

data Cave = Start | End | SmallCave String | BigCave String
  deriving (Eq,Ord,Show)

isEnd :: Cave -> Bool
isEnd End = True
isEnd _ = False

isSmallCave :: Cave -> Bool
isSmallCave (SmallCave _) = True
isSmallCave _ = False

parseLine :: String -> (Cave, Cave)
parseLine s =
  let ss = splitOn "-" s
  in (f (head ss), f (last ss))
  where
    f "start" = Start
    f "end" = End
    f s | all isUpper s = BigCave s
        | otherwise = SmallCave s

readInput :: String -> Map Cave (Set Cave)
readInput = foldl' f Map.empty . lines
  where
    f m l =
      let (c1, c2) = parseLine l
      in Map.insertWith Set.union c2 (Set.singleton c1) $
         Map.insertWith Set.union c1 (Set.singleton c2) m

findPaths :: (Cave -> [Cave] -> Bool) -> Map Cave (Set Cave) -> [[Cave]]
findPaths filterSmallCaves caveMap = go [[Start]]
  where
    go paths
      | all (isEnd . head) paths = paths
      | otherwise = go $ do
          path <- paths
          case path of
            (End:_) -> pure path
            (c:_)   -> do
              n <- filter (/= Start) $ Set.toList $ caveMap Map.! c
              case n of
                SmallCave _ -> guard $ filterSmallCaves n path
                _ -> pure ()
              pure (n:path)

solvePart1 :: Map Cave (Set Cave) -> Int
solvePart1 = length . findPaths notElem

solvePart2 :: Map Cave (Set Cave) -> Int
solvePart2 = length . findPaths filterSmallCaves
  where
    filterSmallCaves n path = go 0 counts
      where
        counts = Map.elems $ foldl' (\m c -> Map.insertWith (+) c (1 :: Int) m) Map.empty $ filter isSmallCave (n:path)
        go n _ | n >= 2 = False
        go n [] = True
        go n (2:cs) = go (n + 1) cs
        go n (1:cs) = go n cs
        go _ _ = False

main :: IO ()
main = defaultMain readInput solvePart1 solvePart2 tests

example1 :: Map Cave (Set Cave)
example1 =
  readInput "dc-end\n\
\HN-start\n\
\start-kj\n\
\dc-start\n\
\dc-HN\n\
\LN-dc\n\
\HN-end\n\
\kj-sa\n\
\kj-HN\n\
\kj-dc"

example2 :: Map Cave (Set Cave)
example2 =
  readInput "fs-end\n\
\he-DX\n\
\fs-he\n\
\start-DX\n\
\pj-DX\n\
\end-zg\n\
\zg-sl\n\
\zg-pj\n\
\pj-he\n\
\RW-he\n\
\fs-DX\n\
\pj-RW\n\
\zg-RW\n\
\start-pj\n\
\he-WI\n\
\zg-he\n\
\pj-fs\n\
\start-RW"

tests :: Map Cave (Set Cave) -> Test
tests input =
  test [ "day 12" ~: "part 1" ~: "example1" ~: 19 ~=? solvePart1 example1
       , "day 12" ~: "part 1" ~: "example2" ~: 226 ~=? solvePart1 example2
       , "day 12" ~: "part 1" ~: "input" ~: 4773 ~=? solvePart1 input
       , "day 12" ~: "part 2" ~: "example1" ~: 103 ~=? solvePart2 example1
       , "day 12" ~: "part 2" ~: "example2" ~: 3509 ~=? solvePart2 example2
       , "day 12" ~: "part 2" ~: "input" ~: 116985 ~=? solvePart2 input
       ]
