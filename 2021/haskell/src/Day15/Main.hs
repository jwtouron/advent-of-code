{-# LANGUAGE ImportQualifiedPost, TypeApplications #-}

module Main where

import Data.List
import Algorithm.Search (aStar)
import Data.Array.Unboxed (Array)
import Data.Array.Unboxed qualified as Array
import Data.Maybe
import Test.HUnit
import Util

type Input = [[Int]]
type RiskMap = Array (Int, Int) Int

readInput :: String -> Input
readInput = map (map (read . (:[]))) . lines

mkRiskMap :: Input -> RiskMap
mkRiskMap xs =
  Array.array ((0, 0), (length xs - 1, length (xs !! 0) - 1)) $ concat $ zipWith f [0..] xs
  where
    f r = zipWith (\c x -> ((r, c), x)) [0..]

inBounds :: RiskMap -> (Int, Int) -> Bool
inBounds riskMap (r1, c1) =
  let (_, (r2, c2)) = Array.bounds riskMap
  in r1 >= 0 && r1 <= r2 && c1 >= 0 && c1 <= c2

neighbors :: (Int, Int) -> [(Int, Int)]
neighbors (r, c) = [(r + 1, c), (r - 1, c), (r, c + 1), (r, c - 1)]

findCheapestPath :: RiskMap -> Maybe (Int, [(Int, Int)])
findCheapestPath riskMap = aStar neighborStates cost estimate atGoal initialState
  where
    neighborStates pos = filter (inBounds riskMap) $ neighbors pos
    cost _ = (riskMap Array.!)
    estimate (r1, c1) =
      let (_, (r2, c2)) = Array.bounds riskMap
      in abs (r1 - r2) + abs (c1 - c2)
    atGoal pos = pos == (snd $ Array.bounds riskMap)
    initialState = (0, 0)

solvePart1 :: Input -> Int
solvePart1 = fst . fromJust . findCheapestPath . mkRiskMap

applyN :: Int -> (a -> a) -> (a -> a)
applyN = (foldr1 (.) .) . replicate

enlargenInput :: [[Int]] -> [[Int]]
enlargenInput xs =
  concat $
  map (map concat . transpose) $
  take 5 xs' :
  take 5 (tail xs') :
  take 5 (applyN 2 tail $ xs') :
  take 5 (applyN 3 tail $ xs') :
  take 5 (applyN 4 tail $ xs') : []
  where
    xs' = iterate (\xs'' -> xs'' >>= \r -> pure (r >>= \c -> [c `rem` 9 + 1])) xs

solvePart2 :: Input -> Int
solvePart2 = fst . fromJust . findCheapestPath . mkRiskMap . enlargenInput

main :: IO ()
main = defaultMain readInput solvePart1 solvePart2 tests

example :: Input
example =
  readInput "1163751742\n\
            \1381373672\n\
            \2136511328\n\
            \3694931569\n\
            \7463417111\n\
            \1319128137\n\
            \1359912421\n\
            \3125421639\n\
            \1293138521\n\
            \2311944581"

tests :: Input -> Test
tests input =
  test [ "day 15" ~: "part 1" ~: "example" ~: 40 ~=? solvePart1 example
       , "day 15" ~: "part 1" ~: "input" ~: 462 ~=? solvePart1 input
       , "day 15" ~: "part 2" ~: "example" ~: 315 ~=? solvePart2 example
       , "day 15" ~: "part 2" ~: "input" ~: 2846 ~=? solvePart2 input
       ]
