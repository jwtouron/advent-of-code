{-# LANGUAGE BangPatterns #-}

module Main where

import Data.Word()
import Test.HUnit hiding (counts)
import Util

data Bit = Zero | One deriving (Show, Enum, Eq, Ord)

data Freqs = Freqs {ones, zeros :: !Word} deriving (Show)

instance Monoid Freqs where
  mempty = Freqs 0 0

instance Semigroup Freqs where
  (Freqs a1 b1) <> (Freqs a2 b2) = Freqs (a1 + a2) (b1 + b2)

example :: [[Bit]]
example =
  [ [Zero,Zero,One,Zero,Zero]
  , [One,One,One,One,Zero]
  , [One,Zero,One,One,Zero]
  , [One,Zero,One,One,One]
  , [One,Zero,One,Zero,One]
  , [Zero,One,One,One,One]
  , [Zero,Zero,One,One,One]
  , [One,One,One,Zero,Zero]
  , [One,Zero,Zero,Zero,Zero]
  , [One,One,Zero,Zero,One]
  , [Zero,Zero,Zero,One,Zero]
  , [Zero,One,Zero,One,Zero]
  ]

tests :: [[Bit]] -> Test
tests input =
  test [ "day 3" ~: "part 1" ~: "example" ~: 198 ~=? solvePart1 example
       , "day 3" ~: "part 1" ~: "input" ~: 1092896 ~=? solvePart1 input
       , "day 3" ~: "part 2" ~: "example" ~: 230 ~=? solvePart2 example
       , "day 3" ~: "part 2" ~: "input" ~: 4672151 ~=? solvePart2 input
       ]

readInput :: String -> [[Bit]]
readInput = map (map f) . lines
  where
    f '0' = Zero
    f '1' = One
    f x   = error $ "Invalid bit value: " ++ show x

bitStringToInt :: [Bit] -> Int
bitStringToInt = bitStringToInt' 1 0 . reverse
  where
    bitStringToInt' _ !acc [] = acc
    bitStringToInt' n !acc (x:xs) =
      bitStringToInt' (n * 2) (acc + n * fromEnum x) xs

solvePart1 :: [[Bit]] -> Int
solvePart1 xss = bitStringToInt gamma * bitStringToInt epsilon
  where
    (gamma, epsilon) = foldMap f counts
      where
        f (Freqs zs os) =
          if zs > os
          then ([Zero], [One])
          else ([One], [Zero])
    counts = foldr f (map (const mempty) (head xss)) xss
      where
        f xs cts = zipWith g xs cts
          where
            g Zero fs = fs <> (Freqs 1 0)
            g One fs = fs <> (Freqs 0 1)

findRating :: (Word -> Word -> Bool) -> [[Bit]] -> [Bit]
findRating cmp xss = findRating' 0 xss
  where
    findRating' i xss
      | length xss == 1 = head xss
      | otherwise =
        if cmp zs os
        then findRating' (i + 1) (filter ((==Zero) . (!! i)) xss)
        else findRating' (i + 1) (filter ((==One) . (!! i)) xss)
      where
        (Freqs zs os) = foldMap f xss
          where
            f xs = if (xs !! i) == Zero then (Freqs 1 0) else (Freqs 0 1)

solvePart2 :: [[Bit]] -> Int
solvePart2 xss = bitStringToInt o2Rating * bitStringToInt co2Rating
  where
    o2Rating = findRating (>) xss
    co2Rating = findRating (<=) xss

main :: IO ()
main = defaultMain readInput solvePart1 solvePart2 tests
