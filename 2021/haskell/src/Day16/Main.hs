{-# LANGUAGE BangPatterns, ImportQualifiedPost #-}

module Main where

import Data.Maybe
import SimpleParse qualified as P
import SimpleParse hiding (Parser, take)
import Test.HUnit
import Util

type Parser = P.Parser Bit

hexStringToBits :: String -> [Bit]
hexStringToBits [] = []
hexStringToBits (c:cs) = fromJust (lookup c table) ++ hexStringToBits cs
  where
    table =
      [ ('0', [Zero, Zero, Zero, Zero])
      , ('1', [Zero, Zero, Zero, One])
      , ('2', [Zero, Zero, One, Zero])
      , ('3', [Zero, Zero, One, One])
      , ('4', [Zero, One, Zero, Zero])
      , ('5', [Zero, One, Zero, One])
      , ('6', [Zero, One, One, Zero])
      , ('7', [Zero, One, One, One])
      , ('8', [One, Zero, Zero, Zero])
      , ('9', [One, Zero, Zero, One])
      , ('A', [One, Zero, One, Zero])
      , ('B', [One, Zero, One, One])
      , ('C', [One, One, Zero, Zero])
      , ('D', [One, One, Zero, One])
      , ('E', [One, One, One, Zero])
      , ('F', [One, One, One, One])
      , ('\n', [])
      ]

data Bit = Zero | One deriving (Show, Enum, Eq)

data PacketValue
  = Literal Int
  | Operator [Packet]
  deriving (Show)

data Packet =
  Packet { packetVersion :: Int
         , packetTypeId :: Int
         , packetValue :: PacketValue
         } deriving Show

bitsToInt :: [Bit] -> Int
bitsToInt = go 1 0 . reverse
  where
    go _ !acc [] = acc
    go n !acc (b:bs) = go (n * 2) (acc + fromEnum b * n) bs

bitP :: Bit -> Parser Bit
bitP b = satisfies (==b)

literalP :: Parser PacketValue
literalP =
  (\gs g -> Literal $ bitsToInt $ gs ++ g) <$>
  (concat <$> many (groupP One)) <*>
  groupP Zero
  where
    groupP b = (\a b c d -> [a,b,c,d]) <$>
               (bitP b *> token) <*> token <*> token <*> token

-- runParser literalP' $ map (toEnum . read . (:[])) "110100101111111000101000"

operatorP :: Parser PacketValue
operatorP = do
  t <- token
  case t of
    Zero -> zeroP
    One  -> oneP
  where
    zeroP = do
      numBits <- intP 15
      subPacketBits <- P.take numBits token
      pure $ Operator $ fst $ head $ runParser (some packetP) subPacketBits
    oneP = do
      numPackets <- intP 11
      packets <- P.take numPackets packetP
      pure $ Operator packets

intP :: Int -> Parser Int
intP n = bitsToInt <$> P.take n token

packetP :: Parser Packet
packetP = do
  version <- intP 3
  typeId <- intP 3
  case typeId of
    4 -> Packet version typeId <$> literalP
    _ -> Packet version typeId <$> operatorP

solvePart1 :: [Bit] -> Int
solvePart1 = sumVersions . fst . head . runParser packetP
  where
    sumVersions (Packet v _ (Literal _)) = v
    sumVersions (Packet v _ (Operator ps)) = v + sum (map sumVersions ps)

evalPacket :: Packet -> Int
evalPacket (Packet _ 0 (Operator ps)) = sum $ map evalPacket ps
evalPacket (Packet _ 1 (Operator ps)) = product $ map evalPacket ps
evalPacket (Packet _ 2 (Operator ps)) = minimum $ map evalPacket ps
evalPacket (Packet _ 3 (Operator ps)) = maximum $ map evalPacket ps
evalPacket (Packet _ 4 (Literal x)) = x
evalPacket (Packet _ 5 (Operator ps)) =
  (\a b -> if a > b then 1 else 0) <$> head <*> last $ map evalPacket ps
evalPacket (Packet _ 6 (Operator ps)) =
  (\a b -> if a < b then 1 else 0) <$> head <*> last $ map evalPacket ps
evalPacket (Packet _ 7 (Operator ps)) =
  (\a b -> if a == b then 1 else 0) <$> head <*> last $ map evalPacket ps

solvePart2 :: [Bit] -> Int
solvePart2 = evalPacket . fst . head . runParser packetP

devel = hexStringToBits <$> readFile "../input/day16.txt" >>= runTestTT . tests

main :: IO ()
main = defaultMain hexStringToBits solvePart1 solvePart2 tests

example1 :: [Bit]
example1 = hexStringToBits "8A004A801A8002F478"

example2 :: [Bit]
example2 = hexStringToBits "620080001611562C8802118E34"

tests :: [Bit] -> Test
tests input =
  test [ "day 16" ~: "solvePart1" ~: "example1" ~: 16 ~=? solvePart1 example1
       , "day 16" ~: "solvePart1" ~: "example2" ~: 12 ~=? solvePart1 example2
       , "day 16" ~: "solvePart1" ~: "input" ~: 955 ~=? solvePart1 input
       , "day 16" ~: "evalPacket (sum)" ~: "C200B40A82" ~: 3 ~=? evalPacket' "C200B40A82"
       , "day 16" ~: "evalPacket" ~: "sum" ~: 3 ~=? evalPacket' "C200B40A82"
       , "day 16" ~: "evalPacket" ~: "product" ~: 54 ~=? evalPacket' "04005AC33890"
       , "day 16" ~: "evalPacket" ~: "minimum" ~: 7 ~=? evalPacket' "880086C3E88112"
       , "day 16" ~: "evalPacket" ~: "maximum" ~: 9 ~=? evalPacket' "CE00C43D881120"
       , "day 16" ~: "evalPacket" ~: "less than" ~: 1 ~=? evalPacket' "D8005AC2A8F0"
       , "day 16" ~: "evalPacket" ~: "greater than" ~: 0 ~=? evalPacket' "F600BC2D8F"
       , "day 16" ~: "evalPacket" ~: "equals" ~: 0 ~=? evalPacket' "9C005AC2F8F0"
       , "day 16" ~: "evalPacket" ~: "complex" ~: 1 ~=? evalPacket' "9C0141080250320F1802104A08"
       , "day 16" ~: "solvePart2" ~: "input" ~: 158135423448 ~=? solvePart2 input
       ]
  where
    evalPacket' = evalPacket . fst . head . runParser packetP . hexStringToBits
