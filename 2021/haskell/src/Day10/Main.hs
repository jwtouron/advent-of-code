{-# LANGUAGE BangPatterns #-}

module Main where

import Data.List
import Test.HUnit
import Util

data LineStatus
  = Correct
  | Corrupt { unCorrupt :: Char }
  | Incomplete { unIncomplete :: String }
  deriving Show

isCorrupt :: LineStatus -> Bool
isCorrupt (Corrupt _) = True
isCorrupt _ = False

isIncomplete :: LineStatus -> Bool
isIncomplete (Incomplete _) = True
isIncomplete _ = False

analyzeLine :: String -> LineStatus
analyzeLine = go []
  where
    go []          []       = Correct
    go stack       []       = Incomplete stack
    go stack       ('(':ss) = go ('(':stack) ss
    go stack       ('[':ss) = go ('[':stack) ss
    go stack       ('{':ss) = go ('{':stack) ss
    go stack       ('<':ss) = go ('<':stack) ss
    go ('(':stack) (')':ss) = go stack ss
    go _           (')':_)  = Corrupt ')'
    go ('[':stack) (']':ss) = go stack ss
    go _           (']':_)  = Corrupt ']'
    go ('{':stack) ('}':ss) = go stack ss
    go _           ('}':_)  = Corrupt '}'
    go ('<':stack) ('>':ss) = go stack ss
    go _           ('>':_)  = Corrupt '>'
    go _           _        = error "solvePart1.f.go"

solvePart1 :: [String] -> Int
solvePart1 =
  sum . map (score . unCorrupt) . filter isCorrupt . map analyzeLine
  where
    score ')' = 3
    score ']' = 57
    score '}' = 1197
    score '>' = 25137
    score _ = 0

solvePart2 :: [String] -> Int
solvePart2 ss = scores !! (length scores `div` 2)
  where
    scores = sort . map (score 0 . unIncomplete) .  filter isIncomplete . map analyzeLine $ ss
    score acc [] = acc
    score !acc ('(':ss) = score (5 * acc + 1) ss
    score !acc ('[':ss) = score (5 * acc + 2) ss
    score !acc ('{':ss) = score (5 * acc + 3) ss
    score !acc ('<':ss) = score (5 * acc + 4) ss

main :: IO ()
main = defaultMain lines solvePart1 solvePart2 tests

example :: [String]
example =
  [ "[({(<(())[]>[[{[]{<()<>>"
  , "[(()[<>])]({[<{<<[]>>("
  , "{([(<{}[<>[]}>{[]{[(<()>"
  , "(((({<>}<{<{<>}{[]{[]{}"
  , "[[<[([]))<([[{}[[()]]]"
  , "[{[{({}]{}}([{[{{{}}([]"
  , "{<[[]]>}<{[{[{[]{()[[[]"
  , "[<(<(<(<{}))><([]([]()"
  , "<{([([[(<>()){}]>(<<{{"
  , "<{([{{}}[<[[[<>{}]]]>[]]"
  ]

tests :: [String] -> Test
tests input = test
  [ "day 10" ~: "part 1" ~: "example" ~: 26397 ~=? solvePart1 example
  , "day 10" ~: "part 1" ~: "input" ~: 316851 ~=? solvePart1 input
  , "day 20" ~: "part 2" ~: "example" ~: 288957 ~=? solvePart2 example
  , "day 20" ~: "part 2" ~: "input" ~: 2182912364 ~=? solvePart2 input
  ]
