{-# LANGUAGE DuplicateRecordFields, ImportQualifiedPost, RecordWildCards #-}

module Main where

import Data.IntMap.Strict (IntMap)
import Data.IntMap.Strict qualified as IntMap
import Data.List.Split
import Data.Set (Set)
import Data.Set qualified as Set
import Test.HUnit
import Util

data Board =
  Board { numbers :: IntMap (Int, Int)
        , called :: Set (Int, Int)
        , won :: Bool
        , size :: Int
        } deriving (Show)

newBoard :: [[Int]] -> Board
newBoard xss =
  Board {numbers = numbers, called = Set.empty, won = False, size = length xss}
  where
    numbers = IntMap.fromList $ concat $ zipWith f [0..] xss
      where
        f r xs = zipWith g [0..] xs
          where g c x = (x, (r, c))

data Input =
  Input { numbers :: [Int]
        , boards :: [Board]
        } deriving (Show)

parseInput :: String -> Input
parseInput s = Input{..}
  where
    boards = map (newBoard . map (map read . words) . lines) bs
    (ns:bs) = splitOn "\n\n" s
    numbers = map read $ splitOn "," ns

callNumber :: Int -> Board -> Board
callNumber x board@Board{..} =
  case IntMap.lookup x numbers of
    Just pos@(r, c) ->
      let called' = Set.insert pos called
          won' = won ||
                 all id [Set.member (r, c') called' | c' <- [0 .. size - 1]] ||
                 all id [Set.member (r', c) called' | r' <- [0 .. size - 1]]
      in board {called = called', won = won'}
    Nothing -> board

callUntilWinner :: [Int] -> [Board] -> (Board, Int)
callUntilWinner (x:xs) boards =
  case wonBoards of
    (b:_) -> (b, x)
    _     -> callUntilWinner xs boards'
  where
    wonBoards = filter won boards'
    boards' = map (callNumber x) boards
callUntilWinner _ _ = error "callUntilWinner: invalid input"

uncalledNums :: Board -> [Int]
uncalledNums board = map fst $ filter f (IntMap.assocs $ numbers (board :: Board))
  where
    f (_, p) = Set.notMember p (called board)

solvePart1 :: Input -> Int
solvePart1 input = sum (uncalledNums winningBoard) * winningNum
  where
    (winningBoard, winningNum) = callUntilWinner (numbers (input :: Input)) (boards input)

callUntilAllWin :: [Int] -> [Board] -> (Board, Int)
callUntilAllWin = go
  where
    go (x:xs) [lastBoard] =
      let board' = callNumber x lastBoard
      in if won board' then (board', x) else go xs [board']
    go (x:xs) unwonBoards =
      let boards' = filter (not . won) $ map (callNumber x) unwonBoards
      in go xs boards'
    go _ _ = error "callUntilAllWin.go: invalid input"

solvePart2 :: Input -> Int
solvePart2 input = sum (uncalledNums lastBoard) * winningNum
  where
    (lastBoard, winningNum) = callUntilAllWin (numbers (input :: Input)) (boards input)

main :: IO ()
main = defaultMain parseInput solvePart1 solvePart2 tests

example :: Input
example =
  Input { numbers =
    [7,4,9,5,11,17,23,2,0,14,21,24,10,16,13,6,15,25,12,22,18,20,8,19,3,26,1]
        , boards =
          [ newBoard [ [22, 13, 17, 11,  0]
                     , [ 8,  2, 23,  4, 24]
                     , [21,  9, 14, 16,  7]
                     , [ 6, 10,  3, 18,  5]
                     , [ 1, 12, 20, 15, 19]
                     ]
          , newBoard [ [ 3, 15,  0,  2, 22]
                     , [ 9, 18, 13, 17,  5]
                     , [19,  8,  7, 25, 23]
                     , [20, 11, 10, 24,  4]
                     , [14, 21, 16, 12,  6]
                     ]
          , newBoard [ [14, 21, 17, 24,  4]
                     , [10, 16, 15,  9, 19]
                     , [18,  8, 23, 26, 20]
                     , [22, 11, 13,  6,  5]
                     , [ 2,  0, 12,  3,  7]
                     ]
          ]
        }

tests :: Input -> Test
tests input =
  test [ "day 4" ~: "part 1" ~: "example" ~: 4512 ~=? solvePart1 example
       , "day 4" ~: "part 1" ~: "input" ~: 58838 ~=? solvePart1 input
       , "day 4" ~: "part 2" ~: "example" ~: 1924 ~=? solvePart2 example
       , "day 4" ~: "part 2" ~: "input" ~: 6256 ~=? solvePart2 input
       ]
