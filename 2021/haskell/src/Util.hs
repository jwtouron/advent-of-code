module Util where

import System.Environment
import System.Exit
import Test.HUnit hiding (path)

defaultMain :: (Show r1, Show r2)
            => (String -> a)
            -> (a -> r1)
            -> (a -> r2)
            -> (a -> Test)
            -> IO ()
defaultMain readInput solvePart1 solvePart2 tests = do
  progName <- getProgName
  args <- getArgs
  case args of
    ["-t", path] -> do
      input <- readInput <$> readFile path
      runTestTTAndExit $ tests input
    [path] -> do
      input <- readInput <$> readFile path
      print $ solvePart1 input
      print $ solvePart2 input
    _ -> do
      putStrLn $ "Usage: " ++ progName ++ " [-t] <input-path>"
      exitFailure

defaultMain' :: (Show r1, Show r2)
             => a
             -> (a -> r1)
             -> (a -> r2)
             -> (a -> Test)
             -> IO ()
defaultMain' input solvePart1 solvePart2 tests = do
  progName <- getProgName
  args <- getArgs
  case args of
    ["-t"] -> do
      runTestTTAndExit $ tests input
    [] -> do
      print $ solvePart1 input
      print $ solvePart2 input
    _ -> do
      putStrLn $ "Usage: " ++ progName ++ " [-t]"
      exitFailure
