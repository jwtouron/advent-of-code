module Main where

import Util
import Test.HUnit hiding (path)

tests :: [Int] -> Test
tests input =
  test [ "day 1" ~: "part 1" ~: "example" ~: 7 ~=? solvePart1 example
       , "day 1" ~: "part 1" ~: "input" ~: 1400 ~=? solvePart1 input
       , "day 1" ~: "part 2" ~: "example" ~: 5 ~=? solvePart2 example
       , "day 1" ~: "part 2" ~: "input" ~: 1429 ~=? solvePart2 input
       ]

example :: [Int]
example = [ 199
          , 200
          , 208
          , 210
          , 200
          , 207
          , 240
          , 269
          , 260
          , 263
          ]

readInput :: String -> [Int]
readInput = map read . lines

solvePart1 :: [Int] -> Int
solvePart1 = length . filter (\(x,y) -> y > x) . (zip <*> tail)

solvePart2 :: [Int] -> Int
solvePart2 (a:b:c:d:xs) =
  solvePart2 (b:c:d:xs) + if b + c + d > a + b + c then 1 else 0
solvePart2 _ = 0

main :: IO ()
main = defaultMain readInput solvePart1 solvePart2 tests
