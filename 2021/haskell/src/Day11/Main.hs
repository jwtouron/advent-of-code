{-# LANGUAGE BangPatterns, ImportQualifiedPost #-}

module Main where

import Control.Monad.ST (ST, runST)
import Control.Monad.State.Strict
import Data.Vector.Unboxed (Vector)
import Data.Vector.Unboxed qualified as Vec
import Data.Vector.Unboxed.Mutable (MVector)
import Data.Vector.Unboxed.Mutable qualified as MVec
import Test.HUnit
import Util
import Debug.Trace
type Input = Vector Int

data Coord = Coord !Int !Int deriving (Show, Eq, Ord)

instance Enum Coord where
  fromEnum (Coord r c) = r * 10 + c
  toEnum = uncurry Coord . flip quotRem 10

data Octopus = Flashed | Unflashed Int deriving (Show, Eq)

instance Enum Octopus where
  fromEnum Flashed = -1
  fromEnum (Unflashed x) = x
  toEnum (-1) = Flashed
  toEnum x = Unflashed x

readInput :: String -> Input
readInput = Vec.fromList . map (read . (:[])) . concat . lines

neighbors :: Coord -> [Coord]
neighbors coord@(Coord r c) = do
  let fns = [succ, id, pred]
  r' <- map ($ r) fns
  c' <- map ($ c) fns
  let n = Coord r' c'
  guard $ coord /= n
  pure n

inBounds :: Coord -> Bool
inBounds (Coord r c) = r >= 0 && r < 10 && c >= 0 && c < 10

increaseEnergy :: MVector s Int -> Coord -> ST s ()
increaseEnergy vec i = MVec.unsafeModify vec f (fromEnum i)
  where
    f v =
      case toEnum v of
        Flashed -> v
        Unflashed x -> fromEnum (Unflashed (succ x))

flash :: MVector s Int -> ST s ()
flash vec = MVec.ifoldM' f False vec >>= \b -> when b (flash vec)
  where
    f b i v = do
      case toEnum v of
        Unflashed x | x > 9 -> do
          let ns = filter inBounds $ neighbors (toEnum i)
          forM_ ns (increaseEnergy vec)
          MVec.unsafeWrite vec i (fromEnum Flashed)
          pure True
        _ -> pure (b || False)

step :: MVector s Int -> StateT Int (ST s) (MVector s Int)
step vec = do
  lift $ MVec.iforM_ vec $ \i _ -> do
    increaseEnergy vec (toEnum i)
  lift $ flash vec
  MVec.iforM_ vec $ \i v -> do
    when (toEnum v == Flashed) $ do
      MVec.unsafeWrite vec i (fromEnum $ Unflashed 0)
      modify' succ
  pure vec

stepN :: Int -> MVector s Int -> StateT Int (ST s) (MVector s Int)
stepN 0 v = pure v
stepN n vec = step vec >>= stepN (n - 1)

printVec :: Vector Int -> IO ()
printVec vec = do
  Vec.iforM_ vec $ \i v -> do
    putStr $ show $ v
    let (Coord _ c) = toEnum i
    when (c == 9) (putStrLn "")

solvePart1 :: Input -> Int
solvePart1 vec = runST $ Vec.thaw vec >>= flip execStateT 0 . stepN 100

solvePart2 :: Input -> Int
solvePart2 vec = runST $ Vec.thaw vec >>= flip evalStateT 0 . go 0
  where
    go n vec = allZero vec >>= \b ->
      if b then pure n else step vec >>= go (n + 1)
    allZero vec = MVec.foldl' (\b n -> b && n == 0) True vec

main :: IO ()
main = defaultMain readInput solvePart1 solvePart2 tests

example :: Input
example =
  Vec.fromList
  [ 5, 4, 8, 3, 1, 4, 3, 2, 2, 3
  , 2, 7, 4, 5, 8, 5, 4, 7, 1, 1
  , 5, 2, 6, 4, 5, 5, 6, 1, 7, 3
  , 6, 1, 4, 1, 3, 3, 6, 1, 4, 6
  , 6, 3, 5, 7, 3, 8, 5, 4, 7, 8
  , 4, 1, 6, 7, 5, 2, 4, 6, 4, 5
  , 2, 1, 7, 6, 8, 4, 1, 7, 2, 1
  , 6, 8, 8, 2, 8, 8, 1, 1, 3, 4
  , 4, 8, 4, 6, 8, 4, 8, 5, 5, 4
  , 5, 2, 8, 3, 7, 5, 1, 5, 2, 6
  ]

tests :: Input -> Test
tests input =
  test [ "day 11" ~: "part 1" ~: "example" ~: 1656 ~=? solvePart1 example
       , "day 11" ~: "part 1" ~: "input" ~: 1659 ~=? solvePart1 input
       , "day 11" ~: "part 2" ~: "example" ~: 195 ~=? solvePart2 example
       , "day 11" ~: "part 2" ~: "input" ~: 227 ~=? solvePart2 input
       ]
