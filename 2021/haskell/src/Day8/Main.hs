{-# LANGUAGE ImportQualifiedPost, TupleSections #-}

module Main where

import Control.Monad
import Data.List
import Data.Map.Strict (Map)
import Data.Map.Strict qualified as Map
import Data.List.Split (splitOn)
import Test.HUnit
import Util

data Entry =
  Entry {patterns :: [String], outputs :: [String]} deriving (Show)

parseEntry :: String -> Entry
parseEntry =
  (Entry <$> (splitOn " " . head) <*> (splitOn " " . last)) . splitOn " | "

readInput :: String -> [Entry]
readInput = map parseEntry . lines

solvePart1 :: [Entry] -> Int
solvePart1 = sum . map f
  where
    f = length . filter ((`elem` [2, 3, 4, 7]) . length) . outputs

findSegments :: [String] -> [Map Char Char]
findSegments = go Map.empty . sortOn length
  where
    go m [] = [m]
    go m (pattern:patterns') = do
      case pattern of
        [_,_]           -> go' ["cf"]
        [_,_,_]         -> go' ["acf"]
        [_,_,_,_]       -> go' ["bcdf"]
        [_,_,_,_,_]     -> go' ["acdeg", "acdfg", "abdfg"]
        [_,_,_,_,_,_]   -> go' ["abcefg", "abdefg", "abcdfg"]
        [_,_,_,_,_,_,_] -> go' ["abcdefg"]
        _               -> error $ "Invalid pattern: " ++ pattern
      where
        go' digits = do
          perm <- permutations pattern
          digit <- digits
          let kvs = zip digit perm
          guard $ all id $ map (\(k1, k2) -> Map.findWithDefault k2 k1 m == k2) kvs
          let m' = foldl' (\m (k, v) -> Map.insert k v m) m kvs
          go m' patterns'

invertMap :: Ord b => Map a b -> Map b a
invertMap = Map.fromList . map (\(x, y) -> (y, x)) . Map.assocs

translateDigit :: Map Char Char -> String -> [Char]
translateDigit m s =
  case sort $ map (m Map.!) s of
    "abcefg" -> ['0']
    "cf" -> ['1']
    "acdeg" -> ['2']
    "acdfg" -> ['3']
    "bcdf" -> ['4']
    "abdfg" -> ['5']
    "abdefg" -> ['6']
    "acf" -> ['7']
    "abcdefg" -> ['8']
    "abcdfg" -> ['9']
    _ -> []

translateDigits :: Map Char Char -> [String] -> [Int]
translateDigits m = (read <$>) . (sequenceA . map (translateDigit m))

solvePart2 :: [Entry] -> Int
solvePart2 entries = sum $ map f entries
  where
    f e =
      let segMaps = map invertMap $ findSegments (patterns e)
          digits = segMaps >>= flip translateDigits (outputs e)
      in case digits of
           [digits'] -> digits'
           digits' -> error $ "Bad translation: " ++ show digits'

main :: IO ()
main = defaultMain readInput solvePart1 solvePart2 tests

example1 :: Entry
example1 = parseEntry "acedgfb cdfbe gcdfa fbcad dab cefabd cdfgeb eafb cagedb ab | cdfeb fcadb cdfeb cdbaf"

example2 :: [Entry]
example2 =
  [ Entry ["be", "cfbegad", "cbdgef", "fgaecd", "cgeb", "fdcge", "agebfd", "fecdb", "fabcd", "edb"]
          ["fdgacbe", "cefdb", "cefbgd", "gcbe"]
  , Entry ["edbfga", "begcd", "cbg", "gc", "gcadebf", "fbgde", "acbgfd", "abcde", "gfcbed", "gfec"]
          ["fcgedb", "cgb", "dgebacf", "gc"]
  , Entry ["fgaebd", "cg", "bdaec", "gdafb", "agbcfd", "gdcbef", "bgcad", "gfac", "gcb", "cdgabef"]
          ["cg", "cg", "fdcagb", "cbg"]
  , Entry ["fbegcd", "cbd", "adcefb", "dageb", "afcb", "bc", "aefdc", "ecdab", "fgdeca", "fcdbega"]
          ["efabcd", "cedba", "gadfec", "cb"]
  , Entry ["aecbfdg", "fbg", "gf", "bafeg", "dbefa", "fcge", "gcbea", "fcaegb", "dgceab", "fcbdga"]
          ["gecf", "egdcabf", "bgf", "bfgea"]
  , Entry ["fgeab", "ca", "afcebg", "bdacfeg", "cfaedg", "gcfdb", "baec", "bfadeg", "bafgc", "acf"]
          ["gebdcfa", "ecba", "ca", "fadegcb"]
  , Entry ["dbcfg", "fgd", "bdegcaf", "fgec", "aegbdf", "ecdfab", "fbedc", "dacgb", "gdcebf", "gf"]
          ["cefg", "dcbef", "fcge", "gbcadfe"]
  , Entry ["bdfegc", "cbegaf", "gecbf", "dfcage", "bdacg", "ed", "bedf", "ced", "adcbefg", "gebcd"]
          ["ed", "bcgafe", "cdgba", "cbgef"]
  , Entry ["egadfb", "cdbfeg", "cegd", "fecab", "cgb", "gbdefca", "cg", "fgcdab", "egfdb", "bfceg"]
          ["gbdfcae", "bgc", "cg", "cgb"]
  , Entry ["gcafb", "gcf", "dcaebfg", "ecagb", "gf", "abcdeg", "gaef", "cafbge", "fdbac", "fegbdc"]
          ["fgae", "cfgab", "fg", "bagce"]
  ]

tests :: [Entry] -> Test
tests input =
  test [ "day 8" ~: "findSegments" ~: "example1" ~: [[('a','d'),('b','e'),('c','a'),('d','f'),('e','g'),('f','b'),('g','c')]] ~=? map Map.assocs (findSegments (patterns example1))
       , "day 8" ~: "part 1" ~: "example" ~: 26 ~=? solvePart1 example2
       , "day 8" ~: "part 1" ~: "input" ~: 352 ~=? solvePart1 input
       , "day 8" ~: "part 2" ~: "example" ~: 61229 ~=? solvePart2 example2
       -- , "day 8" ~: "part 2" ~: "input" ~: 936117 ~=? solvePart2 input
       ]
