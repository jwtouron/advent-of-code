{-# LANGUAGE LambdaCase #-}

module SimpleParse
  ( module Control.Applicative
  , Parser
  , satisfies
  , SimpleParse.take
  , runParser
  , token
  )
  where

import Control.Applicative
import Control.Monad

newtype Parser a r = Parser { runParser :: [a] -> [(r, [a])] }

instance Functor (Parser a) where
  fmap f (Parser p) =
    Parser $ \input -> fmap (\(a, b) -> (f a, b)) (p input)

instance Applicative (Parser a) where
  pure v = Parser $ \input -> [(v, input)]
  (<*>) = ap

instance Monad (Parser a) where
  return = pure
  p >>= f = Parser $ \input -> concatMap (\(x, s) -> runParser (f x) s) (runParser p input)

instance Alternative (Parser a) where
  empty = Parser $ \_ -> []
  p1 <|> p2 = Parser $ \input -> runParser p1 input ++ runParser p2 input

token :: Parser a a
token = Parser $ \case
          []    -> []
          [x]   -> [(x, [])]
          input -> [(head input, tail input)]

satisfies :: (a -> Bool) -> Parser a a
satisfies p = token >>= \t -> guard (p t) >> pure t

take :: Int -> Parser a r -> Parser a [r]
take n _ | n < 0 = empty
take n p = go n
  where
    go 0 = pure []
    go n = (:) <$> p <*> go (n - 1)

