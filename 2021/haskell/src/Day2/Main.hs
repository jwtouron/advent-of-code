{-# LANGUAGE BangPatterns #-}

module Main where

import Data.List
import Test.HUnit
import Text.Regex.TDFA
import Util

example = [Forward 5, Down 5, Forward 8, Up 3, Down 8, Forward 2]

tests input =
  test [ "day 2" ~: "part 1" ~: "example" ~: 150 ~=? solvePart1 example
       , "day 2" ~: "part 1" ~: "input" ~: 1714680 ~=? solvePart1 input
       , "day 2" ~: "part 2" ~: "example" ~: 900 ~=? solvePart2 example
       , "day 2" ~: "part 2" ~: "input" ~: 1963088820 ~=? solvePart2 input
       ]

data Command = Forward Int | Down Int | Up Int deriving (Show)

commandRegex :: Regex
commandRegex = makeRegex "[a-z]+ +"

parseCommand :: String -> Command
parseCommand s =
  case match commandRegex s :: (String, String, String) of
    ("", 'f':'o':'r':'w':'a':'r':'d':_, x) -> Forward (read x)
    ("", 'u':'p':_, x) -> Up (read x)
    ("", 'd':'o':'w':'n':_, x) -> Down (read x)

data Position = Pos {horiz :: Int, depth :: Int, aim :: Int} deriving (Show)

readInput :: String -> [Command]
readInput = map parseCommand . lines

solvePart1 :: [Command] -> Int
solvePart1 commands = horiz pos * depth pos
  where
    pos = foldl' f (Pos 0 0 0) commands
    f pos@(Pos !h _ _) (Forward x) = pos {horiz = h + x}
    f pos@(Pos _ !d _) (Down x) = pos {depth = d + x}
    f pos@(Pos _ !d _) (Up x) = pos {depth = d - x}

solvePart2 :: [Command] -> Int
solvePart2 commands = horiz pos * depth pos
  where
    pos = foldl' f (Pos 0 0 0) commands
    f pos@(Pos !h !d !a) (Forward x) = pos {horiz = h + x, depth = d + a * x}
    f pos@(Pos _ _ !a) (Down x) = pos {aim = a + x}
    f pos@(Pos _ _ !a) (Up x) = pos {aim = a - x}

main :: IO ()
main = defaultMain readInput solvePart1 solvePart2 tests
