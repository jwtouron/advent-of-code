{-# LANGUAGE ImportQualifiedPost #-}

module Main where

import Data.Set (Set)
import Data.Set qualified as Set
import Data.List (foldl')
import Data.List.Split (splitOn)
import Test.HUnit
import Util

data Fold = FoldX Int | FoldY Int deriving Show

data Input =
  Input { dots :: [(Int, Int)]
        , folds :: [Fold]
        } deriving Show

readInput :: String -> Input
readInput = (Input <$> (parseDots . head) <*> (parseFolds . last)) . splitOn "\n\n"
  where
    parseDots = map (((,) <$> head <*> last) . map read . splitOn ",") . lines
    parseFolds = map f . lines
      where
        f s = case splitOn "=" $ (!! 2) $ splitOn " " $ s of
                ["x", ns] -> FoldX (read ns)
                ["y", ns] -> FoldY (read ns)

type Paper = Set (Int, Int)

fold :: Paper -> Fold -> Paper
fold paper (FoldY n) = foldl' f Set.empty $ Set.toList paper
  where
    f s (x, y) =
      if y > n then Set.insert (x, n - (y - n)) s else Set.insert (x, y) s
fold paper (FoldX n) = foldl' f Set.empty $ Set.toList paper
  where
    f s (x, y) =
      if x > n then Set.insert (n - (x - n), y) s else Set.insert (x, y) s

solvePart1 :: Input -> Int
solvePart1 paper = Set.size $ fold (Set.fromList (dots paper)) (head $ folds paper)

drawPaper :: Paper -> String
drawPaper paper = unlines $ map f [0..maxY]
  where
    f y = map f [0..maxX]
      where
        f x = if Set.member (x, y) paper then '#' else '.'
    (maxX, maxY) = Set.foldl' f (0 :: Int, 0 :: Int) paper
      where
        f (w, h) (x, y) = (max x w, max y h)

solvePart2 :: Input -> String
solvePart2 paper = drawPaper $ foldl' fold (Set.fromList (dots paper)) (folds paper)

main :: IO ()
main = defaultMain readInput solvePart1 solvePart2 tests

example :: Input
example =
  Input [ (6,10)
        , (0,14)
        , (9,10)
        , (0,3)
        , (10,4)
        , (4,11)
        , (6,0)
        , (6,12)
        , (4,1)
        , (0,13)
        , (10,12)
        , (3,4)
        , (3,0)
        , (8,4)
        , (1,10)
        , (2,14)
        , (8,10)
        , (9,0)
        ]
        [FoldY 7, FoldX 5]

tests :: Input -> Test
tests input =
  test [ "day 13" ~: "part 1 " ~: "example" ~: 17 ~=? solvePart1 example
       , "day 13" ~: "part 1 " ~: "input" ~: 785 ~=? solvePart1 input
       , "day 13" ~: "part 2 " ~: "input" ~: part2Expected ~=? solvePart2 input
       ]
    where
      part2Expected = "####...##..##..#..#...##..##...##..#..#\n#.......#.#..#.#..#....#.#..#.#..#.#..#\n###.....#.#..#.####....#.#....#..#.####\n#.......#.####.#..#....#.#.##.####.#..#\n#....#..#.#..#.#..#.#..#.#..#.#..#.#..#\n#.....##..#..#.#..#..##...###.#..#.#..#\n"
