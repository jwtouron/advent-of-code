{-# LANGUAGE ImportQualifiedPost, TupleSections #-}

module Main where

import Control.Monad
import Control.Monad.ST
import Data.List.Split (splitOn)
import Data.Vector.Unboxed.Mutable (MVector)
import Data.Vector.Unboxed.Mutable qualified as MVec
import Data.Vector.Unboxed (Vector)
import Data.Vector.Unboxed qualified as Vec
import Test.HUnit
import Util

step :: MVector s Int -> ST s (MVector s Int)
step vec = do
  assocs <- forM [0..8] $ \i -> do
    if i == 6
      then (\x y -> (6, x + y)) <$> MVec.read vec 0 <*> MVec.read vec 7
      else (i,) <$> MVec.read vec (rem (i + 1) 9)
  forM_ assocs $ \(i, x) -> do
    MVec.write vec i x
  pure vec

solve :: Int -> [Int] -> Int
solve n xs = Vec.sum $ runST $ do
  vec <- Vec.unsafeThaw . initVec $ xs
  go n vec
  where
    go :: Int -> MVector s Int -> ST s (Vector Int)
    go 0 xs = Vec.unsafeFreeze xs
    go n xs = step xs >>= go (n - 1)

initVec :: [Int] -> Vector Int
initVec xs = Vec.create $ do
  vec <- MVec.new 9
  forM_ xs $ \x -> MVec.read vec x >>= MVec.write vec x . (+1)
  pure vec

readInput :: String -> [Int]
readInput = map read . splitOn ","

solvePart1 :: [Int] -> Int
solvePart1 = solve 80

solvePart2 :: [Int] -> Int
solvePart2 = solve 256

main :: IO ()
main = defaultMain readInput solvePart1 solvePart2 tests

example :: [Int]
example = [3,4,3,1,2]

tests :: [Int] -> Test
tests input =
  test [ "day 6" ~: "part 1" ~: "example" ~: 5934 ~=? solvePart1 example
       , "day 6" ~: "part 1" ~: "input" ~: 343441 ~=? solvePart1 input
       , "day 6" ~: "part 2" ~: "example" ~: 26984457539 ~=? solvePart2 example
       , "day 6" ~: "part 2" ~: "input" ~: 1569108373832 ~=? solvePart2 input
       ]
-- readInput <$> readFile "../input/day6.txt" >>= runTestTT . tests
