{-# LANGUAGE ImportQualifiedPost #-}

module Main where

import Data.List (sort)
import Data.Map.Strict (Map)
import Data.Map.Strict qualified as Map
import Test.HUnit
import Text.Regex.TDFA
import Util

type Coord = (Int, Int)
type Input = [(Coord, Coord)]

inputRegex :: Regex
inputRegex = makeRegex "([[:digit:]]+),([[:digit:]]+) +-> +([[:digit:]]+),([[:digit:]]+)"

parseLine :: String -> (Coord, Coord)
parseLine s =
  case match inputRegex s :: (String, String, String, [String]) of
    (_, _, _, [x1, y1, x2, y2]) -> ((read x1, read y1), (read x2, read y2))
    _ -> error "parseLine: invalid input"

parseInput :: String -> Input
parseInput = map parseLine . lines

data HandledStatus = Handled (Map Coord Int) | Unhandled

runHandledStatus :: HandledStatus -> Map Coord Int
runHandledStatus (Handled m) = m
runHandledStatus _ = error "Shouldn't happen"

newtype Rule =
  Rule { runRule :: (Coord, Coord) -> Map Coord Int -> HandledStatus }

instance Semigroup Rule where
  Rule f1 <> Rule f2 =
    Rule $ \cs m ->
      case f1 cs m of
        handled@(Handled _) -> handled
        Unhandled -> f2 cs m

verticalRule :: Rule
verticalRule =
  Rule $ \((x1, y1), (x2, y2)) m ->
    if x1 == x2
      then Handled $ foldr (\y m -> Map.insertWith (+) (x1, y) 1 m) m [min y1 y2 .. max y1 y2]
      else Unhandled

horizontalRule :: Rule
horizontalRule =
  Rule $ \((x1, y1), (x2, y2)) m ->
    if y1 == y2
      then Handled $ foldr (\x m -> Map.insertWith (+) (x, y1) 1 m) m [min x1 x2 .. max x1 x2]
      else Unhandled

diagonalRule :: Rule
diagonalRule =
  Rule $ \(c1, c2) m ->
    if slope c1 c2 `elem` [1, (-1)]
      then Handled $ foldr (\(x, y) m -> Map.insertWith (+) (x, y) 1 m) m (diagCoords c1 c2)
      else Unhandled

handleAllRule :: Rule
handleAllRule = Rule $ \_ m -> Handled m

solvePart1 :: Input -> Int
solvePart1 = length . filter (>1) . Map.elems . foldr f Map.empty
  where
    f = (runHandledStatus .) . (runRule $ verticalRule <> horizontalRule <> handleAllRule)

slope :: Coord -> Coord -> Int
slope (x1, y1) (x2, y2) = (y2 - y1) `div` (x2 - x1)

diagCoords :: Coord -> Coord -> [(Coord)]
diagCoords c1' c2' = go c1
  where
    [c1, c2] = [min c1' c2', max c1' c2']
    slope' = slope c1 c2
    go c@(x, y)
      | c == c2 = [c]
      | otherwise = c : go (x + 1, y + slope')

solvePart2 :: Input -> Int
solvePart2 = length . filter (>1) . Map.elems . foldr f Map.empty
  where
    f = (runHandledStatus .) . (runRule $ verticalRule <> horizontalRule <> diagonalRule <> handleAllRule)

main :: IO ()
main = defaultMain parseInput solvePart1 solvePart2 tests

example :: Input
example = map parseLine
  [ "0,9 -> 5,9"
  , "8,0 -> 0,8"
  , "9,4 -> 3,4"
  , "2,2 -> 2,1"
  , "7,0 -> 7,4"
  , "6,4 -> 2,0"
  , "0,9 -> 2,9"
  , "3,4 -> 1,4"
  , "0,0 -> 8,8"
  , "5,5 -> 8,2"
  ]

tests :: Input -> Test
tests input =
  test [ "day 5" ~: "part 1" ~: "example" ~: 5 ~=? solvePart1 example
       , "day 5" ~: "part 1" ~: "input" ~: 5169 ~=? solvePart1 input
       , "day 5" ~: "part 2" ~: "example" ~: 12 ~=? solvePart2 example
       , "day 5" ~: "part 2" ~: "input" ~: 22083 ~=? solvePart2 input
       ]
