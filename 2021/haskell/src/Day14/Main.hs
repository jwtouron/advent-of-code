{-# LANGUAGE ImportQualifiedPost #-}

module Main where

import Data.List.Split (splitOn)
import Data.Map.Strict (Map)
import Data.Map.Strict qualified as Map
import Test.HUnit
import Util

data Pair = Pair { pairFst, pairSnd :: !Char } deriving (Ord, Eq, Show)

type InsertionRules = Map Pair Char

data Input =
  Input { template :: String, rules :: InsertionRules } deriving (Show)

readInput :: String -> Input
readInput = (Input <$> head <*> parseRules . last) . splitOn "\n\n"
  where
    parseRules :: String -> InsertionRules
    parseRules =
       Map.fromList . map (((,) <$> ((Pair <$> head <*> last) . head) <*> head . last) . splitOn " -> ") . lines

step :: InsertionRules -> Map Pair Int -> Map Pair Int
step rules = Map.fromListWith (+) . Map.foldrWithKey f []
  where
    f p c ps =
      (Pair (pairFst p) (rules Map.! p), c) :
      (Pair (rules Map.! p) (pairSnd p), c) : ps

stepN :: Int -> InsertionRules -> Map Pair Int -> Map Pair Int
stepN 0 _ counts = counts
stepN n rules counts = stepN (n - 1) rules (step rules counts)

solve :: Int -> Input -> Int
solve n input = subtract <$> minimum <*> maximum $ finalCharCounts
  where
    initPairCounts =
      Map.fromListWith (+) $
      zipWith (\a b -> (Pair a b, 1)) (template input) (tail (template input))
    finalCharCounts =
      Map.insertWith (+) (last $ template input) 1 $
      Map.mapKeysWith (+) (\(Pair a _) -> a) $
      stepN n (rules input) initPairCounts

solvePart1 :: Input -> Int
solvePart1 = solve 10

solvePart2 :: Input -> Int
solvePart2  = solve 40

main :: IO ()
main = defaultMain readInput solvePart1 solvePart2 tests

example :: Input
example =
  Input "NNCB" $
        Map.fromList [ (Pair 'C' 'H', 'B')
                     , (Pair 'H' 'H', 'N')
                     , (Pair 'C' 'B', 'H')
                     , (Pair 'N' 'H', 'C')
                     , (Pair 'H' 'B', 'C')
                     , (Pair 'H' 'C', 'B')
                     , (Pair 'H' 'N', 'C')
                     , (Pair 'N' 'N', 'C')
                     , (Pair 'B' 'H', 'H')
                     , (Pair 'N' 'C', 'B')
                     , (Pair 'N' 'B', 'B')
                     , (Pair 'B' 'N', 'B')
                     , (Pair 'B' 'B', 'N')
                     , (Pair 'B' 'C', 'B')
                     , (Pair 'C' 'C', 'N')
                     , (Pair 'C' 'N', 'C')
                     ]

tests :: Input -> Test
tests input =
  test [ "day 14" ~: "part 1" ~: "example" ~: 1588 ~=? solvePart1 example
       , "day 14" ~: "part 1" ~: "input" ~: 3095 ~=? solvePart1 input
       , "day 14" ~: "part 2" ~: "example" ~: 2188189693529 ~=? solvePart2 example
       , "day 14" ~: "part 2" ~: "input" ~: 3152788426516 ~=? solvePart2 input
       ]
