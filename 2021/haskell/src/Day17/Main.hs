{-# LANGUAGE MultiWayIf, RecordWildCards #-}

module Main where

import Data.Foldable
import Test.HUnit
import Util

data TargetArea =
  TargetArea { taMinX, taMaxX, taMinY, taMaxY :: Int }
  deriving (Show)

data Probe =
  Probe { probeXPos, probeYPos, probeXVel, probeYVel :: Int }
  deriving (Show)

step :: Probe -> Probe
step Probe{..} = Probe probeXPos' probeYPos' probeXVel' probeYVel'
  where
    probeXPos' = probeXPos + probeXVel
    probeYPos' = probeYPos + probeYVel
    probeXVel' = if | probeXVel == 0 -> 0
                    | probeXVel > 0 -> probeXVel - 1
                    | probeXVel < 0 -> probeXVel + 1
    probeYVel' = probeYVel - 1

inTargetArea :: TargetArea -> Probe -> Bool
inTargetArea TargetArea{..} Probe{..} =
  probeXPos >= taMinX && probeXPos <= taMaxX && probeYPos >= taMinY && probeYPos <= taMaxY

findAllinitVelocities :: TargetArea -> [[Probe]]
findAllinitVelocities targetArea@TargetArea{..} =
  filter (any (inTargetArea targetArea)) $
  map stepProbeMany initProbes
  where
    stepProbeMany = takeWhile isValid . iterate step
    isValid Probe{..} = (probeXPos <= taMaxX && probeYPos >= taMinY)
    initProbes = [Probe 0 0 x y | x <- [(taMaxX `div` 10) .. taMaxX], y <- [taMinY .. abs (taMinY)]]

solvePart1 :: TargetArea -> Int
solvePart1 = probeYPos . highestY . map highestY . findAllinitVelocities
  where
    highestY = maximumBy (\p1 p2 -> probeYPos p1 `compare` probeYPos p2)

solvePart2 :: TargetArea -> Int
solvePart2 = length . findAllinitVelocities

main :: IO ()
main = defaultMain' input solvePart1 solvePart2 tests

example :: TargetArea
example = TargetArea 20 30 (-10) (-5)

input :: TargetArea
input = TargetArea 137 171 (-98) (-73)

tests :: TargetArea -> Test
tests input =
  test [ "day 17" ~: "solvePart1" ~: "example" ~: 45 ~=? solvePart1 example
       , "day 17" ~: "solvePart1" ~: "input" ~: 4753 ~=? solvePart1 input
       , "day 17" ~: "solvePart2" ~: "example" ~: 112 ~=? solvePart2 example
       , "day 17" ~: "solvePart2" ~: "input" ~: 1546 ~=? solvePart2 input
       ]
