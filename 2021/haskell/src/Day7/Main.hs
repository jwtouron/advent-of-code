module Main where

import Data.List.Split (splitOn)
import Test.HUnit
import Util

readInput :: String -> [Int]
readInput = map read . splitOn ","

solve :: (Int -> Int -> Int) -> [Int] -> Int
solve fuelCost xs = minimum $ map f [mn .. mx]
  where
    f x = sum $ map g xs
      where
        g y = fuelCost x y
    mn = minimum xs
    mx = maximum xs

solvePart1 :: [Int] -> Int
solvePart1 = solve ((abs .) . subtract)

solvePart2 :: [Int] -> Int
solvePart2 xs = solve f xs
  where
    f x y =
      let n = abs (subtract x y)
      in n * (n + 1) `div` 2

main :: IO ()
main = defaultMain readInput solvePart1 solvePart2 tests

example :: [Int]
example = [16,1,2,0,4,2,7,1,2,14]

tests :: [Int] -> Test
tests input =
  test [ "day 7" ~: "part 1" ~: "example" ~: 37 ~=? solvePart1 example
       , "day 7" ~: "part 1" ~: "input" ~: 348996 ~=? solvePart1 input
       , "day 7" ~: "part 2" ~: "example" ~: 168 ~=? solvePart2 example
       , "day 7" ~: "part 2" ~: "input" ~: 98231647 ~=? solvePart2 input
       ]
