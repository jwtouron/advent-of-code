{-# LANGUAGE BangPatterns #-}

module Main where

import Data.Char
import Data.List
import Data.Maybe
import Test.HUnit

data Token
  = OpenBkt
  | CloseBkt
  | Val Int
  deriving (Show)

data Zipper a =
  Zipper { zipperLefts :: [a]
         , zipperCurr :: a
         , zipperRights :: [a]
         } deriving (Show)

mkZipper :: [a] -> Zipper a
mkZipper (t:tks) = Zipper [] t tks

zipperLeft :: Zipper a -> Zipper a
zipperLeft zipper@(Zipper [] _ _) = zipper
zipperLeft (Zipper ls c rs) = Zipper (tail ls) (head ls) (c:rs)

zipperRight :: Zipper a -> Zipper a
zipperRight zipper@(Zipper _ _ []) = zipper
zipperRight (Zipper ls c rs) = Zipper (c:ls) (head rs) (tail rs)

zipperReset :: Zipper a -> Zipper a
zipperReset zipper@(Zipper [] _ _) = zipper
zipperReset zipper = zipperReset $ zipperLeft zipper

unzipper :: Zipper a -> [a]
unzipper z =
  let (Zipper _ c rs) = zipperReset z
  in (c:rs)

data Number = Nbr { nbrVal :: !Int, nbrDepth :: !Int } deriving (Show, Eq)

parseLine' :: String -> [Number]
parseLine' = go 0
  where
    go _ [] = []
    go depth ('[':s) = go (depth + 1) s
    go depth (']':s) = go (depth - 1) s
    go depth (' ':s) = go depth s
    go depth (',':s) = go depth s
    go depth ns =
      let (as, bs) = span isDigit ns
      in Nbr (read as) depth : go depth bs

parseLine :: String -> [Token]
parseLine = mapMaybe f
  where
    f ',' = Nothing
    f ' ' = Nothing
    f '[' = Just OpenBkt
    f ']' = Just CloseBkt
    f d = Just (Val $ read $ d:[])

add' :: [Number] -> [Number] -> [Number]
add' xs ys = map f xs ++ map f ys
  where f nbr@(Nbr _ d) = nbr { nbrDepth = (d + 1) }

add :: [Token] -> [Token] -> [Token]
add tks1 tks2 = [OpenBkt] ++ tks1 ++ tks2 ++ [CloseBkt]

explode' :: [Number] -> Maybe [Number]
explode' ns = go [] (head ns) (tail ns)
  where
    go :: [Number] -> Number -> [Number] -> Maybe [Number]
    go _ _ [] = Nothing
    go prev (Nbr x dx) (Nbr y dy:next)
      | dx == dy && dx >= 5 =
          Just $ (reverse (add x prev)) ++ [(Nbr 0 (dx - 1))] ++ (add y next)
      where
        add _ [] = []
        add n (Nbr x d:ns) = Nbr (n + x) d:ns
    go prev curr (n:next) = go (curr:prev) n next

explode :: Zipper Token -> Maybe (Zipper Token)
explode = go . zipperReset
  where
    go (Zipper _ _ []) = Nothing
    go (Zipper ls OpenBkt (Val x:Val y:CloseBkt:rs)) =
      Just (Zipper (add x ls) (Val 0) (add y rs))
      where
        add n [] = []
        add n (Val t:tks) = (Val (t + n):tks)
        add n (t:tks) = t : add n tks
    go z = go $ zipperRight z

split' :: [Number] -> Maybe [Number]
split' [] = Nothing
split' (Nbr x d:ns)
  | x >= 10 =
    let (q, r) = quotRem x 2
    in Just $ Nbr q (d + 1) : Nbr (q + r) (d + 1) : ns
split' (n:ns) = (n:) <$> split' ns

devel = map parseLine' . lines <$> readFile "../input/day18.txt" >>= runTestTT . tests
-- solvePart1 example
  -- split' $ parseLine' "[[[[0,7],4],[15,[0,13]]],[1,1]]"
  --split' $ fromJust $ explode' $ fromJust $ explode' $ add' (parseLine' "[[[[4,3],4],4],[7,[[8,4],9]]]") (parseLine' "[1,1]")

split :: Zipper Token -> Zipper Token
split z@(Zipper _ _ []) = z
split (Zipper ls (Val x) rs) | x >= 10 = Zipper ls OpenBkt (Val a:Val b:CloseBkt:rs)
  where
    a = x `div` 2
    b = a + if x `rem` 2 == 0 then 0 else 1
split z = split $ zipperRight z

calcMagnitude' :: [Number] -> Int
calcMagnitude' = go . mkZipper
  where
    go (Zipper [] (Nbr x _) []) = x
    go z@(Zipper _ _ []) = go $ zipperReset z
    go (Zipper ls (Nbr x dx) (Nbr y dy:rs))
      | dx == dy = go $ zipperReset $ Zipper ls (Nbr (x * 3 + y * 2) (dx - 1)) rs
    go z = go $ zipperRight z

calcMagnitute :: Zipper Token -> Int
calcMagnitute = go 0 . zipperReset
  where
    go !acc (Zipper _ _ []) = acc
    go !acc z@(Zipper ls (Val x) (Val y:tks)) =
      let mag = 3 * x + 2 * y
      in go (acc + mag) (Zipper ls (Val mag) tks)

devel'' = split $ mkZipper [OpenBkt, Val 11, CloseBkt]
devel' = zipperReset $ fromJust $ explode $ mkZipper $ parseLine "[[[[[9,8],1],2],3],4]"
-- [[[[[9,8],1],2],3],4] becomes [[[[0,9],2],3],4] (the 9 has no regular number to its left, so it is not added to any regular number).

-- solvePart1 :: [[Number]] -> Int
sumNumbers = foldl1' (\ns1 ns2 -> reduce $ add' ns1 ns2)
  where
    reduce ns = case (explode' ns, split' ns) of
                  (Just ns', _) -> reduce ns'
                  (Nothing, Just ns') -> reduce ns'
                  (Nothing, Nothing) -> ns

solvePart1 = calcMagnitude' . sumNumbers
-- devel = solvePart1 example
-- 4231 too high
main :: IO ()
main = undefined

example1 :: [[Number]]
example1 = map parseLine' (lines s)
  where
    s = "[[[0,[4,5]],[0,0]],[[[4,5],[2,6]],[9,5]]]\n\
        \[7,[[[3,7],[4,3]],[[6,3],[8,8]]]]\n\
        \[[2,[[0,8],[3,4]]],[[[6,7],1],[7,[1,6]]]]\n\
        \[[[[2,4],7],[6,[0,5]]],[[[6,8],[2,8]],[[2,1],[4,5]]]]\n\
        \[7,[5,[[3,8],[1,4]]]]\n\
        \[[2,[2,2]],[8,[8,1]]]\n\
        \[2,9]\n\
        \[1,[[[9,3],9],[[9,0],[0,7]]]]\n\
        \[[[5,[7,4]],7],1]\n\
        \[[[[4,2],2],6],[8,7]]"

example2 :: [[Number]]
example2 = map parseLine' (lines s)
  where
    s = "[[[0,[5,8]],[[1,7],[9,6]]],[[4,[1,2]],[[1,4],2]]]\n\
        \[[[5,[2,8]],4],[5,[[9,9],0]]]\n\
        \[6,[[[6,2],[5,6]],[[7,6],[4,7]]]]\n\
        \[[[6,[0,7]],[0,9]],[4,[9,[9,0]]]]\n\
        \[[[7,[6,4]],[3,[1,3]]],[[[5,5],1],9]]\n\
        \[[6,[[7,3],[3,2]]],[[[3,8],[5,7]],4]]\n\
        \[[[[5,4],[7,7]],8],[[8,3],8]]\n\
        \[[9,3],[[9,9],[6,[4,9]]]]\n\
        \[[2,[[7,7],7]],[[5,8],[[9,3],[0,2]]]]\n\
        \[[[[5,2],5],[8,[3,7]]],[[5,[7,5]],[4,4]]]"

tests :: [[Number]] -> Test
tests input =
  test [ "day 18" ~: "add" ~: parseLine' "[[1,2],[[3,4],5]]" ~=? add' (parseLine' "[1,2]") (parseLine' "[[3,4],5]")

       , "day 18" ~: "explode" ~: "example 1" ~: Just (parseLine' "[[[[0,9],2],3],4]") ~=? explode' (parseLine' "[[[[[9,8],1],2],3],4]")
       , "day 18" ~: "explode" ~: "example 2" ~: Just (parseLine' "[7,[6,[5,[7,0]]]]") ~=? explode' (parseLine' "[7,[6,[5,[4,[3,2]]]]]")
       , "day 18" ~: "explode" ~: "example 3" ~: Just (parseLine' "[[6,[5,[7,0]]],3]") ~=? explode' (parseLine' "[[6,[5,[4,[3,2]]]],1]")
       , "day 18" ~: "explode" ~: "example 4" ~: Just (parseLine' "[[3,[2,[8,0]]],[9,[5,[4,[3,2]]]]]") ~=? explode' (parseLine' "[[3,[2,[1,[7,3]]]],[6,[5,[4,[3,2]]]]]")
       , "day 18" ~: "explode" ~: "example 5" ~: Just (parseLine' "[[3,[2,[8,0]]],[9,[5,[7,0]]]]") ~=? explode' (parseLine' "[[3,[2,[8,0]]],[9,[5,[4,[3,2]]]]]")

       , "day 18" ~: "sumNumbers" ~: "small example 1" ~: parseLine' "[[[[0,7],4],[[7,8],[6,0]]],[8,1]]" ~=? sumNumbers ([parseLine' "[[[[4,3],4],4],[7,[[8,4],9]]]", parseLine' "[1,1]"])
       , "day 18" ~: "sumNumbers" ~: "small example 2" ~: parseLine' "[[[[1,1],[2,2]],[3,3]],[4,4]]" ~=? sumNumbers (map parseLine' $ lines "[1,1]\n[2,2]\n[3,3]\n[4,4]")
       , "day 18" ~: "sumNumbers" ~: "small example 3" ~: parseLine' "[[[[3,0],[5,3]],[4,4]],[5,5]]" ~=? sumNumbers (map parseLine' $ lines "[1,1]\n[2,2]\n[3,3]\n[4,4]\n[5,5]")
       , "day 18" ~: "sumNumbers" ~: "small example 4" ~: parseLine' "[[[[5,0],[7,4]],[5,5]],[6,6]]" ~=? sumNumbers (map parseLine' $ lines "[1,1]\n[2,2]\n[3,3]\n[4,4]\n[5,5]\n[6,6]")
       , "day 18" ~: "sumNumbers" ~: "example1" ~: parseLine' "[[[[8,7],[7,7]],[[8,6],[7,7]]],[[[0,7],[6,6]],[8,7]]]" ~=? sumNumbers example1
       , "day 18" ~: "sumNumbers" ~: "example2" ~: parseLine' "[[[[6,6],[7,6]],[[7,7],[7,0]]],[[[7,7],[7,7]],[[7,8],[9,9]]]]" ~=? sumNumbers example2

       , "day 18" ~: "solvePart1" ~: "example2" ~: 4140 ~=? solvePart1 example2
       , "day 18" ~: "solvePart1" ~: "input" ~: 4184 ~=? solvePart1 input
       ]
