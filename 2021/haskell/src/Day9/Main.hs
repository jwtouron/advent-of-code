{-# LANGUAGE ImportQualifiedPost #-}

module Main where

import Control.Monad
import Data.Array.Unboxed (UArray)
import Data.Array.Unboxed qualified as Array
import Data.List
import Data.Set (Set)
import Data.Set qualified as Set
import Test.HUnit
import Util

type HeightMap =  UArray (Int, Int) Int

readInput :: String -> HeightMap
readInput = newHeightMap . map (map (read . (:[]))) . lines

newHeightMap :: [[Int]] -> HeightMap
newHeightMap xss =
    Array.array ((0, 0), (height - 1, width - 1)) $ join $ zipWith f [0..] xss
  where
    f r = zipWith (\c x -> ((r, c), x)) [0..]
    height = length xss
    width = length (head xss)

neighbors :: HeightMap -> (Int, Int) -> [(Int, Int)]
neighbors m (r, c) = do
  let ((rlb, clb), (rub, cub)) = Array.bounds m
  p@(r', c') <- [(r + 1, c), (r - 1, c), (r, c + 1), (r, c - 1)]
  guard $ r' >= rlb && r' <= rub && c' >= clb && c' <= cub
  pure $ p

findLowPoints :: HeightMap -> [(Int, Int)]
findLowPoints m = do
  (i, e) <- Array.assocs m
  let ns = neighbors m i
  guard $ all ((> e) . (m Array.!)) ns
  pure i

solvePart1 :: HeightMap -> Int
solvePart1 m = foldl' (\s p -> s + m Array.! p + 1) 0 . findLowPoints $ m

findBasin :: HeightMap -> (Int, Int) -> Set (Int, Int)
findBasin m p = go (Set.singleton p) Set.empty
  where
    go frontier visited
      | Set.null frontier = visited
      | otherwise =
          let p = head $ Set.toList frontier
              pv = m Array.! p
              ns = filter (validNeighbors m pv) $ neighbors m p
              frontier' = Set.union (Set.fromList ns) (Set.delete p frontier)
              visited' = Set.insert p visited
          in go frontier' visited'
          where
            validNeighbors m pv n =
              let nv = m Array.! n
              in nv > pv &&
                 Set.notMember n visited &&
                 nv /= 9

solvePart2 :: HeightMap -> Int
solvePart2 m =
  product . take 3 . reverse . sort . map (Set.size . findBasin m) . findLowPoints $ m

main :: IO ()
main = defaultMain readInput solvePart1 solvePart2 tests

example =
  newHeightMap [ [2, 1, 9, 9, 9, 4, 3, 2, 1, 0]
               , [3, 9, 8, 7, 8, 9, 4, 9, 2, 1]
               , [9, 8, 5, 6, 7, 8, 9, 8, 9, 2]
               , [8, 7, 6, 7, 8, 9, 6, 7, 8, 9]
               , [9, 8, 9, 9, 9, 6, 5, 6, 7, 8]
               ]

tests :: HeightMap -> Test
tests input =
  test [ "day 9" ~: "part 1" ~: "example" ~: 15 ~=? solvePart1 example
       , "day 9" ~: "part 1" ~: "input" ~: 554 ~=? solvePart1 input
       , "day 9" ~: "part 2" ~: "example" ~: 1134 ~=? solvePart2 example
       , "day 9" ~: "part 2" ~: "input" ~: 1017792 ~=? solvePart2 input
       ]
