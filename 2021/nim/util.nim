import os
import strformat
import strutils
import system

proc test*[T](expected: T, actual: T, desc: string) =
  doAssert expected == actual, &"{desc}: expected: {expected}, actual: {actual}"

proc tryParseInt*(s: string, x: var int): bool =
  try:
    x = parseInt(s)
    result = true
  except:
    result = false

template tests*(body: untyped) =
  if paramStr(1) == "-t":
    body
    echo "All tests passed."
    quit(0)

template withFileStream*(fs: untyped, path: string, body: untyped): untyped =
  var fs = newFileStream(path)
  defer: fs.close
  body

template withFileLines*(line: untyped, path: string, body: untyped): untyped =
  withFileStream(fs, path):
    var line = ""
    while fs.readLine(line):
      body

proc readInput*[T](path: string, parseLine: proc(line: string): T): seq[T] =
  result = @[]
  withFileLines(line, path):
    result.add(parseLine(line))
