import os
import streams
import util

proc solvePart1(nums: seq[int]): int =
  var prev = 99999999
  for num in nums:
    if num > prev:
      result += 1
    prev = num

proc solvePart2(nums: seq[int]): int =
  var prev = nums[0] + nums[1] + nums[2]
  for i in 0 ..< len(nums) - 3:
    let new = nums[i + 1] + nums[i + 2] + nums[i + 3]
    if new > prev:
      result += 1
    prev = new

proc readInput(path: string): seq[int] =
  result = @[]
  withFileLines(line, path):
    var x = 0
    if tryParseInt(line, x):
      result.add(x)

when isMainModule:
  tests:
    let example = @[199, 200, 208, 210, 200, 207, 240, 269, 260, 263]
    let input = readInput(paramStr(2))
    test 7, solvePart1 example, "part 1, example"
    test 1400, solvePart1 input, "part 1, input"
    test 5, solvePart2 example, "part 2, example"
    test 1429, solvePart2 input, "part 2, input"
  var input = readInput(paramStr(1))
  echo solvePart1(input)
  echo solvePart2(input)
