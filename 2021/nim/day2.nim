import os
import re
import streams
import strutils
import util

type
  Position = object
    horiz, depth, aim: int

type
  CommandKind = enum
    Down
    Up
    Forward
  Command = object
    kind: CommandKind
    val: int

let
  example = @[Command(kind: Forward, val: 5),
              Command(kind: Down, val: 5),
              Command(kind: Forward, val: 8),
              Command(kind: Up, val: 3),
              Command(kind: Down, val: 8),
              Command(kind: Forward, val: 2)]

let commandRegex = re"(forward|up|down) +(.*)"

proc parseCommand(s: string): Command =
  var subs: array[2, string]
  doAssert s.match(commandRegex, subs)
  let x = subs[1].parseInt
  case subs[0]
  of "forward": result = Command(kind: Forward, val: x)
  of "up": result = Command(kind: Up, val: x)
  of "down": result = Command(kind: Down, val: x)

proc solvePart1(commands: openarray[Command]): int =
  var position = Position()
  for command in commands:
    case command.kind
    of Forward: position.horiz += command.val
    of Up: position.depth -= command.val
    of Down: position.depth += command.val
  result = position.depth * position.horiz

proc solvePart2(commands: openarray[Command]): int =
  var position = Position()
  for command in commands:
    case command.kind
    of Forward:
      position.horiz += command.val
      position.depth += position.aim * command.val
    of Up: position.aim -= command.val
    of Down: position.aim += command.val
  result = position.depth * position.horiz

proc readInput(path: string): seq[Command] =
  result = @[]
  withFileLines(line, path):
    result.add(parseCommand(line))

when isMainModule:
  tests:
    let input = readInput(paramStr(2))
    test 150, solvePart1 example, "part 1, example"
    test 1714680, solvePart1 input, "part 1, input"
    test 900, solvePart2 example, "part 2, example"
    test 1963088820, solvePart2 input, "part 2, input"
  let input = readInput(paramStr(1))
  echo solvePart1(input)
  echo solvePart2(input)
