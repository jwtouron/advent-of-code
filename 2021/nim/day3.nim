import os
import sequtils
import streams
import strutils
import sugar
import util

type
  Bit = enum
    Zero, One

let
  example =
    @[@[Zero, Zero, One, Zero, Zero],
      @[One, One, One, One, Zero],
      @[One, Zero, One, One, Zero],
      @[One, Zero, One, One, One],
      @[One, Zero, One, Zero, One],
      @[Zero, One, One, One, One],
      @[Zero, Zero, One, One, One],
      @[One, One, One, Zero, Zero],
      @[One, Zero, Zero, Zero, Zero],
      @[One, One, Zero, Zero, One],
      @[Zero, Zero, Zero, One, Zero],
      @[Zero, One, Zero, One, Zero]]

proc toInt(bits: openarray[Bit]): int =
  var e = 1
  for i in countdown(high(bits), low(bits)):
    result += e * ord(bits[i])
    e *= 2

proc solvePart1(xss: seq[seq[Bit]]): auto =
  var freqs = xss[0].map((x) => (0,0))
  for xs in xss:
    for i, x in xs:
      if x == Zero:
        freqs[i][0] += 1
      else:
        freqs[i][1] += 1
  var gamma: seq[Bit] = @[]
  var epsilon: seq[Bit] = @[]
  for (zs, os) in freqs:
    if zs > os:
      gamma.add(Zero)
      epsilon.add(One)
    else:
      gamma.add(One)
      epsilon.add(Zero)
  result = gamma.toInt * epsilon.toInt

proc findRating(rating: seq[seq[Bit]], cmp: (int, int) -> bool): seq[Bit] =
  var newRating = rating
  for i in low(rating[0]) .. high(rating[0]):
    if len(newRating) == 1:
      return newRating[0]
    var freqs = (0, 0)
    for bs in newRating:
      if bs[i] == Zero:
        freqs[0] += 1
      else:
        freqs[1] += 1
    var tmpRating: seq[seq[Bit]]
    if cmp(freqs[0], freqs[1]):
      for bs in newRating:
        if bs[i] == Zero:
          tmpRating.add(bs)
    else:
      for bs in newRating:
        if bs[i] == One:
          tmpRating.add(bs)
    newRating = tmpRating
  doAssert len(newRating) == 1
  result = newRating[0]

proc solvePart2(xss: seq[seq[Bit]]): int =
  var o2Rating = findRating(xss, (x, y) => x > y).toInt
  var co2Rating = findRating(xss, (x, y) => x <= y).toInt
  result = o2Rating * co2Rating

proc parseLine(path: string): seq[Bit] =
  for x in path:
    if x == '0':
      result.add(Zero)
    else:
      result.add(One)

when isMainModule:
  tests:
    var input = readInput(paramStr(2), parseLine)
    test 198, solvePart1 example, "day 3, part 1, example"
    test 1092896, solvePart1 input, "day 3, part 1, input"
    test 230, solvePart2 example, "day 3, part 2, example"
    test 4672151, solvePart2 input, "day 3, part 2, input"
  var input = readInput(paramStr(1), parseLine)
  echo solvePart1(input)
  echo solvePart2(input)
