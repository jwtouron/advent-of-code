mod advent_of_code;

use advent_of_code::*;
use std::marker::PhantomData;

#[derive(Debug)]
struct NumberIterator<'a> {
    current: [u8; 6],
    last: [u8; 6],
    first: bool,
    phantom: PhantomData<&'a usize>,
}

impl<'a> NumberIterator<'a> {
    fn new(first: &str, last: &str) -> Self {
        assert!(first.len() == 6);
        assert!(last.len() == 6);
        let mut num = NumberIterator { current: [0; 6], last: [0; 6], first: true, phantom: PhantomData };
        for (i, c) in first.bytes().enumerate() {
            num.current[i] = c - 48;
        }
        for (i, c) in last.bytes().enumerate() {
            num.last[i] = c - 48;
        }
        num
    }
}

impl<'a> Iterator for NumberIterator<'a> {
    type Item = &'a [u8; 6];

    fn next(&mut self) -> Option<Self::Item> {
        if self.current == self.last {
            return None
        }
        let iter = unsafe { std::mem::transmute::<&mut Self, &'a mut Self>(self) };
        if self.first {
            self.first = false;
            return Some(&iter.current)
        }
        let mut carry = true;
        let mut i: i64 = 5;
        while carry && i >= 0 {
            iter.current[i as usize] += 1;
            if iter.current[i as usize] == 10 {
                iter.current[i as usize] = 0;
            } else {
                carry = false
            }
            i -= 1;
        }
        Some(&iter.current)
    }
}

fn all_increasing(digits: &[u8; 6]) -> bool {
    for i in 0..5 {
        if digits[i] > digits[i+1] {
            return false
        }
    }
    true
}

fn two_equal(digits: &[u8; 6]) -> bool {
    for i in 0..5 {
        if digits[i] == digits[i+1] {
            return true
        }
    }
    false
}

fn exactly_two_equal(digits: &[u8; 6]) -> bool {
    for i in 0..5 {
        if digits[i] == digits[i+1] {
            if i == 4 {
                return digits[3] != digits[4]
            }
            if digits[i+1] != digits[i+2] && (i == 0 || digits[i-1] != digits[i]) {
                return true
            }
        }
    }
    false
}

fn part1(iter: NumberIterator) {
    let sum = iter
        .filter(|ds| all_increasing(ds))
        .filter(|ds| two_equal(ds))
        .count();
    print_equality_test("Part 1: ", 1625, sum);
}

fn part2(iter: NumberIterator) {
    let sum = iter
        .filter(|ds| all_increasing(ds))
        .filter(|ds| exactly_two_equal(ds))
        .count();
    print_equality_test("Part 2: ", 1111, sum);
}

fn main() {
    let first = "171309";
    let last = "643603";
    part1(NumberIterator::new(first, last));
    part2(NumberIterator::new(first, last));
}
