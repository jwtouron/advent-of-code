mod advent_of_code;
mod intcode;

use advent_of_code::*;
use std::env;
use std::fs;
use intcode::IntcodeComputer;

fn part1(mut program: Vec<i64>) {
    program[1] = 12;
    program[2] = 2;
    let mut comp = IntcodeComputer::new(program);
    comp.run();
    print_equality_test("Part 1: ", 6627023, comp.memory()[0]);
}

fn part2(program: Vec<i64>) {
    let mut result = 0;
    'outer:
    for i in 1..=99 {
        for j in 1..=99 {
            let mut program = program.clone();
            program[1] = i;
            program[2] = j;
            let mut comp = IntcodeComputer::new(program);
            comp.run();
            if comp.memory()[0] == 19690720 {
                result = 100 * i + j;
                break 'outer;
            }
        }
    }
    print_equality_test("Part 2: ", 4019, result);
}

fn main() {
    let path = env::args().nth(1).unwrap_or("../input/day2.txt".into());
    let program: Vec<i64> = fs::read_to_string(path).unwrap().split(",").map(|x| x.trim().parse().unwrap()).collect();
    part1(program.clone());
    part2(program);
}
