#![allow(dead_code)]

use std::collections::VecDeque;
use std::convert::TryFrom;

#[derive(Debug, PartialEq)]
enum ParameterMode {
    PositionMode,
    ImmediateMode,
}

#[derive(Debug, PartialEq)]
struct Parameter {
    mode: ParameterMode,
    value: i64,
}

impl Parameter {
    fn new(mode: ParameterMode, value: i64) -> Self {
        Parameter { mode, value }
    }

    fn get(&self, memory: &[i64]) -> i64 {
        match self.mode {
            ParameterMode::PositionMode => memory[self.value as usize],
            ParameterMode::ImmediateMode => self.value,
        }
    }
}

#[derive(Debug, PartialEq)]
enum Instruction {
    Add(Parameter, Parameter, Parameter),
    Multiply(Parameter, Parameter, Parameter),
    Input(Parameter),
    Output(Parameter),
    JumpIfTrue(Parameter, Parameter),
    JumpIfFalse(Parameter, Parameter),
    LessThan(Parameter, Parameter, Parameter),
    Equals(Parameter, Parameter, Parameter),
    Halt,
}

impl Instruction {
    fn run(&self, computer: &mut IntcodeComputer) {
        match self {
            Self::Add(p1, p2, dst) => {
                let p1 = p1.get(computer.memory.as_mut_slice());
                let p2 = p2.get(computer.memory.as_mut_slice());
                computer.memory[dst.value as usize] = p1 + p2;
                computer.ip += 4;
            }
            Self::Multiply(p1, p2, dst) => {
                let p1 = p1.get(computer.memory.as_mut_slice());
                let p2 = p2.get(computer.memory.as_mut_slice());
                computer.memory[dst.value as usize] = p1 * p2;
                computer.ip += 4;
            }
            Self::Input(param) => {
                let input = computer.inputs.pop_front().expect("Missing input");
                computer.memory[param.value as usize] = input;
                computer.ip += 2;
            }
            Self::Output(param) => {
                let param = param.get(computer.memory.as_mut_slice());
                computer.outputs.push_back(param);
                computer.ip += 2;
            }
            Self::JumpIfTrue(p1, p2) => {
                let p1 = p1.get(computer.memory.as_mut_slice());
                let p2 = p2.get(computer.memory.as_mut_slice());
                if p1 != 0 {
                    computer.ip = p2 as usize;
                } else {
                    computer.ip += 3;
                }
            }
            Self::JumpIfFalse(p1, p2) => {
                let p1 = p1.get(computer.memory.as_mut_slice());
                let p2 = p2.get(computer.memory.as_mut_slice());
                if p1 == 0 {
                    computer.ip = p2 as usize;
                } else {
                    computer.ip += 3;
                }
            }
            Self::LessThan(p1, p2, p3) => {
                let p1 = p1.get(computer.memory.as_mut_slice());
                let p2 = p2.get(computer.memory.as_mut_slice());
                computer.memory[p3.value as usize] = if p1 < p2 { 1 } else { 0 };
                computer.ip += 4;
            }
            Self::Equals(p1, p2, p3) => {
                let p1 = p1.get(computer.memory.as_mut_slice());
                let p2 = p2.get(computer.memory.as_mut_slice());
                computer.memory[p3.value as usize] = if p1 == p2 { 1 } else { 0 };
                computer.ip += 4;
            }
            Self::Halt => (),
        }
    }
}

fn decode_opcode(mut oc: i64) -> (ParameterMode, ParameterMode, ParameterMode, i64) {
    use ParameterMode::*;
    let cast = |n| { assert!(n == 0 || n == 1); if n == 0 { PositionMode } else { ImmediateMode } };
    let a = oc % 10;
    oc /= 10;
    let b = oc % 10;
    oc /= 10;
    let c = oc % 10;
    oc /= 10;
    let d = oc % 10;
    oc /= 10;
    let e = oc % 10;
    (cast(c), cast(d), cast(e), b * 10 + a)
}

#[derive(Debug, PartialEq)]
struct InvalidInstruction(i64);

impl TryFrom<&[i64]> for Instruction {
    type Error = InvalidInstruction;

    fn try_from(instr: &[i64]) -> Result<Self, Self::Error> {
        use Instruction::*;
        let (pm1, pm2, pm3, opcode) = decode_opcode(instr[0]);
        match opcode {
            1  => Ok( Add(Parameter::new(pm1, instr[1]), Parameter::new(pm2, instr[2]), Parameter::new(pm3, instr[3])) ),
            2  => Ok( Multiply(Parameter::new(pm1, instr[1]), Parameter::new(pm2, instr[2]), Parameter::new(pm3, instr[3])) ),
            3  => Ok( Input(Parameter::new(pm1, instr[1])) ),
            4  => Ok( Output(Parameter::new(pm1, instr[1])) ),
            5  => Ok( JumpIfTrue(Parameter::new(pm1, instr[1]), Parameter::new(pm2, instr[2])) ),
            6  => Ok( JumpIfFalse(Parameter::new(pm1, instr[1]), Parameter::new(pm2, instr[2])) ),
            7  => Ok( LessThan(Parameter::new(pm1, instr[1]), Parameter::new(pm2, instr[2]), Parameter::new(pm3, instr[3])) ),
            8  => Ok( Equals(Parameter::new(pm1, instr[1]), Parameter::new(pm2, instr[2]), Parameter::new(pm3, instr[3])) ),
            99 => Ok( Halt ),
            n  => Err( InvalidInstruction(n) ),
        }
    }
}

#[derive(Debug, Clone)]
pub struct IntcodeComputer {
    memory: Vec<i64>,
    ip: usize,
    inputs: VecDeque<i64>,
    outputs: VecDeque<i64>,
}

impl IntcodeComputer {
    pub fn new(memory: Vec<i64>) -> Self {
        Self { memory, ip: 0, inputs: VecDeque::new(), outputs: VecDeque::new() }
    }

    pub fn with_inputs<I>(memory: Vec<i64>, inputs: I) -> Self
    where
        I: IntoIterator<Item = i64>,
    {
        Self { memory, ip: 0, inputs: VecDeque::from_iter(inputs.into_iter()), outputs: VecDeque::new() }
    }

    pub fn is_halted(&self) -> bool {
        let instr = Instruction::try_from(&self.memory[self.ip..]);
        instr == Ok(Instruction::Halt)
    }

    fn step(&mut self) {
        match Instruction::try_from(&self.memory[self.ip..]) {
            Ok(instr) => instr.run(self),
            Err(err) => unreachable!("{}", format!("{:?}", err)),
        }
    }

    pub fn run(&mut self) {
        self.run_until(|c| c.is_halted());
    }

    pub fn run_until<F>(&mut self, f: F)
    where
        F: Fn(&IntcodeComputer) -> bool
    {
        while !f(&self) {
            self.step();
        }
    }

    pub fn memory(&self) -> &Vec<i64> {
        &self.memory
    }

    pub fn outputs(&self) -> &VecDeque<i64> {
        &self.outputs
    }

    pub fn outputs_pop(&mut self) -> Option<i64> {
        self.outputs.pop_front()
    }

    pub fn inputs_push(&mut self, n: i64) {
        self.inputs.push_back(n);
    }
}
