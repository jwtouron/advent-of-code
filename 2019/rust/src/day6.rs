mod advent_of_code;

use advent_of_code::*;
use petgraph::graphmap::UnGraphMap;
use petgraph::algo::dijkstra;
use std::collections::HashMap;

fn part1_aux<'a>(input: &HashMap<&'a str, &'a str>, obj: &'a str, cache: &mut HashMap<&'a str, u64>) -> u64 {
    if cache.contains_key(obj) {
        cache[obj]
    } else if let Some(obj2) = input.get(obj) {
        let x = 1 + part1_aux(input, obj2, cache);
        cache.insert(obj, x);
        x
    } else {
        0
    }
}

fn part1(input: &HashMap<&str, &str>) {
    let mut cache = HashMap::with_capacity(1000);
    let mut sum: u64 = 0;
    for &key in input.keys() {
        sum += part1_aux(input, key, &mut cache);
    }
    print_equality_test("Part 1: ", 315757, sum);
}

fn part2(input: &HashMap<&str, &str>) {
    let graph = UnGraphMap::<&str, u32>::from_edges(input.iter().map(|(&k, &v)| (k, v, 1)));
    let src = input["YOU"];
    let dst = input["SAN"];
    let result = dijkstra(&graph, src, Some(dst), |(_, _, &w)| w);
    print_equality_test("Part 2: ", 481, result[dst]);
}

fn main() {
    let path = std::env::args().nth(1).unwrap_or("../input/day6.txt".into());
    let content = std::fs::read_to_string(path).unwrap();
    let input: HashMap<&str, &str> = content.split("\n")
        .filter(|&x| x != "")
        .map(|x| x.split_once(")").expect(format!("No ) in value to split on: {}", x).as_ref()))
        .fold(HashMap::with_capacity(1000), |mut m, x| { m.insert(x.1, x.0); m });
    part1(&input);
    part2(&input);
}
