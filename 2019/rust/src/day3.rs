mod advent_of_code;

use advent_of_code::*;
use std::collections::HashMap;
use std::str::FromStr;

type Location = (i64, i64);

#[derive(Debug)]
struct State {
    location: Location,
    visited: HashMap<Location, usize>,
    distance: usize,
}

impl State {
    fn new() -> Self {
        let mut result = State { location: (0, 0), visited: HashMap::with_capacity(10000), distance: 0 };
        result.visited.insert((0, 0), 0);
        result
    }

    fn step(&mut self, instr: &Instruction) {
        use Instruction::*;
        match *instr {
            U(n) => {
                for _ in 0..n {
                    self.location.1 += 1;
                    self.distance += 1;
                    self.visited.entry(self.location).or_insert(self.distance);
                }
            },
            D(n) => {
                for _ in 0..n {
                    self.location.1 -= 1;
                    self.distance += 1;
                    self.visited.entry(self.location).or_insert(self.distance);
                }
            },
            L(n) => {
                for _ in 0..n {
                    self.location.0 -= 1;
                    self.distance += 1;
                    self.visited.entry(self.location).or_insert(self.distance);
                }
            },
            R(n) => {
                for _ in 0..n {
                    self.location.0 += 1;
                    self.distance += 1;
                    self.visited.entry(self.location).or_insert(self.distance);
                }
            },
        }
    }

    fn step_many<T>(&mut self, instrs: T)
    where
        T: IntoIterator<Item = Instruction>,
    {
        for instr in instrs {
            self.step(&instr);
        }
    }
}

#[derive(Clone)]
enum Instruction {
    R(u64),
    L(u64),
    U(u64),
    D(u64),
}

impl FromStr for Instruction {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        use Instruction::*;
        let n = s[1..].parse().expect(format!("bad parse: {:?}", s).as_ref());
        match s.bytes().nth(0).unwrap() {
            b'R' => Ok( R(n) ),
            b'L' => Ok( L(n) ),
            b'U' => Ok( U(n) ),
            b'D' => Ok( D(n) ),
            _ => unreachable!()
        }
    }
}

#[derive(Clone)]
struct Instructions(Vec<Instruction>);

impl IntoIterator for Instructions {
    type Item = Instruction;
    type IntoIter = std::vec::IntoIter<Self::Item>;

    fn into_iter(self) -> Self::IntoIter {
        self.0.into_iter()
    }
}

impl FromStr for Instructions {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok( Instructions(s.split(",").map(|x| x.parse::<Instruction>().unwrap() ).collect()) )
    }
}

fn solve(instrs1: Instructions, instrs2: Instructions) {
    let mut state1 = State::new();
    let mut state2 = State::new();
    state1.step_many(instrs1);
    state2.step_many(instrs2);
    let (visited1, visited2) =
        if state1.visited.len() < state1.visited.len() {
            (&state1.visited, &state2.visited)
        } else {
            (&state2.visited, &state1.visited)
        };
    let mut min_dist1 = usize::MAX;
    let mut min_dist2 = usize::MAX;
    for x in visited1.keys() {
        if *x != (0, 0) && visited2.get(x).is_some()  {
            let d = manhattan_distance(x, &(0, 0));
            min_dist1 = min_dist1.min(d as usize);
            let d = visited1[x] + visited2[x];
            min_dist2 = min_dist2.min(d);
        }
    }
    print_equality_test("Part 1: ", 293, min_dist1);
    print_equality_test("Part 2: ", 27306, min_dist2);
}

fn main() {
    let input_path = std::env::args().nth(1).unwrap_or("../input/day3.txt".into());
    let contents = std::fs::read_to_string(input_path).unwrap();
    let lines = contents.lines().collect::<Vec<_>>();
    let instrs1: Instructions = lines[0].parse().unwrap();
    let instrs2: Instructions = lines[1].parse().unwrap();
    solve(instrs1, instrs2);
}
