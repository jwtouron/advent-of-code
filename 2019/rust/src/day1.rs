mod advent_of_code;

use advent_of_code::*;
use std::env;
use std::fs;

fn calc_fuel(x: i64) -> i64 {
    x / 3 - 2
}

fn part1(input: &Vec<i64>) {
    let sum = input.iter()
        .map(|x| calc_fuel(*x))
        .sum();
    print_equality_test("Part 1: ", 3235550, sum);
}

fn part2(input: &Vec<i64>) {
    let f = |x| {
        let mut sum: i64 = 0;
        let mut x = calc_fuel(x);
        while x > 0 {
            sum += x;
            x = calc_fuel(x);
        }
        sum
    };
    let sum = input.iter()
        .map(|x| f(*x))
        .sum();
    print_equality_test("Part 2: ", 4850462, sum);
}

fn main() {
    let path = env::args().nth(1).unwrap_or("../input/day1.txt".into());
    let content = fs::read_to_string(path).unwrap()
        .split("\n")
        .filter(|&x| x != "")
        .map(|x| x.parse().unwrap())
        .collect::<Vec<i64>>();
    part1(&content);
    part2(&content);
}
