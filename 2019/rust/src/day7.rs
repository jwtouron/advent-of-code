mod advent_of_code;
mod intcode;

use advent_of_code::*;
use intcode::*;
use itertools::Itertools;

fn part1(program: Vec<i64>) {
    let mut max_output = 0;
    for perm in (0..=4).permutations(5) {
        let mut amp_a = IntcodeComputer::with_inputs(program.clone(), [perm[0], 0]);
        amp_a.run();
        let mut amp_b = IntcodeComputer::with_inputs(program.clone(), [perm[1], *amp_a.outputs().front().unwrap()]);
        amp_b.run();
        let mut amp_c = IntcodeComputer::with_inputs(program.clone(), [perm[2], *amp_b.outputs().front().unwrap()]);
        amp_c.run();
        let mut amp_d = IntcodeComputer::with_inputs(program.clone(), [perm[3], *amp_c.outputs().front().unwrap()]);
        amp_d.run();
        let mut amp_e = IntcodeComputer::with_inputs(program.clone(), [perm[4], *amp_d.outputs().front().unwrap()]);
        amp_e.run();
        let output = *amp_e.outputs().front().unwrap();
        max_output = max_output.max(output);
    }
    print_equality_test("Part 1: ", 13848, max_output);
}

fn part2(program: Vec<i64>) {
    let mut max_output = 0;
    let should_halt = |comp: &IntcodeComputer| { comp.is_halted() || !comp.outputs().is_empty() };
    'outer:
    for perm in (5..=9).permutations(5) {
        let mut machines = [
            IntcodeComputer::with_inputs(program.clone(), [perm[0], 0]),
            IntcodeComputer::with_inputs(program.clone(), [perm[1]]),
            IntcodeComputer::with_inputs(program.clone(), [perm[2]]),
            IntcodeComputer::with_inputs(program.clone(), [perm[3]]),
            IntcodeComputer::with_inputs(program.clone(), [perm[4]]),
        ];
        loop {
            for i in 0..5 {
                machines[i].run_until(should_halt);
                if i == 4 && machines[i].is_halted() {
                    max_output = max_output.max(*machines[4].outputs().back().unwrap());
                    continue 'outer;
                } else {
                    let output = machines[i].outputs_pop().unwrap();
                    machines[(i + 1) % 5].inputs_push(output);
                }
            }
        }
    }
    print_equality_test("Part 2: ", 12932154, max_output);
}

fn main() {
    let path = std::env::args().nth(1).unwrap_or("../input/day7.txt".into());
    let content = std::fs::read_to_string(path).unwrap();
    let program: Vec<i64> = content.split(",").map(|x| x.trim().parse().unwrap()).collect();
    part1(program.clone());
    part2(program);
}
