mod advent_of_code;

use advent_of_code::*;
use itertools::*;
use std::mem;
use std::str::FromStr;

#[derive(Debug)]
struct Layer<const W: usize, const H: usize> {
    digits: [[u8; W]; H]
}

impl<const W: usize, const H: usize> Layer<W, H> {
    fn digits(&self) -> Digits<W, H> {
        Digits { layer: &self, row: 0, col: 0 }
    }

    fn print(&self) {
        for row in 0 .. H {
            for col in 0 .. W {
                if self.digits[row][col] == 1 {
                    print!("#");
                } else {
                    print!(" ");
                }
            }
            println!("");
        }
    }
}

impl<const W: usize, const H: usize> FromStr for Layer<W, H> {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let s = s.as_bytes();
        assert!(s.len() == W * H);
        let mut digits: [[u8; W]; H] = unsafe { mem::MaybeUninit::uninit().assume_init() };

        for c in 0 .. W {
            for r in 0 .. H {
                digits[r][c] = s[r * W + c] - 48;
            }
        }
        Ok( Layer { digits } )
    }
}

struct Digits<'a, const W: usize, const H: usize> {
    layer: &'a Layer<W, H>,
    row: usize,
    col: usize,
}

impl<'a, const W: usize, const H: usize> Iterator for Digits<'a, W, H> {
    type Item = u8;

    fn next(&mut self) -> Option<Self::Item> {
        if self.row >= H {
            None
        } else {
            let result = self.layer.digits[self.row][self.col];
            if self.col == W - 1 {
                self.col = 0;
                self.row += 1;
            } else {
                self.col += 1;
            }
            Some(result)
        }
    }
}

fn part1(layers: &Vec<Layer<25, 6>>) {
    let layer = layers.iter().min_by_key(|l| l.digits().counts()[&0]).unwrap();
    let counts = layer.digits().counts();
    print_equality_test("Part 1: ", 2480, counts[&1] * counts[&2]);
}

fn part2(layers: &Vec<Layer<25, 6>>) {
    println!("Part 2:");
    let mut layer = Layer { digits: [[2; 25]; 6] };
    for l in layers {
        for row in 0 .. 6 {
            for col in 0 .. 25 {
                if layer.digits[row][col] == 2 {
                    layer.digits[row][col] = l.digits[row][col];
                }
            }
        }
    }
    layer.print();
}

fn main() {
    let path = std::env::args().nth(1).unwrap_or("../input/day8.txt".into());
    let layers: Vec<Layer<25, 6>> = std::fs::read_to_string(path).unwrap()
        .trim()
        .chars()
        .chunks(25 * 6)
        .into_iter()
        .map(|chunk| chunk.collect::<String>().parse::<Layer<25, 6>>().unwrap())
        .collect();
    part1(&layers);
    part2(&layers);
}
