mod advent_of_code;
mod intcode;

use advent_of_code::*;
use intcode::*;

fn part1(program: Vec<i64>) {
    let mut comp = IntcodeComputer::with_inputs(program, [1]);
    comp.run();
    print_equality_test("Part 1: ", 3122865, *comp.outputs().back().unwrap());
}

fn part2(program: Vec<i64>) {
    let mut comp = IntcodeComputer::with_inputs(program, [5]);
    comp.run();
    print_equality_test("Part 2: ", 773660, *comp.outputs().back().unwrap());
}

fn main() {
    let path = std::env::args().nth(1).unwrap_or("../input/day5.txt".into());
    let program = std::fs::read_to_string(path).unwrap().split(",").map(|x| x.trim().parse().unwrap()).collect::<Vec<i64>>();
    part1(program.clone());
    part2(program);
}
