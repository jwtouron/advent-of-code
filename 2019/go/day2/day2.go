package main

import (
	"advent-of-code/utils"
	"fmt"
	"os"
	"slices"
	"strconv"
	"strings"
)

type intcodeComputer struct {
	memory []int64
	ip      uint64
}

func newIntcodeComputer(program []int64) *intcodeComputer {
	return &intcodeComputer{program, 0}
}

func (computer *intcodeComputer) step() {
	instr := decodeInstruction(computer.memory[computer.ip:])
	instr.run(computer)
}

func (computer *intcodeComputer) run() {
	for computer.memory[computer.ip] != 99 {
		computer.step()
	}
}

type instruction interface {
	run(*intcodeComputer)
}

type add struct {
	param1 int64
	param2 int64
	dest   int64
}

func (instr add)run(computer *intcodeComputer) {
	param1 := computer.memory[instr.param1]
	param2 := computer.memory[instr.param2]
	computer.memory[instr.dest] = param1 + param2
	computer.ip += 4
}

type multiply struct {
	param1 int64
	param2 int64
	dest   int64
}

func (instr multiply)run(computer *intcodeComputer) {
	param1 := computer.memory[instr.param1]
	param2 := computer.memory[instr.param2]
	computer.memory[instr.dest] = param1 * param2
	computer.ip += 4
}

type halt struct {
}

func (instr halt)run(computer *intcodeComputer) {
}

func decodeInstruction(program []int64) instruction {
	if program[0] == 1 {
		return add{program[1], program[2], program[3]}
	} else if program[0] == 2 {
		return multiply{program[1], program[2], program[3]}
	} else if program[0] == 99 {
		return halt{}
	}
	panic(fmt.Sprintf("Unknown opcode: %v", program[0]))
}

func part1(program []int64) {
	program[1] = 12
	program[2] = 2
	comp := newIntcodeComputer(program)
	comp.run()
	utils.PrintEqualityTest("Part 1: ", 6627023, program[0])
}

func part2(program []int64) {
	program2 := slices.Clone(program)
	var n int64
	var v int64
	outer:
	for n = range 100 {
		for v = range 100 {
			copy(program2, program)
			program2[1] = n
			program2[2] = v
			computer := newIntcodeComputer(program2)
			computer.run()
			if program2[0] == 19690720 {
				break outer
			}
		}
	}
	utils.PrintEqualityTest("Part 2: ", 4019, n * 100 + v)
}

func main() {
	path := "../input/day2.txt"
	if len(os.Args) > 1 {
		path = os.Args[1]
	}
	bytes, err := os.ReadFile(path)
	utils.Assert(err == nil, "Could not read file: %v", err)
	contents := strings.TrimRight((string(bytes)), "\n")
	splits := strings.Split(contents, ",")
	program := make([]int64, 0, 1000)
	for _, s := range splits {
		num, err := strconv.ParseInt(s, 0, 64)
		utils.Assert(err == nil, "Couldn't parse number: %v", err)
		program = append(program, num)
	}
	program2 := slices.Clone(program)
	part1(program)
	part2(program2)
}
