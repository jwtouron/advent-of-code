package main

import (
	"advent-of-code/utils"
	"fmt"
	"math"
	"strconv"
	"strings"
)

func absInt64(x int64) int64 {
	if x < 0 {
		return -1 * x
	} else {
		return x
	}
}

type point struct {
	x, y int64
}

type position struct {
	pos point
	dist int64
}

func (p *position)step(instr instruction, hist *map[point]int64) {
	var dx, dy int64
	switch instr.direction {
	case U:
		dy = 1
	case D:
		dy = -1
	case L:
		dx = -1
	case R:
		dx = 1
	}
	for range instr.distance {
		p.pos.x += dx
		p.pos.y += dy
		p.dist += 1
		_, ok := (*hist)[p.pos]
		if !ok {
			(*hist)[p.pos] = p.dist
		}
	}
}

func manhattanDistance(p1, p2 point) int64 {
	return absInt64(p1.x - p2.x) + absInt64(p1.y - p2.y)
}

const (
	U = iota
	D
	R
	L
)

type direction = int

type instruction struct {
	direction direction
	distance int64
}

func parseInstruction(s string) instruction {
	var dir direction
	switch s[0] {
	case 'U':
		dir = U
	case 'D':
		dir = D
	case 'R':
		dir = R
	case 'L':
		dir = L
	default:
		panic(fmt.Sprintf("Cannot parse: %v", s))
	}
	n, err := strconv.ParseInt(s[1:], 10, 0)
	utils.Assert(err == nil, "Cannot parse: %v", s)
	return instruction{dir, n}
}

func parseInstructions(s string) []instruction {
	splits := strings.Split(s, ",")
	instructions := make([]instruction, 0, 1000)
	for _, split := range splits {
		instr := parseInstruction(split)
		instructions = append(instructions, instr)
	}
	return instructions
}

func runInstructions(instrs []instruction) map[point]int64 {
	pos := position{}
	hist := make(map[point]int64, 1000)
	for _, instr := range instrs {
		pos.step(instr, &hist)
	}
	return hist
}

func solve(hist1, hist2 map[point]int64, distance func(point, int64, int64) int64) int64 {
	var closest int64 = math.MaxInt64
	for pt, dist1 := range hist1 {
		dist2, ok := hist2[pt]
		if ok {
			dist := distance(pt, dist1, dist2)
			if dist < closest {
				closest = dist
			}
		}
	}
	return closest
}

func part1(hist1, hist2 map[point]int64) {
	closest := solve(hist1, hist2, func(pt point, a, b int64) int64 {
		return manhattanDistance(point{0, 0}, pt)
	})
	utils.PrintEqualityTest("Part 1: ", 293, closest)
}

func part2(hist1, hist2 map[point]int64) {
	closest := solve(hist1, hist2, func(p point, d1, d2 int64) int64 {
		return d1 + d2
	})
	utils.PrintEqualityTest("Part 2: ", 27306, closest)
}

func main() {
	contents, err := utils.ReadArg1Or("../input/day3.txt")
	utils.Assert(err == nil, "Couldn't read input: %v", err)
	splits := strings.Split(contents, "\n")
	instrs1 := parseInstructions(splits[0])
	instrs2 := parseInstructions(splits[1])
	hist1 := runInstructions(instrs1)
	hist2 := runInstructions(instrs2)
	part1(hist1, hist2)
	part2(hist1, hist2)
}
