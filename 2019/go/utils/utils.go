package utils

import (
	"bufio"
	"fmt"
	"os"
)

func Assert(cond bool, format string, a ...any) {
	if !cond {
		panic(fmt.Sprintf(format, a...))
	}
}

func IsDigit(b byte) bool {
	return b >= 48 && b <= 57
}

func PrintEqualityTest[T comparable](label string, expected, actual T) {
	fmt.Printf("%sExpected: %v, Actual: %v, Equal: %v\n", label, expected, actual, expected == actual)
}

func ReadArg1Or(defaultPath string) (string, error) {
	path := defaultPath
	if len(os.Args) > 1 {
		path = os.Args[1]
	}
	bytes, err := os.ReadFile(path)
	if err != nil {
		return "", nil
	}
	return string(bytes), nil
}

func ReadFileLines(path string, callback func(string)) {
	file, err := os.Open(path)
	if err != nil {
		panic(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)

	for scanner.Scan() {
		line := scanner.Text()
		callback(line)
	}

	if err := scanner.Err(); err != nil {
		panic(err)
	}
}
