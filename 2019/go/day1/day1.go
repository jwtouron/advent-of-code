package main

import (
	"advent-of-code/utils"
	"os"
	"strconv"
)

func part1(nums []int64) {
	var sum int64
	for _, num := range nums {
		sum += num / 3 - 2
	}
	utils.PrintEqualityTest("Part 1: ", 3235550, sum)
}

func part2(nums []int64) {
	var sum int64
	for _, num := range nums {
		for {
			num = num / 3 - 2
			if num > 0 {
				sum += num
			} else {
				break
			}
		}
	}
	utils.PrintEqualityTest("Part 2: ", 4850462, sum)
}

func main() {
	path := "../input/day1.txt"
	if len(os.Args) > 1 {
		path = os.Args[1]
	}
	nums := make([]int64, 0, 10000)
	utils.ReadFileLines(path, func(line string) {
		n, err := strconv.ParseInt(line, 0, 64)
		if err != nil { panic(err) }
		nums = append(nums, n)
	})
	part1(nums)
	part2(nums)
}
