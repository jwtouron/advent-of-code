package main

import (
	"advent-of-code/utils"
	"fmt"
)

func isValidPart1(bs []byte) bool {
	twoSame := false
	for i := 1; i < len(bs); i += 1 {
		if bs[i] == bs[i - 1] {
			twoSame = true
		}
		if bs[i] < bs[i - 1] {
			return false
		}
	}
	return twoSame
}

func isValidPart2(bs []byte) bool {
	for i := 0; i < len(bs) - 1; i += 1 {
		if bs[i] > bs[i + 1] {
			return false
		}
	}
	for i := 0; i < len(bs) - 1; i += 1 {
		if bs[i] == bs[i+1] {
			if i == 4 {
				return bs[3] != bs[4]
			}
			if bs[i+1] != bs[i+2] && (i == 0 || bs[i-1] != bs[i]) {
				return true
			}
		}
	}
	return false
}

func solve(first, last int64, isValid func([]byte) bool) int64 {
	var sum int64
	for i := first; i <= last; i += 1 {
		if isValid([]byte(fmt.Sprintf("%d", i))) {
			sum += 1
		}
	}
	return sum
}

func part1(first, last int64) {
	sum := solve(first, last, isValidPart1)
	utils.PrintEqualityTest("Part 1: ", 1625, sum)
}

func part2(first, last int64) {
	sum := solve(first, last, isValidPart2)
	utils.PrintEqualityTest("Part 2: ", 1111, sum)
}
func main() {
	part1(171309, 643603)
	part2(171309, 643603)
	fmt.Println("DELETEME")
}
