(defpackage #:day7
  (:use #:cl))

(in-package #:day7)

(defvar *example* '("$ cd /"
                    "$ ls"
                    "dir a"
                    "14848514 b.txt"
                    "8504156 c.dat"
                    "dir d"
                    "$ cd a"
                    "$ ls"
                    "dir e"
                    "29116 f"
                    "2557 g"
                    "62596 h.lst"
                    "$ cd e"
                    "$ ls"
                    "584 i"
                    "$ cd .."
                    "$ cd .."
                    "$ cd d"
                    "$ ls"
                    "4060174 j"
                    "8033020 d.log"
                    "5626152 d.ext"
                    "7214296 k"))

(defvar *input* (str:lines (str::from-file "../input/day7.txt")))

(defclass node ()
  ((name :initarg :name :reader name)
   (parent :initarg :parent :initform nil :reader parent)))

(defclass file (node)
   ((size :initarg :size :reader size)))

(defclass dir (node)
  ((size :initarg :size :accessor size)
   (children :initarg :children :initform nil :accessor children)))

(defun make-file (name size &key (parent nil))
  (make-instance 'file :name name :size size :parent parent))

(defun make-dir (name &key (size 0) (parent nil) (children nil))
  (make-instance 'dir :name name :size size :parent parent :children children))

(defmethod print-object ((obj file) stream)
  (with-slots (name size) obj
    (print-unreadable-object (obj stream :type t :identity t)
      (format stream "NAME=\"~A\" SIZE=~A" name size))))

(defmethod print-object ((obj dir) stream)
  (with-slots (name size children) obj
    (print-unreadable-object (obj stream :type t :identity t)
      (format stream "NAME=\"~A\" SIZE=~A CHILDREN=~A" name size children))))

(defgeneric init-sizes (node))

(defmethod init-sizes ((node dir))
  (progn
    (loop for child in (children node)
          do (init-sizes child))
    (setf (size node) (loop for child in (children node)
                            sum (size child)))))

(defmethod init-sizes ((node file))
  (size node))

(defun make-tree (lines)
  (loop with tree = nil
        with curr = nil
        for line in lines
        do (destructuring-bind (a b &optional c) (str:split " " line)
             (cond
               ((and (string= a "$") (string= b "cd") (string= c "/"))
                (setf tree (make-dir "/")
                      curr tree))
               ((and (string= a "$") (string= b "cd") (string= c ".."))
                (setf curr (parent curr)))
               ((and (string= a "$") (string= b "ls") (null c)))
               ((and (string= a "$") (string= b "cd"))
                (let ((new-node (make-dir c :parent curr)))
                  (setf (children curr) (cons new-node (children curr))
                        curr new-node)))
               ((and (string= a "dir") (null c)))
               ((null c)
                (let ((new-node (make-file b (parse-integer a) :parent curr)))
                  (setf (children curr) (cons new-node (children curr)))))))
        finally (return (progn (init-sizes tree) tree))))

(defgeneric map-tree (node fun))

(defmethod map-tree ((node dir) fun)
  (funcall fun node)
  (loop for child in (children node)
        do (map-tree child fun)))

(defmethod map-tree ((node file) fun)
  (funcall fun node))

(defun part1 (lines)
  (let ((tree (make-tree lines))
        (sum 0))
    (map-tree tree (lambda (n)
                     (when (and (equal 'dir (type-of n))
                                (<= (size n) 100000))
                       (incf sum (size n)))))
    sum))

(defun part2 (lines)
  (let* ((tree (make-tree lines))
         (needed (- (size tree) 40000000))
         smallest)
    (map-tree tree (lambda (n)
                     (when (and (equal 'dir (type-of n))
                                (>= (size n) needed)
                                (or (null smallest) (< (size n) smallest)))
                       (setf smallest (size n)))))
    smallest))

(assert (= 1086293 (part1 *input*)))
(assert (= 24933642 (part2 *example*)))
(assert (= 366028 (part2 *input*)))
