(defpackage #:day4)

(defun parse-input-line (line)
  (let ((pair (str:split "," line)))
    (list (apply #'cons (mapcar #'parse-integer (str:split "-" (first pair))))
          (apply #'cons (mapcar #'parse-integer (str:split "-" (second pair)))))))

(defvar *example*
  (mapcar #'parse-input-line
          '("2-4,6-8"
            "2-3,4-5"
            "5-7,7-9"
            "2-8,3-7"
            "6-6,4-6"
            "2-6,4-8")))

(defvar *input* (mapcar #'parse-input-line
                        (str:lines (str:from-file "../input/day4.txt"))))

(defun part1 (lines)
  (loop for ((a . b) (c . d)) in lines
        when (or (and (<= a c) (>= b d))
                 (and (<= c a) (>= d b)))
          sum 1))

(defun part2 (lines)
  (loop for ((a . b) (c . d)) in lines
        when (not (or (< b c) (< d a)))
          sum 1))

(assert (= 550 (part1 *input*)))
(assert (= 931 (part2 *input*)))
