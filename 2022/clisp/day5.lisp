(defpackage #:day5)

(defun parse-move (line)
  (destructuring-bind (_ num _ from _ to) (str:split " " line)
    (apply #'values (mapcar (lambda (x) (parse-integer (string x)))
                            (list num from to)))))

(defvar *raw-example*
  (list '((#\N #\Z)
          (#\D #\C #\M)
          (#\P))
        (mapcar (lambda (l) (multiple-value-list (parse-move l)))
                '("move 1 from 2 to 1"
                  "move 3 from 1 to 3"
                  "move 2 from 2 to 1"
                  "move 1 from 1 to 2"))))

(defvar *input*
  (list '((#\W #\R #\T #\G)
          (#\W #\V #\S #\M #\P #\H #\C #\G)
          (#\M #\G #\S #\T #\L #\C)
          (#\F #\R #\W #\M #\D #\H #\J)
          (#\J #\F #\W #\S #\H #\L #\Q #\P)
          (#\S #\M #\F #\N #\D #\J #\P)
          (#\J #\S #\C #\G #\F #\D #\B #\Z)
          (#\B #\T #\R)
          (#\C #\L #\W #\N #\H))
        (mapcar (lambda (l) (multiple-value-list (parse-move l)))
                (str:lines
                 (second
                  (str:split
                   (concatenate 'string '(#\linefeed #\linefeed))
                   (str:from-file "../input/day5.txt")))))))

(defun solve (input reorder-fn)
  (destructuring-bind (stacks moves) input
    (loop for move in moves
          do (destructuring-bind (num from to) move
               (setf (nth (1- to) stacks)
                     (concatenate 'list
                                  (funcall reorder-fn (subseq (nth (1- from) stacks) 0 num))
                                  (nth (1- to) stacks)))
               (setf (nth (1- from) stacks)
                     (subseq (nth (1- from) stacks) num))))
    (concatenate 'string (mapcar #'first stacks))))

(defun part1 (input)
  (solve input #'reverse))

(defun part2 (input)
  (solve input #'identity))

(assert (string= "JCMHLVGMG" (part1 (copy-tree *input*))))
(assert (string= "LVMRWSSPZ" (part2 (copy-tree *input*))))
