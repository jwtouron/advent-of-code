(defpackage #:day1)

(defvar *input*
  (str:split (concatenate 'string '(#\linefeed) '(#\linefeed))
             (str:from-file "../input/day1.txt")))

(defun solve ()
  (loop with elf-totals = nil
        for elf in *input*
        do (loop for n in (str:lines elf)
                 sum (parse-integer n) into total
                 finally (setf elf-totals (cons total elf-totals)))
        finally (return (sort elf-totals #'>))))

(defun part1 ()
  (car (solve)))

(defun part2 ()
  (loop for x in (subseq (solve) 0 3)
        sum x))

(assert (= 72240 (part1)))

(assert (= 210957 (part2)))
