(defpackage #:day6)

(defvar *example1* "bvwbjplbgvbhsrlpgdmjqwftvncz")
(defvar *example2* "nppdvjthqldpwncqszvftbrmjlhg")
(defvar *example3* "nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg")
(defvar *example4* "zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw")

(defvar *example5* "mjqjpqmgbljsphdztnvjfqwrcgsmlb")

(defvar *input* (str:trim (str:from-file "../input/day6.txt")))

(defun all-different (lst)
  (equal lst
         (remove-duplicates lst)))

(defun solve (input num)
  (loop with q = nil
        for c across input
        for i from 1 upto 9999999
        do (setf q (concatenate 'list q (list c)))
        when (> (length q) num)
          do (setf q (subseq q 1))
        when (and (= (length q) num) (all-different q))
          return i))

(defun part1 (input)
  (solve input 4))

(defun part2 (input)
  (solve input 14))

(assert (= 1707 (part1 *input*)))
(assert (= 3697 (part2 *input*)))
