(defpackage #:day2)

(defvar *input* (mapcar (lambda (l)
                          (mapcar #'intern (str:split " " l)))
                        (str:lines (str:from-file #p"../input/day2.txt"))))

(defun solve (score-fn)
  (loop with score = 0
        for (o m) in *input*
        do (incf score (funcall score-fn o m))
        finally (return score)))

(defun part1 ()
  (solve (lambda (o m)
           (let ((m-score (case m
                            (x 1)
                            (y 2)
                            (z 3)))
                 (om-score (cond
                             ((equal (cons o m) '(a . x)) 3)
                             ((equal (cons o m) '(a . y)) 6)
                             ((equal (cons o m) '(a . z)) 0)
                             ((equal (cons o m) '(b . x)) 0)
                             ((equal (cons o m) '(b . y)) 3)
                             ((equal (cons o m) '(b . z)) 6)
                             ((equal (cons o m) '(c . x)) 6)
                             ((equal (cons o m) '(c . y)) 0)
                             ((equal (cons o m) '(c . z)) 3))))
             (+ m-score om-score)))))

(defun part2 ()
  (solve (lambda (o m)
           (cond
             ((equal (cons o m) '(a . x)) (+ 0 3))
             ((equal (cons o m) '(b . x)) (+ 0 1))
             ((equal (cons o m) '(c . x)) (+ 0 2))
             ((equal (cons o m) '(a . y)) (+ 3 1))
             ((equal (cons o m) '(b . y)) (+ 3 2))
             ((equal (cons o m) '(c . y)) (+ 3 3))
             ((equal (cons o m) '(a . z)) (+ 6 2))
             ((equal (cons o m) '(b . z)) (+ 6 3))
             ((equal (cons o m) '(c . z)) (+ 6 1))))))

(assert (= 15337 (part1)))
(assert (= 11696 (part2)))
