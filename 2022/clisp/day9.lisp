(defpackage #:day9
  (:use #:cl))

(in-package #:day9)

(defun parse-line (line)
  (destructuring-bind (a b) (str:split " " line)
    (cons (intern a) (parse-integer b))))

(defvar *example* (mapcar #'parse-line
                          '("R 4"
                            "U 4"
                            "L 3"
                            "D 1"
                            "R 4"
                            "D 1"
                            "L 5"
                            "R 2")))

(defvar *input* (mapcar #'parse-line (str:lines (str:from-file "../input/day9.txt"))))

(defun move-head (head dir)
  (case dir
    (U (incf (car head) -1))
    (D (incf (car head)))
    (L (incf (cdr head) -1))
    (R (incf (cdr head)))))

(defun manhattan-distance (pos1 pos2)
  (destructuring-bind (p1r . p1c) pos1
    (destructuring-bind (p2r . p2c) pos2
      (+ (abs (- p1r p2r))
         (abs (- p1c p2c))))))

(defun clamp (n)
  (cond
    ((> n 0) 1)
    ((< n 0) -1)
    ((zerop n) 0)))

(defun update-tails (rope)
  (loop with prev = (car rope)
        for knot in (cdr rope)
        do (let ((dist (manhattan-distance knot prev)))
             (destructuring-bind (pr . pc) prev
               (destructuring-bind (kr . kc) knot
                 (cond
                   ((and (= 2 dist) (= kr pr))
                    (incf (cdr knot) (clamp (- pc kc))))
                   ((and (= 2 dist) (= kc pc))
                    (incf (car knot) (clamp (- pr kr))))
                   ((< 2 dist)
                    (incf (car knot) (clamp (- pr kr)))
                    (incf (cdr knot) (clamp (- pc kc))))))))
        do (setf prev knot)))

(defun solve (motions n)
  (loop with counts = (make-hash-table :test #'equal :size 1000)
        with rope = (loop for _ from 1 to n collect (cons 0 0))
        for (d . n) in motions
        do (loop for i from n downto 1
                 do (move-head (car rope) d)
                 do (update-tails rope)
                 do (setf (gethash (copy-list (car (last rope))) counts) t))
        finally (return (hash-table-count counts))))

(defun part1 (motions)
  (solve motions 2))

(defun part2 (motions)
  (solve motions 10))

(assert (= 6266 (part1 *input*)))
(assert (= 2369 (part2 *input*)))
