(defpackage #:day8
  (:use #:cl))

(in-package #:day8)

(defun parse-input (lines)
  (let ((array (make-array (list (length lines) (length lines)))))
    (loop for line in lines
          for r = 0 then (incf r)
          do (loop for x across line
                   for c = 0 then (incf c)
                   do (setf (aref array r c) (- (char-int x) 48))))
    array))

(defvar *example* (parse-input '("30373"
                                 "25512"
                                 "65332"
                                 "33549"
                                 "35390")))

(defvar *input* (parse-input (str:lines (str:from-file "../input/day8.txt"))))

(defmacro part1-loop (outer inner highest)
  `(loop ,@outer
         do (loop with highest = ,highest
                  ,@inner
                  when (> (aref input r c) highest)
                    do (setf (gethash (cons r c) hash-table) t
                             highest (aref input r c)))))

(defun part1 (input)
  (let ((hash-table (make-hash-table :test #'equal :size (array-dimension input 0))))
    (destructuring-bind (r-size c-size) (array-dimensions input)
      ;; visible from the top
      (part1-loop (for c from 1 below (- c-size 1))
                  (for r from 1 below (- r-size 1))
                  (aref input 0 c))
      ;; visible from the bottom
      (part1-loop (for c from 1 below (- c-size 1))
                  (for r from (- r-size 2) downto 1)
                  (aref input (- r-size 1) c))
      ;; visible from the left
      (part1-loop (for r from 1 below (- r-size 1))
                  (for c from 1 below (- c-size 1))
                  (aref input r 0))
      ;; visible from the right
      (part1-loop (for r from 1 below (- r-size 1))
                  (for c from (- c-size 2) downto 1)
                  (aref input r (- c-size 1))))
    (+ (hash-table-count hash-table)
       (* (array-dimension input 0) 2)
       (* (- (array-dimension input 1) 2) 2))))

(defun count-visible-trees (input dir r c)
  (labels ((count-visible-trees-aux (r1 c1 accum)
             (destructuring-bind (r-size c-size) (array-dimensions input)
               (case dir
                 (up (cond
                       ((zerop r1) accum)
                       ((< (aref input (- r1 1) c) (aref input r c))
                        (count-visible-trees-aux (- r1 1) c (incf accum)))
                       ((>= (aref input (- r1 1) c) (aref input r c))
                        (incf accum))))
                 (down (cond
                         ((= r1 (- r-size 1)) accum)
                         ((< (aref input (+ r1 1) c) (aref input r c))
                          (count-visible-trees-aux (+ r1 1) c (incf accum)))
                         ((>= (aref input (+ r1 1) c) (aref input r c))
                          (incf accum))))
                 (left (cond
                         ((zerop c1) accum)
                         ((< (aref input r (- c1 1)) (aref input r c))
                          (count-visible-trees-aux r (- c1 1) (incf accum)))
                         ((>= (aref input r (- c1 1)) (aref input r c))
                          (incf accum))))
                 (right (cond
                          ((= c1 (- c-size 1)) accum)
                          ((< (aref input r (+ c1 1)) (aref input r c))
                           (count-visible-trees-aux r (+ c1 1) (incf accum)))
                          ((>= (aref input r (+ c1 1)) (aref input r c))
                           (incf accum))))))))
    (count-visible-trees-aux r c 0)))

(defun part2 (input)
  (let ((max 0))
    (destructuring-bind (r-size c-size) (array-dimensions input)
      (loop for r from 0 below r-size
            do (loop for c from 0 below c-size
                     do (let ((scenic-score (* 1
                                               (count-visible-trees input 'up r c)
                                               (count-visible-trees input 'down r c)
                                               (count-visible-trees input 'left r c)
                                               (count-visible-trees input 'right r c))))
                          (when (> scenic-score max)
                            (setf max scenic-score))))
            finally (return max)))))

(assert (= 1814 (part1 *input*)))
(assert (= 330786 (part2 *input*)))
