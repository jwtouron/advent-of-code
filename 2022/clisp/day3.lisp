(defpackage #:day3)

(defvar *example* '("vJrwpWtwJgWrhcsFMMfFFhFp"
                    "jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL"
                    "PmmdzqPrVvPwwTWBwg"
                    "wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn"
                    "ttgJtRGJQctTZtZT"
                    "CrZsJsPPZsGzwwsLwLmpwMDw"))

(defvar *input* (str:lines (str::from-file "../input/day3.txt")))

(defun char->priority (c)
  (if (char<= c #\Z)
      (- (char-int c) 38)
      (- (char-int c) 96)))

(defun part1 (lines)
  (loop with commons = nil
        for line in lines
        do (let* ((length (/ (length line) 2))
                  (first-substr (subseq line 0 length))
                  (second-substr (subseq line length))
                  (first-hash-table (make-hash-table))
                  (second-hash-table (make-hash-table)))
             (loop for f across first-substr
                   for s across second-substr
                   do (setf (gethash f first-hash-table) t)
                   do (setf (gethash s second-hash-table) t))
             (loop for k being the hash-keys of second-hash-table
                   when (gethash k first-hash-table)
                     do (push k commons)))
        finally (return (reduce (lambda (s c) (+ s (char->priority c))) commons :initial-value 0))))

(defun part2 (lines)
  (let ((lines (apply #'vector lines)))
    (loop with commons = nil
          for i = 0 then (+ i 3)
          for j = 1 then (+ j 3)
          for k = 2 then (+ k 3)
          when (>= i (length lines))
            do (return (reduce (lambda (s c) (+ s (char->priority c))) commons :initial-value 0))
          do (let* ((first-hash-table (make-hash-table))
                    (second-hash-table (make-hash-table))
                    (third-hash-table (make-hash-table)))
               (loop for c across (elt lines i)
                     do (setf (gethash c first-hash-table) t))
               (loop for c across (elt lines j)
                     do (setf (gethash c second-hash-table) t))
               (loop for c across (elt lines k)
                     do (setf (gethash c third-hash-table) t))
               (loop for c being the hash-keys of first-hash-table
                     when (and (gethash c second-hash-table)
                               (gethash c third-hash-table))
                       do (push c commons))))))

(assert (= 7597) (part1 *input*))
(assert (= 2607 (part2 *input*)))
