module Main where

import System.Environment
import qualified Day1

runSolve :: [String] -> IO ()
runSolve [] = pure ()
runSolve ("1":xs) = Day1.solve >> runSolve xs

runTests :: [String] -> IO ()
runTests [] = pure ()
runTests ("1":xs) = Day1.runTests >> runTests xs

main :: IO ()
main = do
  args <- getArgs
  if (head args == "--test") then
    runTests (tail args)
  else
    runSolve args
