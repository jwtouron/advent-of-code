{-# LANGUAGE OverloadedStrings #-}

module Day1 where

import Control.Monad (when)
import Data.List (foldl', sortBy, sortOn)
import qualified Data.Text as Text
import qualified Data.Text.IO as TextIO

input :: IO [[Int]]
input = map (map (read . Text.unpack) . Text.lines) . Text.splitOn "\n\n" <$> TextIO.readFile "../input/day1.txt"

part1 :: [[Int]] -> Int
part1 input = maximum $ map (foldl' (+) 0) input

part2 :: [[Int]] -> Int
part2 input = foldl' (+) 0 $ take 3 $ sortBy (flip compare) $ map (foldl' (+) 0) input

solve :: IO ()
solve = do
  input' <- input
  putStrLn $ show $ part1 input'
  putStrLn $ show $ part2 input'

runTests :: IO ()
runTests = do
  input' <- input
  when (part1 input' /= 72240) (error "part1 failed!")
  when (part2 input' /= 210957) (error "part2 failed!")
  putStrLn "All tests passed!"
