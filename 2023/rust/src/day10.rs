use advent_of_code::*;
use std::collections::{HashMap, HashSet};
use std::env;
use std::fs;
use std::str::FromStr;

type Direction = (i64, i64);
type Position = (i64, i64);
type TileMap = HashMap<(i64, i64), char>;

const UP:    (i64, i64) = (-1, 0);
const DOWN:  (i64, i64) = (1, 0);
const LEFT:  (i64, i64) = (0, -1);
const RIGHT: (i64, i64) = (0, 1);

#[derive(Debug)]
struct Input {
    start: (i64, i64),
    tiles: TileMap,
    height: usize,
    width: usize,
}

impl FromStr for Input {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut input = Input { start: (0, 0), tiles: HashMap::new(), height: 0, width: 0 };
        for (row, line) in s.lines().enumerate() {
            for (col, ch) in line.chars().enumerate() {
                input.tiles.insert((row as i64, col as i64), ch);
                if ch == 'S' {
                    input.start = (row as i64, col as i64);
                }
                input.width = col;
            }
            input.height = row;
        }
        input.width += 1;
        input.height += 1;
        return Ok(input)
    }
}

struct StepState<'a> {
    input: &'a Input,
    position: Position,
    direction: Direction,
    path: Vec<Position>,
    visited: HashSet<Position>,
}

impl<'a> StepState<'a> {
    fn new(input: &'a Input, direction: &Direction) -> Self {
        return StepState {
            input,
            position: input.start,
            direction: *direction,
            path: Vec::with_capacity(250),
            visited: HashSet::with_capacity(250),
        }
    }
}

fn step(state: &mut StepState) -> bool {
    let next_tile = (state.position.0 + state.direction.0, state.position.1 + state.direction.1);
    let next_tile_val = state.input.tiles.get(&next_tile);
    let (nt, nd, success) = match (&state.direction, next_tile_val) {
        (_, Some('S'))      => (next_tile, state.direction, true),
        (&RIGHT, Some('-')) => (next_tile, RIGHT, true),
        (&RIGHT, Some('J')) => (next_tile, UP, true),
        (&RIGHT, Some('7')) => (next_tile, DOWN, true),
        (&LEFT, Some('-'))  => (next_tile, LEFT, true),
        (&LEFT, Some('F'))  => (next_tile, DOWN, true),
        (&LEFT, Some('L'))  => (next_tile, UP, true),
        (&DOWN, Some('|'))  => (next_tile, DOWN, true),
        (&DOWN, Some('J'))  => (next_tile, LEFT, true),
        (&DOWN, Some('L'))  => (next_tile, RIGHT, true),
        (&UP, Some('|'))    => (next_tile, UP, true),
        (&UP, Some('F'))    => (next_tile, RIGHT, true),
        (&UP, Some('7'))    => (next_tile, LEFT, true),
        _                   => ((0, 0), UP, false),
    };
    if success {
        state.position = nt;
        state.direction = nd;
        state.path.push(nt);
        state.visited.insert(nt);
    }
    success
}

fn step_until_looped(input: &Input) -> StepState {
    for dir in [(0, 1), (0, -1), (1, 0), (-1, 0)] {
        let mut state = StepState::new(input, &dir);
        while step(&mut state) {
            if state.input.tiles[&state.position] == 'S' {
                return state
            }
        }
    }
    unreachable!();
}

fn part1(step_state: &StepState) {
    print_equality_test("Part 1: ", 6754, step_state.path.len() / 2);
}

fn part2(step_state: &StepState) {
    let mut inside_count = 0;
    for r in 1..step_state.input.height - 1 {
        for c in 1..step_state.input.width - 1 {
            if !step_state.visited.contains(&(r as i64, c as i64)) {
                if (0..c)
                    .filter(|c| step_state.visited.contains(&(r as i64, *c as i64)))
                    .map(|c| step_state.input.tiles[&(r as i64, c as i64)])
                    .filter(|ch| "|LJ".contains([*ch]))
                    .count() % 2 == 1 {
                        inside_count += 1;
                    }
            }
        }
    }
    print_equality_test("Part 2: ", 567, inside_count);
}

fn main() {
    let path = env::args().nth(1).unwrap_or("../input/day10.txt".into());
    let input: Input = fs::read_to_string(path).unwrap().parse().unwrap();
    let step_state = step_until_looped(&input);
    part1(&step_state);
    part2(&step_state);
}
