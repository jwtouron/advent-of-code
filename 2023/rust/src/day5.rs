use advent_of_code::*;
use std::env;
use std::str::FromStr;

#[derive(Debug, PartialEq, Copy, Clone)]
enum Category {
    Seed,
    Soil,
    Fertilizer,
    Water,
    Light,
    Temperature,
    Humidity,
    Location,
}

impl FromStr for Category {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s.to_lowercase().as_str() {
            "seed"        => Ok(Category::Seed),
            "soil"        => Ok(Category::Soil),
            "fertilizer"  => Ok(Category::Fertilizer),
            "water"       => Ok(Category::Water),
            "light"       => Ok(Category::Light),
            "temperature" => Ok(Category::Temperature),
            "humidity"    => Ok(Category::Humidity),
            "location"    => Ok(Category::Location),
            _ => Err(()),
        }
    }
}

#[derive(Debug)]
struct Almanac {
    seeds: Vec<u64>,
    maps: Vec<Map>,
}

#[derive(Debug)]
struct Map {
    source: Category,
    dest: Category,
    ranges: Vec<Range>,
}

#[derive(Debug)]
struct Range {
    source_start: u64,
    dest_start: u64,
    len: u64,
}

fn parse_seeds(lines: &mut Lines) -> Vec<u64> {
    let mut line = lines.next().unwrap();
    assert!(line.starts_with("seeds: "));
    let seeds = line
        .split_once("seeds: ")
        .unwrap()
        .1
        .split(" ")
        .map(|s| s.parse::<u64>().unwrap())
        .collect::<_>();
    line = lines.next().unwrap();
    assert_eq!(line, "");
    seeds
}

fn parse_map(lines: &mut Lines) -> Option<Map> {
    match lines.next() {
        Some(line) => {
            let source_dest = line.split_once(" map:").unwrap().0;
            let (source, dest) = source_dest.split_once("-to-").unwrap();
            let ranges = lines
                .take_while(|l| l != "")
                .map(|l| {
                    let v = l.split(" ")
                        .map(|n| n.parse::<u64>().unwrap())
                        .collect::<Vec<_>>();
                    assert_eq!(v.len(), 3);
                    Range{source_start: v[1], dest_start: v[0], len: v[2]}
                })
            .collect::<_>();
            Some(Map{
                source: source.parse::<Category>().unwrap(),
                dest: dest.parse::<Category>().unwrap(),
                ranges,
            })
        }
        None => None,
    }
}

fn parse_maps(lines: &mut Lines) -> Vec<Map> {
    let mut maps = Vec::with_capacity(10);
    while let Some(map) = parse_map(lines) {
        maps.push(map);
    }
    maps
}

fn parse_almanac(path: &str) -> Almanac {
    let mut lines = read_file_lines(path);
    let seeds = parse_seeds(&mut lines);
    let maps = parse_maps(&mut lines);
    Almanac { seeds, maps }
}

fn find_location(seed: u64, almanac: &Almanac) -> u64 {
    use Category::*;
    let mut current_category = Seed;
    let mut current_value = seed;
    while current_category != Location {
        for map in almanac.maps.iter() {
            if map.source == current_category {
                current_category = map.dest;
                for range in map.ranges.iter() {
                    if current_value >= range.source_start && current_value < range.source_start + range.len {
                        current_value += range.dest_start - range.source_start;
                        break
                    }
                }
                break
            }
        }
    }
    current_value
}

fn part1(almanac: &Almanac) {
    let lowest = almanac.seeds.iter().map(|seed| find_location(*seed, &almanac)).min().unwrap();
    print_equality_test("Part 1: ", 389056265, lowest);
}

#[derive(Copy, Clone)]
struct Interval {
    start: u64,
    end: u64,
}

fn interval_intersection(int1: &Interval, int2: &Interval) -> (Interval, bool) {
    if int2.start >= int1.start && int2.end <= int1.end {
        // int2 is entirely within int1
        return (Interval{start: int2.start, end: int2.end}, true)
    } else if int1.start >= int2.start && int1.end <= int2.end {
        // int1 is entirely within int2
        return (Interval{start: int1.start, end: int1.end}, true)
    } else if int1.start >= int2.start && int1.start < int2.end {
        // int1's start is within int2
        return (Interval{start: int1.start, end: int2.end}, true)
    } else if int1.end > int2.start && int1.end < int2.end {
        // int1's end is within int2
        return (Interval{start: int2.start, end: int1.end}, true)
    } else if int2.start >= int1.start && int2.start < int1.end {
        // int2's start is within int1
        return (Interval{start: int2.start, end: int1.end}, true)
    } else if int2.end > int1.start && int2.end < int1.end {
        // int2's end is witin int1
        return (Interval{start: int1.start, end: int2.end}, true)
    }
    return (Interval{start: 0, end: 0}, false)
}

fn interval_differences(interval1: &Interval, interval2: &Interval) -> Vec<Interval> {
    let mut differences = Vec::with_capacity(2);
    if interval2.start > interval1.start && interval2.start < interval1.end {
        differences.push(Interval { start: interval1.start, end: interval2.start });
    }
    if interval2.end > interval1.start && interval2.end < interval1.end {
        differences.push (Interval { start: interval2.end, end: interval1.end });
    }
    return differences
}

fn calc_next_category(category: Category, input_intervals: &mut Vec<Interval>, almanac: &Almanac) -> (Category, Vec<Interval>) {
    fn find_map(category: Category, almanac: &Almanac) -> &Map {
        for m in almanac.maps.iter() {
            if m.source == category {
                return m
            }
        }
        unreachable!();
    }
    let m = find_map(category, almanac);
    let mut output_intervals = Vec::with_capacity(input_intervals.len());
    let mut ii = 0;
    while ii < input_intervals.len() {
        let mut found = false;
        for r in m.ranges.iter() {
            let interval2 = Interval { start: r.source_start, end: r.source_start + r.len };
            let intersection: Interval;
            (intersection, found) = interval_intersection(&input_intervals[ii], &interval2);
            if found {
                let diff = r.dest_start - r.source_start;
                output_intervals.push(Interval { start: intersection.start + diff, end: intersection.end + diff });
                let diff_intervals = interval_differences(&input_intervals[ii], &interval2);
                for interval in diff_intervals.iter() {
                    input_intervals.push(*interval);
                }
                break
            }
        }
        if !found {
            output_intervals.push(input_intervals[ii]);
        }
        ii += 1;
    }
    return (m.dest, output_intervals)
}

fn part2(almanac: &Almanac) {
    let mut lowest: u64 = 9999999999999999999;
    let mut input_intervals = Vec::with_capacity(100);
    for i in (0 .. almanac.seeds.len()).step_by(2) {
        input_intervals.push(Interval { start: almanac.seeds[i], end: almanac.seeds[i] + almanac.seeds[i+1] })
    }
    let mut cat = Category::Seed;
    while cat != Category::Location {
        (cat, input_intervals) = calc_next_category(cat, &mut input_intervals, almanac);
    }
    for rp in input_intervals.iter() {
        lowest = lowest.min(rp.start)
    }
    print_equality_test("Part 2: ", 137516820, lowest);
}

fn main() {
    let path = env::args().nth(1).unwrap();
    let almanac = parse_almanac(&path);
    part1(&almanac);
    part2(&almanac);
}
