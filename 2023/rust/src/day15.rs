use advent_of_code::*;
use std::env;
use std::fs;

type Input = Vec<String>;

fn read_input(path: &str) -> Input {
    fs::read_to_string(path)
        .unwrap()
        .trim()
        .split(",")
        .map(|x| x.into())
        .collect()
}

fn hash(s: &str) -> u64 {
    s.as_bytes().iter().fold(0u64, |acc, b| (acc + *b as u64) * 17 % 256)
}

fn part1(input: &Input) {
    let sum: u64 = input.iter()
        .map(|s| hash(s))
        .sum();
    print_equality_test("Part 1: ", 516070, sum);
}

fn part2(input: &Input) {
    let mut boxes: Vec<Vec<(&str, u64)>> = std::iter::repeat(Vec::new()).take(256).collect();
    for step in input.iter() {
        if let Some((label, _)) = step.split_once('-') {
            let h = hash(label);
            let bx = &mut boxes[h as usize];
            for i in (0 .. bx.len()).rev() {
                if bx[i].0 == label {
                    bx.remove(i);
                }
            }
        } else if let Some((label, focal_len)) = step.split_once('=') {
            let mut replaced = false;
            let h = hash(label);
            let bx = &mut boxes[h as usize];
            for i in (0 .. bx.len()).rev() {
                if bx[i].0 == label {
                    bx[i].1 = focal_len.parse().unwrap();
                    replaced = true;
                    break
                }
            }
            if !replaced {
                boxes[h as usize].push((label, focal_len.parse().unwrap()));
            }
        } else {
            unreachable!();
        }
    }
    let focusing_power: u64 = boxes.iter().enumerate()
        .map(|(i, b)|
             b.iter().enumerate()
             .map(|(j, (_, f))| (i as u64 + 1) * (j as u64 + 1) * f)
             .sum::<u64>()
            )
        .sum();
    print_equality_test("Part 2: ", 244981, focusing_power);
}

fn main() {
    let path = env::args().nth(1).unwrap_or("../input/day15.txt".into());
    let input = read_input(&path);
    part1(&input);
    part2(&input);
}
