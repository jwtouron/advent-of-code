use advent_of_code::*;
use std::collections::HashSet;
use std::env;
use std::fs;

type Contraption = Vec<Vec<char>>;
const UP: (i64, i64)    = (-1, 0);
const DOWN: (i64, i64)  = (1,  0);
const RIGHT: (i64, i64) = (0,  1);
const LEFT: (i64, i64)  = (0, -1);

fn parse_contraption(s: &str) -> Contraption {
    s.lines().map(|l| l.chars().collect()).collect()
}

#[derive(Debug, Default, Eq, PartialEq, Hash)]
struct State {
    pos: (i64, i64),
    dir: (i64, i64),
}

impl State {
    fn new(pos: &(i64, i64), dir: &(i64, i64)) -> Self {
        State { pos: *pos, dir: *dir, }
    }
}

fn move_pos(pos: &(i64, i64), dir: &(i64, i64)) -> (i64, i64) {
    (pos.0 + dir.0, pos.1 + dir.1)
}

fn is_pos_valid(pos: &(i64, i64), contraption: &Contraption) -> bool {
    pos.0 >= 0 && pos.0 < contraption.len() as i64 && pos.1 >= 0 && pos.1 < contraption[0].len() as i64 
}

fn solve(contraption: &Contraption, start_states: Vec<State>) -> HashSet<(i64, i64)> {
    let mut states = start_states;
    let mut seen_states: HashSet<State> = HashSet::new();
    let mut visited: HashSet<(i64, i64)> = HashSet::new();
    while let Some(state) = states.pop() {
        let pos = state.pos;
        let dir = state.dir;
        if !is_pos_valid(&pos, contraption) || seen_states.contains(&state) {
            continue
        }
        seen_states.insert(state);
        visited.insert(pos);
        match (contraption[pos.0 as usize][pos.1 as usize], dir) {
            ('|', d) if d == RIGHT || d == LEFT =>
                states.extend([State { pos: move_pos(&pos, &UP), dir: UP, }, State { pos: move_pos(&pos, &DOWN), dir: DOWN, }]),
            ('-', d) if d == UP || d == DOWN =>
                states.extend([State { pos: move_pos(&pos, &RIGHT), dir: RIGHT, }, State { pos: move_pos(&pos, &LEFT), dir: LEFT, }]),
            ('\\', RIGHT) => states.push(State::new(&move_pos(&pos, &DOWN), &DOWN)),
            ('\\', LEFT) => states.push(State::new(&move_pos(&pos, &UP), &UP)),
            ('\\', DOWN) => states.push(State::new(&move_pos(&pos, &RIGHT), &RIGHT)),
            ('\\', UP) => states.push(State::new(&move_pos(&pos, &LEFT), &LEFT)),
            ('/', RIGHT) => states.push(State::new(&move_pos(&pos, &UP), &UP)),
            ('/', LEFT) => states.push(State::new(&move_pos(&pos, &DOWN), &DOWN)),
            ('/', DOWN) => states.push(State::new(&move_pos(&pos, &LEFT), &LEFT)),
            ('/', UP) => states.push(State::new(&move_pos(&pos, &RIGHT), &RIGHT)),
            _ => states.push(State { pos: move_pos(&pos, &dir), dir }),
        };
    }
    return visited
}

fn part1(contraption: &Contraption) {
    let count = solve(contraption, vec![State { pos: (0, 0), dir: RIGHT, }]).len();
    print_equality_test("Part 1: ", 8901, count);
}

fn part2(contraption: &Contraption) {
    let mut counts = Vec::<usize>::with_capacity(250);
    for c in 0 .. contraption[0].len() {
        // Top row, going down
        counts.push(solve(contraption, vec![State { pos: (0, c as i64), dir: (1, 0) }]).len());
        // Bottom row, going up
        counts.push(solve(contraption, vec![State { pos: (contraption.len() as i64 - 1, c as i64), dir: (-1, 0) }]).len());
    }
    for r in 0 .. contraption.len() {
        // Left column, going right
        counts.push(solve(contraption, vec![State { pos: (r as i64, 0), dir: (0, 1) }]).len());
        // Right column, going left
        counts.push(solve(contraption, vec![State { pos: (r as i64, contraption[0].len() as i64 - 1), dir: (0, -1) }]).len());
    }
    let count = counts.iter().max().unwrap();
    print_equality_test("Part 2: ", 9064, *count);
}

fn main() {
    let path = env::args().nth(1).unwrap_or("../input/day16.txt".into());
    let contraption = parse_contraption(&fs::read_to_string(&path).unwrap());
    part1(&contraption);
    part2(&contraption);
}
