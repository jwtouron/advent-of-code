use std::fmt::Display;
use std::fs::File;
use std::io;
use std::io::{BufRead, BufReader};

pub struct Lines {
    io_lines: io::Lines<BufReader<File>>,
}

impl Lines {
    pub fn new(io_lines: io::Lines<BufReader<File>>) -> Lines {
        Lines{io_lines}
    }
}

impl Iterator for Lines {
    type Item = String;
    fn next(&mut self) -> Option<Self::Item> {
        match self.io_lines.next() {
            Some(n) => Some(n.unwrap()),
            None => None,
        }
    }
}

pub fn read_file_lines(path: &str) -> Lines {
    let file = File::open(path).unwrap();
    let reader = BufReader::new(file);
    Lines::new(reader.lines())
}

pub fn print_equality_test<T>(label: &str, expected: T, actual: T)
    where T: PartialEq + Display
{
    println!("{label}Expected: {expected}, Actual: {actual}, Equal: {}", expected == actual)
}

