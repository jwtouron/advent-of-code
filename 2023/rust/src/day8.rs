use advent_of_code::*;
use std::collections::HashMap;
use std::env;

#[derive(Debug)]
struct PuzzleMap {
    instructions: Vec<char>,
    node_links: HashMap<String, NodeLink>,
}

#[derive(Debug)]
struct NodeLink {
    left: String,
    right: String,
}

fn lcm(first: usize, second: usize) -> usize {
    first * second / gcd(first, second)
}

fn gcd(first: usize, second: usize) -> usize {
    let mut max = first;
    let mut min = second;
    if min > max {
        let val = max;
        max = min;
        min = val;
    }

    loop {
        let res = max % min;
        if res == 0 {
            return min;
        }

        max = min;
        min = res;
    }
}

fn read_puzzle_map(path: &str) -> PuzzleMap {
    let mut instructions = Vec::with_capacity(250);
    let mut node_links = HashMap::new();
    for line in read_file_lines(path) {
        if instructions.len() == 0 {
            instructions = line.chars().collect();
        } else if line != "" {
            let split1 = line.split_once(" = (").unwrap();
            let split2 = split1.1.split_once(", ").unwrap();
            node_links.insert(
                split1.0.into(),
                NodeLink {
                    left: split2.0.into(),
                    right: split2.1.replace(")", ""),
                });
        }
    }
    PuzzleMap { instructions, node_links }
}

fn step<'a>(puzzle_map: &'a PuzzleMap, instr: char, current_node: &str) -> &'a str {
    if instr == 'L' {
        puzzle_map.node_links[current_node].left.as_ref()
    } else {
        puzzle_map.node_links[current_node].right.as_ref()
    }
}

fn part1(puzzle_map: &PuzzleMap) {
    let mut current_node = "AAA";
    let mut steps: u64 = 0;
    while current_node != "ZZZ" {
        for c in puzzle_map.instructions.iter() {
            current_node = step(puzzle_map, *c, current_node);
            steps += 1;
        }
    }
    print_equality_test("Part 1: ", 19099, steps);
}

fn part2(puzzle_map: &PuzzleMap) {
    let starting_nodes: Vec<&str> = puzzle_map.node_links
        .keys()
        .filter(|n| n.ends_with("A"))
        .map(|s| s.as_ref())
        .collect();
    let mut node_steps = vec![0; starting_nodes.len()];
    for (&node, steps) in starting_nodes.iter().zip(node_steps.iter_mut()) {
        let mut instr_idx: u64 = 0;
        let mut current_node = node;
        while !current_node.ends_with('Z') {
            current_node = step(puzzle_map, puzzle_map.instructions[instr_idx as usize], current_node);
            *steps += 1;
            instr_idx += 1;
            instr_idx %= puzzle_map.instructions.len() as u64;
        }
    }
    let mut steps = node_steps[0];
    for i in 1..node_steps.len() {
        steps = lcm(steps, node_steps[i]);
    }
    print_equality_test("Part 2: ", 17099847107071, steps);
}

fn main() {
    let path = env::args().nth(1).unwrap_or("../input/day8.txt".to_string());
    let puzzle_map = read_puzzle_map(&path);
    part1(&puzzle_map);
    part2(&puzzle_map);
}
