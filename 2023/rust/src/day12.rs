use advent_of_code::*;
use std::collections::HashMap;
use std::env::args;
use std::str::FromStr;

type Input = Vec<SpringRow>;

#[derive(Debug)]
struct SpringRow {
    springs: Vec<char>,
    sizes: Vec<u64>,
}

impl FromStr for SpringRow {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let splits = s.split_once(' ').unwrap();
        let springs = splits.0.chars().collect();
        let sizes = splits.1.split(',').map(|x| x.parse().unwrap()).collect();
        Ok(SpringRow { springs, sizes })
    }
}

fn read_input(path: &str) -> Input {
    read_file_lines(path).map(|x| x.parse().unwrap()).collect()
}

fn solve(springs: &[char], si: usize, goal: &[u64], gi: usize, current: usize, cache: &mut HashMap<(u64, u64, u64), u64>) -> u64 {
    let key = (si as u64, gi as u64, current as u64);
    if cache.contains_key(&key) {
        return cache[&key]
    }
    if si == springs.len() {
        if gi == goal.len() && current == 0 {
            return 1
        } else if gi == goal.len() - 1 && goal[gi] == current as u64 {
            return 1
        } else {
            return 0
        }
    }
    let mut ans = 0;
    for c in ['.', '#'] {
        if springs[si] == c || springs[si] == '?' {
            if c == '.' && current == 0 {
                ans += solve(springs, si + 1, goal, gi, 0, cache);
            } else if c == '.' && current > 0 && gi < goal.len() && goal[gi] == current as u64 {
                ans += solve(springs, si + 1, goal, gi + 1, 0, cache);
            } else if c == '#' {
                ans += solve(springs, si + 1, goal, gi, current + 1, cache);
            }
        }
    }
    cache.insert(key, ans);
    return ans
}

fn part1(input: &Input) {
    let mut count = 0;
    for row in input.iter() {
        let mut cache = HashMap::with_capacity(1000);
        count += solve(&row.springs, 0, &row.sizes, 0, 0, &mut cache);
    }
    print_equality_test("Part 1: ", 7191, count);
}

fn part2(input: &Input) {
    let mut count = 0;
    for row in input.iter() {
        let mut cache = HashMap::with_capacity(1000);
        let mut springs = row.springs.clone();
        for _ in 0 .. 4 {
            springs.push('?');
            springs.extend(&row.springs);
        }
        let sizes = row.sizes.repeat(5);
        count += solve(&springs, 0, &sizes, 0, 0, &mut cache);
    }
    print_equality_test("Part 2: ", 6512849198636, count);
}

fn main() {
    let path = args().nth(1).unwrap_or("../input/day12.txt".into());
    let input = read_input(&path);
    part1(&input);
    part2(&input);
}
