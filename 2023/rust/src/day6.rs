use advent_of_code::*;
use std::str::FromStr;

static INPUT_STR: &str = "Time:        59     79     65     75
Distance:   597   1234   1032   1328";

#[allow(dead_code)]
static EXAMPLE_STR: &str = "Time:      7  15   30
Distance:  9  40  200";

#[derive(Debug)]
struct Races {
    times: Vec<u64>,
    distances: Vec<u64>,
}

impl FromStr for Races {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let splits = s.split_once("\n").ok_or(())?;
        let times = splits.0
            .strip_prefix("Time:").ok_or(())?
            .split(" ")
            .filter(|s| s != &"")
            .map(|s| s.parse().unwrap())
            .collect();
        let distances = splits.1
            .strip_prefix("Distance:").ok_or(())?
            .split(" ")
            .filter(|s| s != &"")
            .map(|s| s.parse().unwrap())
            .collect();
        Ok(Races { times, distances })
    }
}

fn calc_num_winners(time: u64, distance: u64) -> u64 {
    let mut sum = 0;
    for t in 1 ..= time {
        let d = (time - t) * t;
        if d > distance {
            sum += 1;
        }
    }
    sum
}

fn part1(races: &Races) {
    let mut sum = 1;
    for (time, distance) in races.times.iter().zip(races.distances.iter()) {
        sum *= calc_num_winners(*time, *distance);
    }
    print_equality_test("Part 1: ", 220320, sum);
}

fn part2(races: &Races) {
    let time: u64 = races.times.iter()
        .fold("".to_string(), |acc, x| acc + &x.to_string())
        .parse().unwrap();
    let distance: u64 = races.distances.iter()
        .fold("".to_string(), |acc, x| acc + &x.to_string())
        .parse().unwrap();
    let sum = calc_num_winners(time, distance);
    print_equality_test("Part 2: ", 34454850, sum);
}

fn main() {
    let races: Races = INPUT_STR.parse().unwrap();
    // let races: Races = EXAMPLE_STR.parse().unwrap();
    println!("{races:?}");
    part1(&races);
    part2(&races);
}
