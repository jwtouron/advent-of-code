use advent_of_code::*;
use std::env;
use std::fs;

#[derive(Debug)]
enum Direction {
    Up,
    Down,
    Left,
    Right,
}

#[derive(Debug)]
struct Instruction {
    direction: Direction,
    distance: i64,
}

type Instructions = Vec<Instruction>;

fn parse_instruction_part1(s: &str) -> Instruction {
    let splits = s.split(" ").collect::<Vec<&str>>();
    let direction = match splits[0] {
        "R" => Direction::Right,
        "L" => Direction::Left,
        "U" => Direction::Up,
        "D" => Direction::Down,
        _   => unreachable!(),
    };
    let distance = splits[1].parse().unwrap();
    Instruction { direction, distance }
}

fn parse_instruction_part2(s: &str) -> Instruction {
    let splits = s.split(" ").collect::<Vec<&str>>();
    let direction = match &splits[2][splits[2].len()-2 .. splits[2].len()-1] {
        "0" => Direction::Right,
        "1" => Direction::Down,
        "2" => Direction::Left,
        "3" => Direction::Up,
        _   => unreachable!(),
    };
    let distance = i64::from_str_radix(&splits[2][2..splits[2].len()-2], 16).unwrap();
    Instruction { direction, distance }
}

fn vertices_from_instructions(instructions: &Instructions) -> Vec<(i64, i64)> {
    instructions.iter().fold(vec![(0i64, 0i64)], |mut vec, instr| {
        let last_coord = vec.last().unwrap();
        let new_coord = match instr {
            &Instruction { direction: Direction::Right, distance } => (last_coord.0, last_coord.1 + distance),
            &Instruction { direction: Direction::Left, distance }  => (last_coord.0, last_coord.1 - distance),
            &Instruction { direction: Direction::Up, distance }    => (last_coord.0 - distance, last_coord.1),
            &Instruction { direction: Direction::Down, distance }  => (last_coord.0 + distance, last_coord.1),
        };
        if new_coord != vec[0] {
            vec.push(new_coord);
        }
        vec
    })
}

fn shoelace<T>(vertices: &[(T, T)]) -> T
where
    T: num::Signed + num::PrimInt
{
    ((0 .. vertices.len()-1).fold(T::zero(), |sum, i| sum + (vertices[i].0 + vertices[i+1].0) * (vertices[i+1].1 - vertices[i].1)) / T::from(2).unwrap()).abs()
}

fn calc_perimeter(vertices: &[(i64, i64)]) -> i64 {
    let perimeter = vertices.iter().fold((0, (0i64, 0i64)), |(sum, (r1, c1)), (r2, c2)| (sum + r1.abs_diff(*r2) as i64 + c1.abs_diff(*c2) as i64, (*r2, *c2))).0;
    perimeter + vertices[0].0.abs_diff(vertices.last().unwrap().0) as i64 + vertices[0].1.abs_diff(vertices.last().unwrap().1) as i64
}

fn pick_i(area: i64, perimeter: i64) -> i64 {
    area - perimeter/2 + 1
}

fn solve(instructions: &Instructions) -> i64 {
    let vertices = vertices_from_instructions(instructions);
    let area = shoelace(&vertices);
    let perimeter = calc_perimeter(&vertices);
    pick_i(area, perimeter) + perimeter
}

fn part1(input: &str) {
    let instructions = input.lines().map(|x| parse_instruction_part1(x)).collect();
    let sum = solve(&instructions);
    print_equality_test("Part 1: ", 35401, sum);
}

fn part2(input: &str) {
    let instructions = input.lines().map(|x| parse_instruction_part2(x)).collect();
    let sum = solve(&instructions);
    print_equality_test("Part 2: ", 48020869073824, sum);
}

fn main() {
    let path = env::args().nth(1).unwrap_or("../input/day18.txt".into());
    let input = fs::read_to_string(&path).unwrap();
    part1(&input);
    part2(&input);
}
