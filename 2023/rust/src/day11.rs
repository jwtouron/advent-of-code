use advent_of_code::*;
use std::env;

type Universe = Vec<Vec<char>>;

struct Input {
    empty_rows: Vec<usize>,
    empty_cols: Vec<usize>,
    galaxies: Vec<(usize, usize)>,
}

fn parse_input(path: &str) -> Input {
    let mut universe = Vec::with_capacity(250);
    for line in read_file_lines(path) {
        let mut line_vec = Vec::with_capacity(250);
        for c in line.chars() {
            line_vec.push(c);
        }
        universe.push(line_vec);
    }
    let empty_rows = find_empty_rows(&universe);
    let empty_cols = find_empty_cols(&universe);
    let galaxies = find_galaxies(&universe);
    return Input { empty_rows, empty_cols, galaxies, }
}

fn find_empty_rows(universe: &Universe) -> Vec<usize> {
    let mut empty_rows = Vec::with_capacity(150);
    for (i, r) in universe.iter().enumerate() {
        let mut empty = true;
        for c in r.iter() {
            if *c != '.' {
                empty = false;
                break
            }
        }
        if empty {
            empty_rows.push(i);
        }
    }
    return empty_rows
}

fn find_empty_cols(universe: &Universe) -> Vec<usize> {
    let mut empty_cols = Vec::with_capacity(150);
    for c in 0 .. universe[0].len() {
        let mut empty = true;
        for r in 0 .. universe.len() {
            if universe[r][c] != '.' {
                empty = false;
                break
            }
        }
        if empty {
            empty_cols.push(c);
        }
    }
    return empty_cols
}

fn find_galaxies(universe: &Universe) -> Vec<(usize, usize)> {
    let mut galaxies = Vec::with_capacity(250);
    for (r, v) in universe.iter().enumerate() {
        for (c, ch) in v.iter().enumerate() {
            if *ch != '.' {
                galaxies.push((r, c));
            }
        }
    }
    return galaxies
}

fn count_empty_between(n1: usize, n2: usize, empties: &Vec<usize>) -> usize {
    let mut x1 = n1;
    let mut x2 = n2;
    if x1 > x2 {
        let tmp = x1;
        x1 = x2;
        x2 = tmp;
    }
    empties.iter().filter(|&&n| n > x1 && n < x2).count()
}

fn manhattan_distance(p1: &(usize, usize), p2: &(usize, usize), input: &Input, expand_factor: usize) -> usize {
    let row_diff: usize = (p1.0 as i64 - p2.0 as i64).abs() as usize;
    let col_diff: usize = (p1.1 as i64 - p2.1 as i64).abs() as usize;
    let mut dist = row_diff + col_diff;
    dist += count_empty_between(p1.0, p2.0, &input.empty_rows) * expand_factor;
    dist += count_empty_between(p1.1, p2.1, &input.empty_cols) * expand_factor;
    return dist
}

fn solve(input: &Input, expand_factor: usize) -> usize {
    let mut sum = 0;
    for g1 in input.galaxies.iter() {
        for g2 in input.galaxies.iter() {
            if g1 < g2 {
                sum += manhattan_distance(g1, g2, &input, expand_factor);
            }
        }
    }
    return sum
}

fn part1(input: &Input) {
    let sum = solve(&input, 1);
    print_equality_test("Part 1: ", 10494813, sum);
}

fn part2(input: &Input) {
    let sum = solve(&input, 999999);
    print_equality_test("Part 2: ", 840988812853, sum);
}

fn main() {
    let path = env::args().nth(1).unwrap_or("../input/day11.txt".into());
    let input = parse_input(&path);
    part1(&input);
    part2(&input);
}
