use std::env;
use advent_of_code::*;

fn get_input() -> Vec<String> {
    let mut v = Vec::with_capacity(1000);
    for line in read_file_lines(env::args().nth(1).expect("Missing input path!").as_ref()) {
        v.push(line);
    }
    v
}

fn part1(input: &Vec<String>) {
    let mut sum: u64 = 0;
    for line in input {
        let mut digits: Vec<char> = Vec::new();
        for c in line.chars() {
            if c.is_digit(10) {
                digits.push(c.into());
            }
        }
        let mut num = digits.first().unwrap().to_string();
        num.push(*digits.last().unwrap());
        sum += num.parse::<u64>().unwrap();
    }
    print_equality_test("Part 1: ", 55090, sum)
}

fn part2(input: &Vec<String>) {
    let digits = [
        ("one", 1),
        ("two", 2),
        ("three", 3),
        ("four", 4),
        ("five", 5),
        ("six", 6),
        ("seven", 7),
        ("eight", 8),
        ("nine", 9),
        ("1", 1),
        ("2", 2),
        ("3", 3),
        ("4", 4),
        ("5", 5),
        ("6", 6),
        ("7", 7),
        ("8", 8),
        ("9", 9),
    ];
    let mut sum: u64 = 0;
    for line in input {
        let mut found_digits: Vec<u64> = Vec::new();
        for i in 0..line.len() {
            for digit in &digits {
                if digit.0.len() <= line.len() - i && &line[i .. i + digit.0.len()] == digit.0 {
                    found_digits.push(digit.1);
                }
            }
        }
        sum += found_digits.first().unwrap() * 10 + found_digits.last().unwrap();
    }
    print_equality_test("Part 2: ", 54845, sum);
}

fn main() {
    let input = get_input();
    part1(&input);
    part2(&input);
}
