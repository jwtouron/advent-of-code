use advent_of_code::*;
use std::env;
use std::fs;

type Pattern = Vec<Vec<char>>;
type Patterns = Vec<Pattern>;

fn parse_patterns(s: &str) -> Patterns {
    s.split("\n\n").map(|p| p.lines().map(|l| l.chars().collect()).collect()).collect()
}

fn find_vertical_reflection(pattern: &Pattern, badness: i64) -> Option<usize> {
    'cols: for col in 0 .. pattern[0].len() - 1 {
        let mut badness = badness;
        let mut dc = 0;
        loop {
            let left = col - dc;
            let right = col + 1 + dc;
            for row in 0 .. pattern.len() {
                if pattern[row][left] != pattern[row][right] {
                    badness -= 1;
                }
            }
            if left == 0 || right == pattern[0].len() - 1 {
                if badness == 0 {
                    return Some(col)
                } else {
                    continue 'cols
                }
            }
            dc += 1;
        }
    }
    return None
}

fn find_horizontal_reflection(pattern: &Pattern, badness: i64) -> Option<usize> {
    'rows: for row in 0 .. pattern.len() - 1 {
        let mut badness = badness;
        let mut dr = 0;
        loop {
            let up = row - dr;
            let down = row + 1 + dr;
            for col in 0 .. pattern[0].len() {
                if pattern[up][col] != pattern[down][col] {
                    badness -= 1;
                }
            }
            if up == 0 || down == pattern.len() - 1 {
                if badness == 0 {
                    return Some(row)
                } else {
                    continue 'rows
                }
            }
            dr += 1;
        }
    }
    return None
}

fn solve(patterns: &Patterns, badness: i64) -> usize {
    patterns.iter()
        .map(|p|
             find_vertical_reflection(p, badness)
             .map(|x| x + 1)
             .or(find_horizontal_reflection(p, badness).map(|x| (x + 1) * 100)).expect("Expected to find a horizontal or vertical fold!"))
        .sum()
}

fn part1(patterns: &Patterns) {
    let sum = solve(patterns, 0);
    print_equality_test("Part 1: ", 27664, sum);
}

fn part2(patterns: &Patterns) {
    let sum = solve(patterns, 1);
    print_equality_test("Part 2: ", 33991, sum);
}

fn main() {
    let path = env::args().nth(1).unwrap_or("../input/day13.txt".into());
    let patterns = parse_patterns(&fs::read_to_string(&path).unwrap());
    part1(&patterns);
    part2(&patterns);
}
