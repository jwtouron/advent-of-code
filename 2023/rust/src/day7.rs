use advent_of_code::*;
use std::cmp::*;
use std::collections::HashMap;
use std::env::args;
use std::str::FromStr;

#[derive(Debug, PartialOrd, Ord, Eq, PartialEq)]
enum HandType {
    HighCard,
    OnePair,
    TwoPair,
    ThreeOfAKind,
    FullHouse,
    FourOfAKind,
    FiveOfAKind,
}

type Card = char;

#[derive(Debug)]
struct Hand {
    cards: [Card; 5],
    bid: u64,
    hand_type: HandType,
}

impl FromStr for Hand {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let splits = s.split_once(" ").ok_or(())?;
        let mut cards = [' '; 5];
        for (i, c) in splits.0.chars().enumerate() {
            cards[i] = c;
        }
        let hand_type = get_hand_type(&cards, false);
        let bid = splits.1.parse().map_err(|_| ())?;
        Ok(Hand { cards, bid, hand_type, })
    }
}

fn read_input(path: &str) -> Vec<Hand> {
    read_file_lines(path).map(|x| x.parse().unwrap()).collect()
}

fn compare_hands<'a, 'b>(h1: &'a Hand, h2: &'b Hand, card_rankings: &HashMap<char, i32>) -> Ordering {
    match h1.hand_type.cmp(&h2.hand_type) {
        Ordering::Less => Ordering::Less,
        Ordering::Greater => Ordering::Greater,
        Ordering::Equal => {
            for (&c1, &c2) in h1.cards.iter().zip(h2.cards.iter()) {
                if c1 != c2 {
                    return card_rankings[&c1].cmp(&card_rankings[&c2])
                }
            }
            Ordering::Equal
        }
    }
}

fn get_hand_type(cards: &[Card; 5], use_jokers: bool) -> HandType {
    let mut counts: [u64; 256] = [0; 256];
    for c in cards.iter() {
        counts[*c as usize] += 1;
    }
    let joker_count = counts['J' as usize];
    let find_count = |val| {
        let mut count = 0;
        for i in 0..256 {
            if use_jokers && i == 'J' as usize {
                continue
            }
            if counts[i] == val {
                count += 1;
            }
        }
        count
    };
    if find_count(5) == 1 {
        return HandType::FiveOfAKind
    } else if find_count(4) == 1 {
        if use_jokers && joker_count > 0 {
            return HandType::FiveOfAKind
        } else {
            return HandType::FourOfAKind
        }
    } else if find_count(2) == 1 && find_count(3) == 1 {
        if use_jokers && joker_count == 1 {
            return HandType::FourOfAKind
        } else if use_jokers && joker_count >= 2 {
            return HandType::FiveOfAKind
        } else {
            return HandType::FullHouse
        }
    } else if find_count(3) == 1 {
        if use_jokers && joker_count == 1 {
            return HandType::FourOfAKind
        } else if use_jokers && joker_count >= 2 {
            return HandType::FiveOfAKind
        } else {
            return HandType::ThreeOfAKind
        }
    } else if find_count(2) == 2 {
        if use_jokers && joker_count == 1 {
            return HandType::FullHouse
        } else if use_jokers && joker_count == 2 {
            return HandType::FourOfAKind
        } else if use_jokers && joker_count >= 3 {
            return HandType::FiveOfAKind
        } else {
            return HandType::TwoPair
        }
    } else if find_count(2) == 1 {
        if use_jokers && joker_count == 1 {
            return HandType::ThreeOfAKind
        } else if use_jokers && joker_count == 2 {
            return HandType::FourOfAKind
        } else if use_jokers && joker_count >= 3 {
            return HandType::FiveOfAKind
        } else {
            return HandType::OnePair
        }
    } else {
        if use_jokers && joker_count == 1 {
            return HandType::OnePair
        } else if use_jokers && joker_count == 2 {
            return HandType::ThreeOfAKind
        } else if use_jokers && joker_count == 3 {
            return HandType::FourOfAKind
        } else if use_jokers && joker_count >= 4 {
            return HandType::FiveOfAKind
        } else {
            HandType::HighCard
        }
    }
}

fn part1(hands: &mut Vec<Hand>) {
    let card_rankings =
        HashMap::from([
                      ('A', 13),
                      ('K', 12),
                      ('Q', 11),
                      ('J', 10),
                      ('T', 9),
                      ('9', 8),
                      ('8', 7),
                      ('7', 6),
                      ('6', 5),
                      ('5', 4),
                      ('4', 3),
                      ('3', 2),
                      ('2', 1),
        ]);
    hands.sort_by(|h1, h2| compare_hands(h1, h2, &card_rankings));
    let sum = (1 ..= hands.len()).zip(hands.iter()).fold(0, |acc, (i, hand)| acc + hand.bid * i as u64);
    print_equality_test("Part 1: ", 250058342, sum);
}

fn part2(hands: &mut Vec<Hand>) {
    let card_rankings =
        HashMap::from([
                      ('A', 13),
                      ('K', 12),
                      ('Q', 11),
                      ('T', 9),
                      ('9', 8),
                      ('8', 7),
                      ('7', 6),
                      ('6', 5),
                      ('5', 4),
                      ('4', 3),
                      ('3', 2),
                      ('2', 1),
                      ('J', 0),
        ]);
    for hand in hands.iter_mut() {
        hand.hand_type = get_hand_type(&hand.cards, true);
    }
    hands.sort_by(|h1, h2| compare_hands(h1, h2, &card_rankings));
    let sum = (1 ..= hands.len()).zip(hands.iter()).fold(0, |acc, (i, hand)| acc + hand.bid * i as u64);
    print_equality_test("Part 2: ", 250506580, sum);
}

fn main() {
    let path = args().nth(1).unwrap_or("../input/day7.txt".into());
    let mut hands = read_input(&path);
    part1(&mut hands);
    part2(&mut hands);
}
