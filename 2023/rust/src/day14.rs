use advent_of_code::*;
use std::collections::HashMap;
use std::env;
use std:: fs;

type Platform = Vec<Vec<char>>;

fn parse_platform(s: &str) -> Platform {
    s.lines().map(|l| l.chars().collect()).collect()
}

fn _print_platform(platform: &Platform,) {
    for row in platform {
        println!("{}", row.iter().collect::<String>());
    }
}

fn calculate_north_beam_load(platform: &Platform) -> u64 {
    platform.iter().zip((1..=platform.len()).rev())
        .map(|(row, x)| row.iter().filter(|&c| *c == 'O').count() as u64 * x as u64)
        .sum()
}

fn tilt_north(platform: &mut Platform) {
    for row in 1 .. platform.len() {
        for r in (1 ..= row).rev() {
            for c in 0 .. platform[r].len() {
                if platform[r][c] == 'O' && platform[r-1][c] == '.' {
                    platform[r-1][c] = 'O';
                    platform[r][c] = '.';
                }
            }
        }
    }
}

fn tilt_west(platform: &mut Platform) {
    for col in 1 .. platform[0].len() {
        for c in (1 ..= col).rev() {
            for r in 0 .. platform.len() {
                if platform[r][c] == 'O' && platform[r][c-1] == '.' {
                    platform[r][c-1] = 'O';
                    platform[r][c] = '.';
                }
            }
        }
    }
}

fn tilt_south(platform: &mut Platform) {
    for row in (0 .. platform.len() - 1).rev() {
        for r in row .. platform.len() - 1 {
            for c in 0 ..platform[r].len() {
                if platform[r][c] == 'O' && platform[r+1][c] == '.' {
                    platform[r][c] = '.';
                    platform[r+1][c] = 'O';
                }
            }
        }
    }
}

fn tilt_east(platform: &mut Platform) {
    for col in (0 .. platform[0].len() - 1).rev() {
        for c in col .. platform[0].len() - 1 {
            for r in 0 ..platform.len() {
                if platform[r][c] == 'O' && platform[r][c+1] == '.' {
                    platform[r][c] = '.';
                    platform[r][c+1] = 'O';
                }
            }
        }
    }
}

fn cycle_platform(platform: &mut Platform) {
    tilt_north(platform);
    tilt_west(platform);
    tilt_south(platform);
    tilt_east(platform);
}

fn part1(platform: &Platform) {
    let mut platform = platform.clone();
    tilt_north(&mut platform);
    let load = calculate_north_beam_load(&platform);
    print_equality_test("Part 1: ", 110565, load);
}

fn part2(platform: &Platform) {
    let mut cache: HashMap<String, usize> = HashMap::new();
    let mut platform = platform.clone();
    let mut t = 0;
    let target = 1_000_000_000;
    while t < target {
        t += 1;
        cycle_platform(&mut platform);
        let key: String = platform.iter().map(|x| x.iter().collect::<String>()).collect::<String>();
        if cache.contains_key(&key) {
            let cycle_length: usize = t - cache[&key];
            let amt = (target - t) / cycle_length;
            t += amt * cycle_length;
        }
        cache.insert(key, t);
    }
    let load = calculate_north_beam_load(&platform);
    print_equality_test("Part 2: ", 89845, load);
}

fn main() {
    let path = env::args().nth(1).unwrap_or("../input/day14.txt".into());
    let platform = parse_platform(&fs::read_to_string(&path).unwrap());
    part1(&platform);
    part2(&platform);
}
