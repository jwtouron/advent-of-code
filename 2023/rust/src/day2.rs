use advent_of_code::*;
use std::env;
use std::str::FromStr;

#[derive(Debug)]
struct Game {
    id: u64,
    cube_sets: Vec<CubeSet>,
}

impl FromStr for Game {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let splits = s.split_once(": ").ok_or(())?;
        let id = splits.0.split_once(" ").ok_or(())?.1.parse::<u64>().map_err(|_| ())?;
        let cube_sets = splits.1
            .split("; ")
            .map(|s| s.parse().unwrap())
            .collect();
        Ok(Game { id, cube_sets, })
    }
}

#[derive(Debug)]
struct CubeSet {
    r: u64,
    g: u64,
    b: u64,
}

impl FromStr for CubeSet {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let cube_set = CubeSet { r: 0, g: 0, b: 0 };
        Ok(
            s.split(", ").fold(cube_set, |mut cs, s| {
                let s2 = s.split_once(" ").unwrap();
                let n = s2.0.parse().unwrap();
                match s2.1 {
                    "red"   => cs.r = n,
                    "green" => cs.g = n,
                    "blue"  => cs.b = n,
                    _       => unreachable!(),
                };
                cs
            })
          )
    }
}

fn part1(games: &Vec<Game>) {
    let id_sum: u64 = games.iter()
        .filter_map(|g| {
            if g.cube_sets.iter().all(|cs| cs.r <= 12 && cs.g <= 13 && cs.b <= 14) {
                Some(g.id)
            } else {
                None
            }
        })
    .sum();
    print_equality_test("Part 1: ", 2879, id_sum);
}

fn part2(games: &Vec<Game>) {
    let sum: u64 = games.iter()
        .map(|g| {
            let cs = g.cube_sets.iter().fold(CubeSet{r: 1, g: 1, b: 1}, |mut acc, cs| {
                acc.r = acc.r.max(cs.r);
                acc.g = acc.g.max(cs.g);
                acc.b = acc.b.max(cs.b);
                acc
            });
            cs.r * cs.g * cs.b
        })
    .sum();
    print_equality_test("Part 2: ", 65122, sum);
}

fn main() {
    let path = env::args().nth(1).unwrap_or("../input/day2.txt".to_string());
    let games = read_file_lines(&path).map(|l| l.parse().unwrap()).collect::<Vec<Game>>();
    part1(&games);
    part2(&games);
}
