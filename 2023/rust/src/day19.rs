use advent_of_code::*;
use std::collections::HashMap;
use std::fmt::{Debug, Formatter};
use std::str::FromStr;
use std::env::args;
use std::fs;

#[derive(Debug)]
struct Part {
    x: i64,
    m: i64,
    a: i64,
    s: i64,
}

impl Part {
    fn run_workflows(self: &Self, workflows: &HashMap<String, Workflow>) -> Destination {
        let mut dest = self.step_workflow(&workflows["in".into()]);
        loop {
            match dest {
                Destination::Accepted => return Destination::Accepted,
                Destination::Rejected => return Destination::Rejected,
                Destination::Workflow(name) => dest = self.step_workflow(&workflows[&name]),
            }
        }
    }

    fn step_workflow(self: &Self, current: &Workflow) -> Destination {
        for rule in current.rules.iter() {
            if (*rule.condition)(&self) {
                return rule.destination.clone()
            }
        }
        unreachable!()
    }
}

impl FromStr for Part {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let splits = s[1 .. s.len()-1].split(",").collect::<Vec<_>>();
        Ok(
            Part {
                x: splits[0].split_once("=").unwrap().1.parse().unwrap(),
                m: splits[1].split_once("=").unwrap().1.parse().unwrap(),
                a: splits[2].split_once("=").unwrap().1.parse().unwrap(),
                s: splits[3].split_once("=").unwrap().1.parse().unwrap()
            }
          )
    }
}

#[derive(Debug)]
struct Workflow {
    name: String,
    rules: Vec<Rule>,
}

impl FromStr for Workflow {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let (name, rules) = s.split_once('{').unwrap();
        let name = name.into();
        let rules = rules[..rules.len() - 1]
            .split(',')
            .map(|rule| rule.parse().unwrap())
            .collect::<Vec<Rule>>();
        Ok(
            Workflow{name, rules}
          )
    }
}

struct Rule {
    condition: Box<dyn Fn(&Part) -> bool>,
    destination: Destination,
}

impl Debug for Rule {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), std::fmt::Error> {
        f.write_str("<closure>").unwrap();
        Ok(())
    }
}

impl FromStr for Rule {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let rule = if let Some((cond, dest)) = s.split_once(':') {
            Rule{condition: parse_condition(cond), destination: dest.parse().unwrap()}
        } else {
            Rule{condition: Box::new(|_| true), destination: s.parse().unwrap()}
        };
        Ok(rule)
    }
}

fn parse_condition(s: &str) -> Box<dyn Fn(&Part) -> bool> {
    let test_attr = |p: &Part, a: &String, f: &dyn Fn(i64) -> bool| {
        match a.as_str() {
            "x" => f(p.x),
            "m" => f(p.m),
            "a" => f(p.a),
            "s" => f(p.s),
            _   => unreachable!(),
        }
    };
    if let Some((attr, num)) = s.split_once('<') {
        let num = num.parse().unwrap();
        let attr = attr.into();
        Box::new(move |part: &Part| test_attr(part, &attr, &|n| n < num))
    } else if let Some((attr, num)) = s.split_once('>') {
        let num = num.parse().unwrap();
        let attr = attr.into();
        Box::new(move |part: &Part| test_attr(part, &attr, &|n| n > num))
    } else {
        unreachable!();
    }
}

#[derive(Debug, Clone, PartialEq)]
enum Destination {
    Accepted,
    Rejected,
    Workflow(String),
}

impl FromStr for Destination {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(
            match s {
                "A" => Destination::Accepted,
                "R" => Destination::Rejected,
                _   => Destination::Workflow(s.into()),
            }
          )
    }
}

struct Input {
    workflows: HashMap<String, Workflow>,
    parts: Vec<Part>,
}

impl FromStr for Input {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let splits = s.split_once("\n\n").unwrap();
        let mut workflows: HashMap<String, Workflow> = HashMap::with_capacity(1000);
        for line in splits.0.lines() {
            let workflow: Workflow = line.parse().unwrap();
            workflows.insert(workflow.name.clone(), workflow);
        }
        let mut parts = Vec::with_capacity(1000);
        for line in splits.1.lines() {
            let part = line.parse().unwrap();
            parts.push(part);
        }
        Ok( Input { workflows, parts } )
    }
}

fn part1(input: &Input) {
    let mut sum: i64 = 0;
    for part in input.parts.iter() {
        let dest = part.run_workflows(&input.workflows);
        if dest == Destination::Accepted {
            sum += part.x + part.m + part.a + part.s;
        }
    }
    print_equality_test("Part1: ", sum, 406849);
}

fn main() {
    let path = args().nth(1).unwrap_or("../input/day19.txt".into());
    let input = fs::read_to_string(path).unwrap();
    let input = input.parse().unwrap();
    part1(&input);
}
