use advent_of_code::*;
use pathfinding::directed::astar::astar;
use std::env;
use std::hash::*;

type City = Vec<Vec<u64>>;

#[derive(Eq, PartialEq, Clone, Hash)]
struct State(Coordinate, Direction, usize);

#[derive(Eq, PartialEq, Hash, Clone)]
enum Direction {
    North,
    South,
    East,
    West,
    None,
}

impl Direction {
    fn to_tuple(&self) -> (i64, i64) {
        match self {
            Direction::North => (-1, 0),
            Direction::South => (1, 0),
            Direction::East  => (0, 1),
            Direction::West  => (0, -1),
            Direction::None  => (0, 0),
        }
    }

    fn reverse(&self) -> Self {
        match self {
            Direction::North => Direction::South,
            Direction::South => Direction::North,
            Direction::East  => Direction::West,
            Direction::West  => Direction::East,
            Direction::None  => Direction::None,
        }
    }
}

#[derive(Clone, Eq, PartialEq, Hash)]
struct Coordinate(i64, i64);

impl Coordinate {
    fn mov(&self, dir: &Direction) -> Self {
        let t = dir.to_tuple();
        Coordinate(self.0 + t.0, self.1 + t.1)
    }

    fn manhattan_distance(&self, other: &Self) -> i64 {
        self.0.abs_diff(other.0) as i64 + self.1.abs_diff(other.1) as i64
    }
}

fn parse_city(path: &str) -> City {
    read_file_lines(path).map(|line| line.as_bytes().iter().map(|d| (d - b'0') as u64).collect()).collect()
}

fn is_valid_dir(dir: &Direction, state: &State, min: usize, max: usize) -> bool {
    if *dir == state.1.reverse() {
        return false
    }
    if state.2 > 0 && state.2 < min && *dir != state.1 {
        return false
    }
    if state.2 == max && *dir == state.1 {
        return false
    }
    return true
}

fn is_valid_pos(pos: &Coordinate, city: &City) -> bool {
    pos.0 >= 0 && pos.0 < city.len() as i64 && pos.1 >= 0 && pos.1 < city[0].len() as i64
}

fn solve(city: &City, min: usize, max: usize) -> i64 {
    let start = State(Coordinate(0i64, 0i64), Direction::None, 0);
    let goal = Coordinate((city.len() - 1) as i64, (city[0].len() - 1) as i64);
    let successors = |state: &State| {
        let mut vec = Vec::<(State, i64)>::new();
        for dir in [Direction::North, Direction::South, Direction::East, Direction::West] {
            if !is_valid_dir(&dir, state, min, max) {
                continue
            }
            let new_pos = state.0.mov(&dir);
            if !is_valid_pos(&new_pos, city) {
                continue
            }
            let new_len = if state.1 == dir { state.2 + 1 } else { 1 };
            if new_pos == goal && min > 0 && new_len < min {
                continue
            }
            let cost = city[new_pos.0 as usize][new_pos.1 as usize];
            vec.push((State(new_pos, dir, new_len), cost as i64));
        }
        vec
    };
    let heuristic = |state: &State| state.0.manhattan_distance(&goal);
    let success = |state: &State| state.0 == goal;
    let result = astar(&start, successors, heuristic, success).unwrap();
    result.1
}

fn part1(city: &City) {
    let result = solve(city, 0, 3);
    print_equality_test("Part 1: ", 866, result);
}

fn part2(city: &City) {
    let result = solve(city, 4, 10);
    print_equality_test("Part 2: ", 1010, result);
}

fn main() {
    let path = env::args().nth(1).unwrap_or("../input/day17.txt".into());
    let city = parse_city(&path);
    part1(&city);
    part2(&city);
}
