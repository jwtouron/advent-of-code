package main

import (
	"strconv"
	"strings"
	"advent-of-code/utils"
)

type card struct {
	winningNumbers map[int]struct{}
	haveNumbers map[int]struct{}
}

func getInput() []card {
	cards := make([]card, 0, 1000)
	utils.ReadFileLines("../input/day4.txt", func(line string) {
		parts1 := strings.Split(line, ": ")
		parts2 := strings.Split(parts1[1], " | ")
		winningNumbers := make(map[int]struct{}, 20)
		haveNumbers := make(map[int]struct{}, 20)
		for _, n := range strings.Split(parts2[0], " ") {
			if len(n) > 0 {
				n1, err := strconv.Atoi(n)
				if err != nil { panic(nil) }
				winningNumbers[n1] = struct{}{}
			}
		}
		for _, n := range strings.Split(parts2[1], " ") {
			if len(n) > 0 {
				n1, err := strconv.Atoi(n)
				if err != nil { panic(nil) }
				haveNumbers[n1] = struct{}{}
			}
		}
		cards = append(cards, card{winningNumbers: winningNumbers, haveNumbers: haveNumbers,})
	})
	return cards
}

func getWinningNumbers(card card) []int {
	winningNumbers := make([]int, 0, 20)
	for wn, _ := range card.winningNumbers {
		for hn := range card.haveNumbers {
			if wn == hn {
				winningNumbers = append(winningNumbers, wn)
			}
		}
	}
	return winningNumbers
}

func scoreCard(winningNumbers []int) int {
	count := len(winningNumbers)
	score := 0
	for x := 0; x < count; x++ {
		score = max(1, score * 2)
	}
	return score
}

func part1(input []card) {
	sum := 0
	for _, card := range input {
		score := scoreCard(getWinningNumbers(card))
		sum += score
	}
	utils.PrintEqualityTest(24542, sum)
}

func part2(input []card) {
	cache := make(map[int]int, 1000)
	sum := 0
	cards := make([]int, 0, 1000)
	for i := range input {
		cards = append(cards, i)
	}
	for i := 0; i < len(cards); i++ {
		sum += 1
		inputIdx := cards[i]
		numWinners := 0
		if nw, ok := cache[inputIdx]; ok {
			numWinners = nw
		} else {
			numWinners = len(getWinningNumbers(input[inputIdx]))
			cache[inputIdx] = numWinners
		}
		for j := inputIdx + 1; j < min(len(input), inputIdx + numWinners + 1); j++ {
			cards = append(cards, j)
		}
	}
	utils.PrintEqualityTest(8736438, sum)
}

func main() {
	input := getInput()
	part1(input)
	part2(input)
}
