package main

import "advent-of-code/utils"

func getInput() [][]byte {
	lines := make([][]byte, 0, 100)
	utils.ReadFileLines("../input/day3.txt", func(line string) {
		lines = append(lines, []byte(line))
	})
	return lines
}

func isSymbol(b byte) bool {
	return !utils.IsDigit(b) && b != byte('.')
}

func findNumberStart(line []byte, col int) int {
	for ; col >= 0; col-- {
		if !utils.IsDigit(line[col]) {
			break
		}
	}
	return col + 1
}

func findNumberEnd(line []byte, col int) int {
	for ; col < len(line); col++ {
		if !utils.IsDigit(line[col]) {
			break
		}
	}
	return col - 1
}

func hasNearbySymbol(lines [][]byte, row, startCol, endCol int) bool {
	for r := max(0, row - 1); r < min(len(lines), row + 2); r++ {
		for c := max(0, startCol - 1); c < min(len(lines[0]), endCol + 2); c++ {
			if isSymbol(lines[r][c]) {
				return true
			}
		}
	}
	return false
}

func getNumberValue(line []byte, start, end int) uint64 {
	var mul uint64 = 1
	var val uint64
	for c := end; c >= start; c-- {
		val += (uint64(line[c]) - 48) * mul
		mul *= 10
	}
	return val
}

func part1(lines [][]byte) {
	var sum uint64
	for r := 0; r < len(lines); r++ {
		line := lines[r]
		for c := 0; c < len(line); c++ {
			b := line[c]
			if utils.IsDigit(b) {
				end := findNumberEnd(line, c)
				if hasNearbySymbol(lines, r, c, end) {
					sum += getNumberValue(line, c, end)
				}
				c = end + 1
			}
		}
	}
	utils.PrintEqualityTest(544433, sum)
}

func addNum(lines [][]byte, r, c int, m *map[uint64]bool) {
	if r < 0 || r >= len(lines) || c < 0 || c >= len(lines[0]) {
		return
	}
	if utils.IsDigit(lines[r][c]) {
		start := findNumberStart(lines[r], c)
		end := findNumberEnd(lines[r], c)
		(*m)[getNumberValue(lines[r], start, end)] = true
	}
}

func getNearbyNums(lines [][]byte, r, c int) map[uint64]bool {
	ret := make(map[uint64]bool)
	// row above
	addNum(lines, r - 1, c - 1, &ret)
	addNum(lines, r - 1, c, &ret)
	addNum(lines, r - 1, c + 1, &ret)
	// left side
	addNum(lines, r, c - 1, &ret)
	// right side
	addNum(lines, r, c + 1, &ret)
	// row below
	addNum(lines, r + 1, c - 1, &ret)
	addNum(lines, r + 1, c, &ret)
	addNum(lines, r + 1, c + 1, &ret)
	return ret
}

func part2(lines [][]byte) {
	var sum uint64
	for r := 0; r < len(lines); r++ {
		for c := 0; c < len(lines[0]); c++ {
			b := lines[r][c]
			if b == byte('*') {
				nearbyNums := getNearbyNums(lines, r, c)
				if len(nearbyNums) == 2 {
					var s uint64 = 1
					for k := range nearbyNums {
						s *= uint64(k)
					}
					sum += s
				}
			}
		}
	}
	utils.PrintEqualityTest(76314915, sum)
}

func main() {
	input := getInput()
	part1(input)
	part2(input)
}
