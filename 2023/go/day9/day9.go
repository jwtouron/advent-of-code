package main

import (
	"advent-of-code/utils"
	"os"
	"slices"
	"strconv"
	"strings"
)

func readInput(path string) [][]int64 {
	input := make([][]int64, 0, 1000)
	utils.ReadFileLines(path, func(line string) {
		l := make([]int64, 0, 100)
		for _, s := range strings.Split(line, " ") {
			n, err := strconv.ParseInt(s, 10, 64)
			if err != nil { panic(err) }
			l = append(l, n)
		}
		input = append(input, l)
	})
	return input
}

func isLastAllZeros(lines [][]int64) bool {
	for _, n := range lines[len(lines) - 1] {
		if n != 0 {
			return false
		}
	}
	return true
}

func calcDifferences(lines *[][]int64) {
	for !isLastAllZeros(*lines) {
		line := make([]int64, 0, 100)
		lastLine := (*lines)[len(*lines) - 1]
		for i := 0; i < len(lastLine) - 1; i++ {
			line = append(line, lastLine[i+1] - lastLine[i])
		}
		*lines = append(*lines, line)
	}
}

func predictNextValues(lines *[][]int64) {
	(*lines)[len(*lines) - 1] = append((*lines)[len(*lines) - 1], 0)
	for i := len(*lines) - 2; i >= 0; i-- {
		l0 := &(*lines)[i]
		l1 := &(*lines)[i+1]
		n0 := (*l0)[len(*l0) - 1]
		n1 := (*l1)[len(*l1) - 1]
		*l0 = append(*l0, n0 + n1)
	}
}

func part1(input [][]int64) {
	var sum int64
	for _, line := range input {
		lines := make([][]int64, 0, 100)
		lines = append(lines, line)
		calcDifferences(&lines)
		predictNextValues(&lines)
		sum += lines[0][len(lines[0]) - 1]
	}
	utils.PrintEqualityTest(1921197370, sum)
}

func part2(input [][]int64) {
	for _, line := range input {
		slices.Reverse(line)
	}
	var sum int64
	for _, line := range input {
		lines := make([][]int64, 0, 100)
		lines = append(lines, line)
		calcDifferences(&lines)
		predictNextValues(&lines)
		sum += lines[0][len(lines[0]) - 1]
	}
	for _, line := range input {
		slices.Reverse(line)
	}
	utils.PrintEqualityTest(1124, sum)
}

func main() {
	path := "../input/day9.txt"
	if len(os.Args) > 1 {
		path = os.Args[1]
	}
	input := readInput(path)
	part1(input)
	part2(input)
}
