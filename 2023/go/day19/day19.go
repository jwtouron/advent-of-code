package main

import (
	"advent-of-code/utils"
	"fmt"
	"os"
	"regexp"
	"strconv"
	"strings"
)

func assert(cond bool, msg string, args ...any) {
	if !cond {
		panic(fmt.Sprintf(msg, args...))
	}
}

type part struct {
    x, m, a, s int64
}

var partRegexp *regexp.Regexp = regexp.MustCompile(`^{x=(\d+),m=(\d+),a=(\d+),s=(\d+)}$`)

func parsePart(input string) part {
	parse := func(s string) int64 {
		n, err := strconv.ParseInt(s, 10, 64)
		assert(err == nil, "Bad int64 parse")
		return n
	}
	matches := partRegexp.FindStringSubmatch(input)
	x := parse(matches[1])
	m := parse(matches[2])
	a := parse(matches[3])
	s := parse(matches[4])
	return part{x, m, a, s}
}

type workflow struct {
	name string
	rules []rule
}

var workflowRegexp *regexp.Regexp = regexp.MustCompile(`^([^{]+){([^}]+)}$`)

func parseWorkflow(s string) workflow {
	matches := workflowRegexp.FindStringSubmatch(s)
	assert(matches != nil, "Bad workflow parse: %v", s)
	splits := strings.Split(matches[2], ",")
	rules := make([]rule, 0, 10)
	for _, split := range splits {
		rules = append(rules, parseRule(split))
	}
	return workflow{matches[1], rules}
}

type rule struct {
	cond func(part) bool
	dest string
}

var ruleRegexp *regexp.Regexp = regexp.MustCompile(`^(?:(\w+)(<|>)(\d+):)?(\w+)$`)

func parseRule(s string) rule {
	matches := ruleRegexp.FindStringSubmatch(s)
	assert(matches != nil, "No rule match: %v", s)
	if matches[1] == "" {
		return rule{cond: func(p part) bool { return true }, dest: matches[4]}
	}
	amt, err := strconv.ParseInt(matches[3], 10, 64)
	assert(err == nil, "Bad number conversion: %v", matches[3])
	if matches[1] == "x" && matches[2] == "<" {
		return rule{func(p part) bool { return p.x < amt }, matches[4]}
	} else if matches[1] == "m" && matches[2] == "<" {
		return rule{func(p part) bool { return p.m < amt }, matches[4]}
	} else if matches[1] == "a" && matches[2] == "<" {
		return rule{func(p part) bool { return p.a < amt }, matches[4]}
	} else if matches[1] == "s" && matches[2] == "<" {
		return rule{func(p part) bool { return p.s < amt }, matches[4]}
	} else if matches[1] == "x" && matches[2] == ">" {
		return rule{func(p part) bool { return p.x > amt }, matches[4]}
	} else if matches[1] == "m" && matches[2] == ">" {
		return rule{func(p part) bool { return p.m > amt }, matches[4]}
	} else if matches[1] == "a" && matches[2] == ">" {
		return rule{func(p part) bool { return p.a > amt }, matches[4]}
	} else if matches[1] == "s" && matches[2] == ">" {
		return rule{func(p part) bool { return p.s > amt }, matches[4]}
	} else {
		panic(fmt.Sprintf("Bad rule parse: %v", s))
	}
}

type input struct {
	workflows map[string]workflow
	parts []part
}

func parseInput(s string) input {
	splits := strings.Split(s, "\n\n")
	workflows := make(map[string]workflow, 100)
	for _, line := range strings.Split(splits[0], "\n") {
		if line != "" {
			workflow := parseWorkflow(line)
			workflows[workflow.name] = workflow
		}
	}
	workflows["A"] = workflow{"A", make([]rule, 0)}
	workflows["R"] = workflow{"R", make([]rule, 0)}
	parts := make([]part, 0, 100)
	for _, line := range strings.Split(splits[1], "\n") {
		if line != "" {
			parts = append(parts, parsePart(line))
		}
	}
	return input{workflows, parts}
}

func runPart(part part, workflows map[string]workflow) string {
	workflow := workflows["in"]
	for workflow.name != "A" && workflow.name != "R" {
		for _, rule := range workflow.rules {
			if rule.cond(part) {
				workflow = workflows[rule.dest]
				break
			}
		}
	}
	return workflow.name
}

func part1(input input) {
	var sum int64
	for _, part := range input.parts {
		dest := runPart(part, input.workflows)
		if dest == "A" {
			sum += part.x + part.m + part.a + part.s
		}
	}
	utils.PrintEqualityTest(406849, sum)
}

func main() {
	var inputFile string
	if len(os.Args) == 1 {
		inputFile = "../input/day19.txt"
	} else {
		inputFile = os.Args[1]
	}
	contents, err := os.ReadFile(inputFile)
	assert(err == nil, "os.ReadFile: %v", err)
	input := parseInput(string(contents))
	part1(input)
}
