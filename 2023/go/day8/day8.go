package main

import (
	"advent-of-code/utils"
	"os"
	"strings"
)

type puzzleMap struct {
	instructions []rune
	nodes map[string]leftRight
}

type leftRight struct {
	left, right string
}

func readPuzzleMap(path string) puzzleMap {
	pm := puzzleMap{}
	pm.nodes = make(map[string]leftRight, 1000)
	utils.ReadFileLines(path, func(line string) {
		if len(pm.instructions) == 0 {
			pm.instructions = []rune(line)
		} else if line != "" {
			splits1 := strings.Split(line, " = (")
			splits2 := strings.Split(splits1[1], ", ")
			pm.nodes[splits1[0]] = leftRight{
				left: splits2[0],
				right: splits2[1][:len(splits2[1])-1],
			}
		}
	})
	return pm
}

func step(puzzleMap puzzleMap, instr rune, node *string) {
	if instr == 'L' {
		*node = puzzleMap.nodes[*node].left
	} else if instr == 'R' {
		*node = puzzleMap.nodes[*node].right
	}
}

func part1(puzzleMap puzzleMap) {
	currentNode := "AAA"
	var steps uint64
	for currentNode != "ZZZ" {
		for _, instr := range puzzleMap.instructions {
			step(puzzleMap, instr, &currentNode)
			steps++
		}
	}
	utils.PrintEqualityTest(19099, steps)
}

func GCD(a, b uint64) uint64 {
	for b != 0 {
		t := b
		b = a % b
		a = t
	}
	return a
}

// find Least Common Multiple (LCM) via GCD
func LCM(a, b uint64, integers ...uint64) uint64 {
	var result uint64 = a * b / GCD(a, b)
	for i := 0; i < len(integers); i++ {
		result = LCM(result, integers[i])
	}
	return result
}

func part2(puzzleMap puzzleMap) {
	currentNodes := make([]string, 0, 100)
	for k := range puzzleMap.nodes {
		if k[len(k)-1] == 'A' {
			currentNodes = append(currentNodes, k)
		}
	}
	nodeSteps := make([]uint64, len(currentNodes))
	for nodeIdx := range currentNodes {
		node := &currentNodes[nodeIdx]
		var instrIdx uint64
		for (*node)[len(*node)-1] != 'Z' {
			instr := puzzleMap.instructions[instrIdx]
			step(puzzleMap, instr, node)
			instrIdx++
			instrIdx %= uint64(len(puzzleMap.instructions))
			nodeSteps[nodeIdx]++
		}
	}
	steps := LCM(nodeSteps[0], nodeSteps[1], nodeSteps[2:]...)
	utils.PrintEqualityTest(17099847107071, steps)
}

func main() {
	args := os.Args
	var path string
	if len(args) > 1 {
		path = args[1]
	} else {
		path = "../input/day8.txt"
	}
	puzzleMap := readPuzzleMap(path)
	part1(puzzleMap)
	part2(puzzleMap)
}
