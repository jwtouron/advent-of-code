package main

import (
	"log"
	"slices"
	"strconv"
	"advent-of-code/utils"
)

func callback(elves *[]int) func(string) {
	elf := 0
	return func(line string) {
		if line == "" {
			if elf > 0 {
				*elves = append(*elves, elf)
			}
			elf = 0
		} else {
			n, err := strconv.ParseInt(line, 10, 0)
			if err != nil {
				log.Fatalln(err)
			}
			elf += int(n)
		}
	}
}

func solve() []int {
	elves := make([]int, 0)
	utils.ReadFileLines("../input/example1.txt", callback(&elves))
	return elves
}

func part1() {
	elves := solve()
	max := slices.Max(elves)
	expected := 72240
	utils.PrintEqualityTest(expected, max)
}

func part2() {
	elves := solve()
	slices.SortFunc(elves, func(x int, y int) int { return y - x })
	sum := elves[0] + elves[1] + elves[2]
	expected := 210957
	utils.PrintEqualityTest(expected, sum)
}

func main() {
	part1()
	part2()
}
