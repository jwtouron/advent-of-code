package main

import (
	"advent-of-code/utils"
)

func findFirstLastPart1(s string) (byte, byte) {
	lineBytes := []byte(s)
	var first byte
	var last byte
	for _, b := range lineBytes {
		if utils.IsDigit(b) {
			first = b - 48
			break
		}
	}
	for i := len(lineBytes) - 1 ; i >= 0; i-- {
		b := lineBytes[i]
		if utils.IsDigit(b) {
			last = b - 48
			break
		}
		if i == 0 {
			break
		}
	}
	return first, last
}

func part1() {
	var sum uint64
	utils.ReadFileLines("../input/day1.txt", func(line string) {
		first, last := findFirstLastPart1(line)
		sum += 10 * uint64(first) + uint64(last)
	})
	utils.PrintEqualityTest(55090, sum)
}

func parseDigit(bytes []byte, idx int, digit string) bool {
	digitBytes := []byte(digit)
	if idx + len(digit) > len(bytes) {
		return false
	}
	for i := 0; i < len(digitBytes) && i + idx < len(bytes); i++ {
		if digitBytes[i] != bytes[i + idx] {
			return false
		}
	}
	return true
}

func findDigit(bytes []byte, idx int) (byte, bool) {
	if utils.IsDigit(bytes[idx]) {
		return bytes[idx] - 48, true
	} else if parseDigit(bytes, idx, "zero") {
		return 0, true
	} else if parseDigit(bytes, idx, "one") {
		return 1, true
	} else if parseDigit(bytes, idx, "two") {
		return 2, true
	} else if parseDigit(bytes, idx, "three") {
		return 3, true
	} else if parseDigit(bytes, idx, "four") {
		return 4, true
	} else if parseDigit(bytes, idx, "five") {
		return 5, true
	} else if parseDigit(bytes, idx, "six") {
		return 6, true
	} else if parseDigit(bytes, idx, "seven") {
		return 7, true
	} else if parseDigit(bytes, idx, "eight") {
		return 8, true
	} else if parseDigit(bytes, idx, "nine") {
		return 9, true
	} else {
		return 0, false
	}
}

func findFirstLastPart2(s string) (byte, byte) {
	bytes := []byte(s)
	var first, last byte
	var ok bool
	for i, _ := range bytes {
		first, ok = findDigit(bytes, i)
		if ok {
			break
		}
	}
	for i := len(bytes) - 1; i >= 0; i-- {
		last, ok = findDigit(bytes, i)
		if ok {
			break
		}
		if i == 0 {
			break
		}
	}
	return first, last
}

func part2() {
	var sum uint64
	utils.ReadFileLines("../input/day1.txt", func(line string) {
		first, last := findFirstLastPart2(line)
		sum += 10 * uint64(first) + uint64(last)
	})
	utils.PrintEqualityTest(54845, sum)
}

func main() {
	part1()
	part2()
}
