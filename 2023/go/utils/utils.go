package utils

import (
	"bufio"
	"fmt"
	"os"
)

func ReadFileLines(path string, callback func(string)) {
	file, err := os.Open(path)
	if err != nil {
		panic(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)

	for scanner.Scan() {
		line := scanner.Text()
		callback(line)
	}

	if err := scanner.Err(); err != nil {
		panic(err)
	}
}

func PrintEqualityTest[T comparable](expected, actual T) {
	fmt.Printf("Expected: %v, Actual: %v, Equal: %v\n", expected, actual, expected == actual)
}

func IsDigit(b byte) bool {
	return b >= 48 && b <= 57
}
