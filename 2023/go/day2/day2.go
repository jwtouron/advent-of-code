package main

import (
	"regexp"
	"strconv"
	"strings"
	"advent-of-code/utils"
)

var (
	idRe = regexp.MustCompile("Game (\\d+)")
	redRe = regexp.MustCompile("(\\d+) red")
	greenRe = regexp.MustCompile("(\\d+) green")
	blueRe = regexp.MustCompile("(\\d+) blue")
)

type handful struct {
	r, g, b uint64
}

type game struct {
	id uint64
	handfuls []handful
}

func parseHandful(hf string) handful {
	var red, green, blue uint64
	var err error
	matches := redRe.FindStringSubmatch(hf)
	if matches != nil {
		if red, err = strconv.ParseUint(matches[1], 10, 0); err != nil {
			panic(err)
		}
	}
	matches = greenRe.FindStringSubmatch(hf)
	if matches != nil {
		if green, err = strconv.ParseUint(matches[1], 10, 0); err != nil {
			panic(err)
		}
	}
	matches = blueRe.FindStringSubmatch(hf)
	if matches != nil {
		if blue, err = strconv.ParseUint(matches[1], 10, 0); err != nil {
			panic(err)
		}
	}
	return handful{r: red, g: green, b: blue}
}

func parseGame(s string) game {
	id, err := strconv.ParseUint(idRe.FindStringSubmatch(s)[1], 10, 0)
	if err != nil { panic(err) }
	handfuls := make([]handful, 0)
	splits := strings.Split(s, ";")
	for _, split := range splits {
		handful := parseHandful(split)
		handfuls = append(handfuls, handful)
	}
	return game{id: id, handfuls: handfuls}
}

func part1() {
	var sum uint64
	utils.ReadFileLines("../input/day2.txt", func(line string) {
		game := parseGame(line)
		valid := true
		for _, handful := range game.handfuls {
			if !(handful.r <= 12 && handful.g <= 13 && handful.b <= 14) {
				valid = false
				break
			}
		}
		if valid {
			sum += game.id
		}
	})
	utils.PrintEqualityTest(2879, sum)
}

func part2() {
	var sum uint64
	utils.ReadFileLines("../input/day2.txt", func(line string) {
		game := parseGame(line)
		var maxR, maxG, maxB uint64
		for _, handful := range game.handfuls {
			maxR = max(handful.r, maxR)
			maxG = max(handful.g, maxG)
			maxB = max(handful.b, maxB)
		}
		maxR = max(maxR, 1)
		maxG = max(maxG, 1)
		maxB = max(maxB, 1)
		sum += maxR * maxG * maxB
	})
	utils.PrintEqualityTest(65122, sum)
}

func main() {
	part1()
	part2()
}
