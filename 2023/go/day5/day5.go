package main

import (
	"advent-of-code/utils"
	"fmt"
	"os"
	"strconv"
	"strings"
)

type category = int

const (
	Seed = iota
	Soil
	Fertilizer
	Water
	Light
	Temperature
	Humidity
	Location
)

func parseCategory(s string) category {
	if s == "seed" {
		return Seed
	} else if s == "soil" {
		return Soil
	} else if s == "fertilizer" {
		return Fertilizer
	} else if s == "water" {
		return Water
	} else if s == "light" {
		return Light
	} else if s == "temperature" {
		return Temperature
	} else if s == "humidity" {
		return Humidity
	} else if s == "location" {
		return Location
	} else {
		panic(fmt.Sprintf("Unknown category: %v", s))
	}
}

type Almanac struct {
	seeds []uint64
	maps []Map
}

func parseAlmanac(s string) Almanac {
	splits := strings.Split(s, "\n\n")
	seeds := parseSeeds(splits[0])
	maps := make([]Map, 0, 10)
	for i := 1; i < len(splits); i++ {
		maps = append(maps, parseMap(splits[i]))
	}
	return Almanac{seeds: seeds, maps: maps}
}

func parseSeeds(s string) []uint64 {
	seeds := make([]uint64, 0, 100)
	splits := strings.Split(strings.Split(s, "seeds: ")[1], " ")
	for _, split := range splits {
		seed, err := strconv.Atoi(split)
		if err != nil { panic(err) }
		seeds = append(seeds, uint64(seed))
	}
	return seeds
}
type Map struct {
	source category
	dest category
	ranges []Range
}

func parseMap(s string) Map {
	lines := strings.Split(s, "\n")
	splits := strings.Split(strings.Split(lines[0], " map:")[0], "-to-")
	ranges := make([]Range, 0, 10)
	for i := 1; i < len(lines); i++ {
		if lines[i] != "" {
			splits := strings.Split(lines[i], " ")
			source_start, err := strconv.Atoi(splits[1])
			if err != nil { panic(err) }
			dest_start, err := strconv.Atoi(splits[0])
			if err != nil { panic(err) }
			length, err := strconv.Atoi(splits[2])
			if err != nil { panic(err) }
			ranges = append(ranges, Range{source_start: uint64(source_start), dest_start: uint64(dest_start), length: uint64(length)})
		}
	}
	return Map{source: parseCategory(splits[0]), dest: parseCategory(splits[1]), ranges: ranges}
}

type Range struct {
	source_start uint64
	dest_start uint64
	length uint64
}

func findLocation(seed uint64, almanac Almanac) uint64 {
	current_value := seed
	current_category := Seed
	for current_category != Location {
		for _, m := range almanac.maps {
			if m.source == current_category {
				current_category = m.dest
				for _, r := range m.ranges {
					if current_value >= r.source_start && current_value < r.source_start + r.length {
						current_value += r.dest_start - r.source_start
						break
					}
				}
			}
		}
	}
	return current_value
}

func part1(almanac Almanac) {
	var lowest uint64 = ^uint64(0)
	for _, seed := range almanac.seeds {
		loc := findLocation(seed, almanac)
		lowest = min(lowest, loc)
	}
	utils.PrintEqualityTest(389056265, lowest)
}

type interval struct {
	start, end uint64
}

func intervalIntersection(int1, int2 interval) (interval, bool) {
	if int2.start >= int1.start && int2.end <= int1.end {
		// int2 is entirely within int1
		return interval{int2.start, int2.end}, true
	} else if int1.start >= int2.start && int1.end <= int2.end {
		// int1 is entirely within int2
		return interval{int1.start, int1.end}, true
	} else if int1.start >= int2.start && int1.start < int2.end {
		// int1's start is within int2
		return interval{int1.start, int2.end}, true
	} else if int1.end > int2.start && int1.end < int2.end {
		// int1's end is within int2
		return interval{int2.start, int1.end}, true
	} else if int2.start >= int1.start && int2.start < int1.end {
		// int2's start is within int1
		return interval{int2.start, int1.end}, true
	} else if int2.end > int1.start && int2.end < int1.end {
		// int2's end is witin int1
		return interval{int1.start, int2.end}, true
	}
	return interval{}, false
}

func intervalDifferences(interval1, interval2 interval) []interval {
	differences := make([]interval, 0, 2)
	if interval2.start > interval1.start && interval2.start < interval1.end {
		differences = append(differences, interval{interval1.start, interval2.start})
	}
	if interval2.end > interval1.start && interval2.end < interval1.end {
		differences = append(differences, interval{interval2.end, interval1.end})
	}
	return differences
}

func calcNextCategory(cat category, inputIntervals []interval, almanac Almanac) (category, []interval) {
	next_category := cat
	var m Map
	for _, m = range almanac.maps {
		if m.source == cat {
			next_category = m.dest
			break
		}
	}
	outputIntervals := make([]interval, 0, len(inputIntervals))
	for ii := 0; ii < len(inputIntervals); ii++ {
		interval1 := inputIntervals[ii]
		found := false
		for _, r := range m.ranges {
			interval2 := interval{r.source_start, r.source_start + r.length}
			var intersection interval
			intersection, found = intervalIntersection(interval1, interval2)
			if found {
				diff := r.dest_start - r.source_start
				outputIntervals = append(outputIntervals, interval{intersection.start + diff, intersection.end + diff})
				diffIntervals := intervalDifferences(interval1, interval2)
				for _, interval := range diffIntervals {
					inputIntervals = append(inputIntervals, interval)
				}
				break
			}
		}
		if !found {
			outputIntervals = append(outputIntervals, interval1)
		}
	}
	return next_category, outputIntervals
}

func part2(almanac Almanac) {
	var lowest uint64 = ^uint64(0)
	inputRanges := make([]interval, 0, 100)
	for i := 0; i < len(almanac.seeds); i += 2 {
		inputRanges = append(inputRanges, interval{almanac.seeds[i], almanac.seeds[i] + almanac.seeds[i+1]})
	}
	cat := Seed
	for cat != Location {
		cat, inputRanges = calcNextCategory(cat, inputRanges, almanac)
	}
	for _, rp := range inputRanges {
		lowest = min(lowest, rp.start)
	}
	utils.PrintEqualityTest(137516820, lowest)
}

func main() {
	path := "../input/day5.txt"
	if len(os.Args) > 1 {
		path = os.Args[1]
	}
	bs, err := os.ReadFile(path)
	if err != nil { panic(err) }
	content := string(bs)
	almanac := parseAlmanac(content)
	part1(almanac)
	part2(almanac)
}
