package main

import "core:c"
import "core:fmt"
import "core:strings"

print_equality_test :: proc(label: string = "", expected, actual: $T) {
	fmt.printfln("%sExpected: %v, Actual: %v, Equal: %v", label, expected, actual, expected == actual)
}

regex_t :: struct {
	__buffer: rawptr,
	__allocated: c.size_t,
	__used: c.size_t,
	__syntax: c.ulong,
	__fastmap: rawptr,
	__translate: rawptr,
	re_nsub: c.size_t,
	__flags: c.uint,
}

regmatch_t :: struct {
	rm_so: c.int,
	rm_eo: c.int,
}

foreign import libc "system:c"
foreign libc {
	regcomp :: proc(preg: ^regex_t, regex: cstring, cflags: c.int) -> c.int ---
	regexec :: proc(preg: ^regex_t, string: cstring,
			nmatch: c.size_t, pmatch: ^regmatch_t,
			eflags: c.int) -> c.int ---
	regerror :: proc(errcode: c.int, preg: ^regex_t,
			errbuf: ^u8, errbuf_size: c.size_t) -> c.size_t ---
	regfree :: proc(preg: ^regex_t) ---
}

REG_EXTENDED :: 1

example1 := []string{
	 "1abc2",
	 "pqr3stu8vwx",
	 "a1b2c3d4e5f",
	 "treb7uchet"
}

example2 := []string{
	"two1nine",
	"eightwothree",
	"abcone2threexyz",
	"xtwone3four",
	"4nineeightseven2",
	"zoneight234",
	"7pqrstsixteen",
}

input := strings.split(#load("../../input/day1.txt", string), "\n")

num_map := map[string]u64{
	"0"     = 0,
	"1"     = 1,
	"2"     = 2,
	"3"     = 3,
	"4"     = 4,
	"5"     = 5,
	"6"     = 6,
	"7"     = 7,
	"8"     = 8,
	"9"     = 9,
	"zero"  = 0,
	"one"   = 1,
	"two"   = 2,
	"three" = 3,
	"four"  = 4,
	"five"  = 5,
	"six"   = 6,
	"seven" = 7,
	"eight" = 8,
	"nine"  = 9,
}

solve :: proc(s: []string, regex: string) -> u64 {
	defer free_all(context.temp_allocator)
	sum : u64 = 0
	preg: regex_t
	regex_c := strings.clone_to_cstring(regex, context.temp_allocator)
	rc := regcomp(&preg, cstring(regex_c), REG_EXTENDED)
	assert(rc == 0)
	for line in s {
		line := transmute([^]u8)strings.clone_to_cstring(line, context.temp_allocator)
		pmatch: [3]regmatch_t
		rc = regexec(&preg, cstring(line), 3, transmute(^regmatch_t)&pmatch, 0)
		if rc != 0 {
			continue
		}

		line = line[pmatch[0].rm_so:]
		orig := line[pmatch[0].rm_eo - pmatch[0].rm_so]
		line[pmatch[0].rm_eo - pmatch[0].rm_so] = 0
		first := strings.clone_from_cstring(cstring(line), context.temp_allocator)
		line[pmatch[0].rm_eo - pmatch[0].rm_so] = orig

		last := first

		for {
			line = line[1:]
			rc = regexec(&preg, cstring(line), 1, transmute(^regmatch_t)&pmatch, 0)
			if rc != 0 {
				break
			}
			line = line[pmatch[0].rm_so:]
			orig := line[pmatch[0].rm_eo - pmatch[0].rm_so]
			line[pmatch[0].rm_eo - pmatch[0].rm_so] = 0
			last = strings.clone_from_cstring(cstring(line), context.temp_allocator)
			line[pmatch[0].rm_eo - pmatch[0].rm_so] = orig
		}

		sum += (num_map[first] * 10) + num_map[last]
	}
	return sum
}

part1 :: proc(s: []string) -> u64 {
	regex :: "([0-9])"
	return solve(s, regex)
}

part2 :: proc(s: []string) -> u64 {
	return solve(s, "([0-9]|zero|one|two|three|four|five|six|seven|eight|nine)")
}

main :: proc() {
	print_equality_test("Part 1: ", u64(55090), part1(input))
	print_equality_test("Part 2: ", u64(54845), part2(input))
}

import "core:testing"

@(test)
test_part1_example :: proc(t: ^testing.T) {
	testing.expect_value(t, part1(example1), 142)
}

@(test)
test_part1_solution :: proc(t: ^testing.T) {
	testing.expect_value(t, part1(input), 55090)
}

@(test)
test_part2_example :: proc(t: ^testing.T) {
	testing.expect_value(t, part2(example2), 281)
}

@(test)
test_part2_solution :: proc(t: ^testing.T) {
	testing.expect_value(t, part2(input), 54845)
}
